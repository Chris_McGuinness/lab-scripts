import pyvisa as visa
import time

class gpd_2303s(object):

    def __init__(self, comm="COM9", tmout=3000):
        print("Initializing gpd_2303s at ", comm)

        rm = visa.ResourceManager()
        self.dev = rm.open_resource(comm, baud_rate=9600)
        self.dev.timeout = tmout * 1
        self.dev.write("*CLS")
        print("Device initialized \n \n")

    def set_current(self, channel=1, current=0.0):
        if channel not in [1, 2]:
            print('Incorrect channel input.')

        else:
            set_current_string = 'ISET' + str(channel) + ':'

            if 0 <= current < 3:
                current = round(current, 3)
                set_current_string = set_current_string + str(current)

                self.dev.write(set_current_string)
                time.sleep(0.2)

                what_current = self.dev.query('ISET' + str(channel) + '?')
                print("Channel " + str(channel) + " current is now set to " + str(what_current))

            else:
                print('Current out of bounds.')

    def set_voltage(self, channel=1, voltage=0.0):
        if channel not in [1, 2]:
            print('Incorrect channel input.')

        else:
            set_voltage_string = 'VSET' + str(channel) + ':'

            if 0 <= voltage < 30:
                voltage = round(voltage, 3)
                set_voltage_string = set_voltage_string + str(voltage)

                self.dev.write(set_voltage_string)
                time.sleep(0.2)

                what_voltage = self.dev.query('VSET' + str(channel) + '?')
                print("Channel " + str(channel) + " voltage is now set to " + str(what_voltage))

            else:
                print('Voltage out of bounds.')

    def actual_output_current(self, channel=1):
        if channel not in [1, 2]:
            print('Incorrect channel input.')

        else:
            output_current_string = 'IOUT' + str(channel) + '?'
            output_current = self.dev.query(output_current_string)
            print("Actual output current is " + str(output_current))

    def actual_output_voltage(self, channel=1):
        if channel not in [1, 2]:
            print('Incorrect channel input.')

        else:
            output_voltage_string = 'VOUT' + str(channel) + '?'
            output_voltage = self.dev.query(output_voltage_string)
            print("Actual output voltage is " + str(output_voltage))

    def turn_on_off(self, status=0):
        if status not in [0, 1]:
            print('Incorrect status given.')

        else:
            output_string = 'OUT' + str(status)
            self.dev.write(output_string)

            current_status = self.dev.query('STATUS?')

            if int(current_status[5]) == 0:
                print("Power supply output is turned OFF.")
            elif int(current_status[5]) == 1:
                print("Power supply output is turned ON.")
            else:
                print("ERROR! Status unknown.")