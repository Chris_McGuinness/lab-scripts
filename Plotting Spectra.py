import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

file0 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Comb Spectra\SHIJIA_P950_W1550_348_C40_46_T23_1_Spectrum.txt'
file1 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Comb Spectra\SHIJIA_P1075_W1550_348_C40_5_T23_1_Spectrum.txt'

all_files = [file0, file1]

spectra = plt.figure()
for file in all_files:
    df = pd.read_csv(file, sep="\t", header=2)
    plt.plot(df['nm'], df['dBm'])
    # plt.grid(True)
    # plt.title('Tuneable Laser Example Modes')
    plt.xlabel('Wavelength (nm)')
    plt.ylabel('Power (dBm)')
    plt.xticks(np.arange(1547, 1553, 0.25))
    plt.xticks(rotation=25)
    plt.xticks(fontsize=6)
    plt.xlim(1550, 1551.4)
    # plt.ylim(-90, 10)
plt.show()

# spectra = plt.figure()
# df = pd.read_csv(file1, sep="\t", header=2)
# x = df['nm']
# y = df['dBm']
# plt.plot(x, y)
# # plt.grid(True)
# # plt.title('Tuneable Laser Example Modes')
# plt.xlabel('Wavelength (nm)')
# plt.ylabel('Power (dBm)')
# plt.xticks(np.arange(1547, 1553, 0.25))
# plt.xticks(rotation=25)
# plt.xticks(fontsize=6)
# # plt.xlim(1548.75, 1552.25)
# plt.xlim(1550, 1551.4)
# # plt.ylim(-90, 10)
# peaks, _ = find_peaks(y, height=-30)
# plt.plot(x[peaks], y[peaks], "x")
# plt.show()

# print(f'Peak Wavelength: {list(x[peaks])} \nWavelength Difference: {np.diff(x[peaks])}'
#       f'\nPeak Power: {list(y[peaks])} \nPower Difference: {np.diff(y[peaks])}')
