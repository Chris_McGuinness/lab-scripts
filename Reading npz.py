import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
from datetime import datetime

def burn_in_time(files):
    '''
    Takes a list of file paths.
    Prints the total time of each burn in duration in hours.
    '''
    for i in range(len(files)):
        data = np.load(fr'{files[i]}')
        time = (data['time_list'][-1] - data['time_list'][0])/3600
        print(f'{time} Hours')

def plot_power_vs_time(path2, path3):
    files2 = [file for file in listdir(path2) if isfile(join(path2, file))]
    files3 = [file for file in listdir(path3) if isfile(join(path3, file))]

    t0 = np.load(fr'{path3}\{files3[0]}')['time_list'][0]

    # some are plotted individually to clean away messy data when the lasers were not on
    # 1-2 is in blue, 1-3 is in red
    for i in range(len(files2)):
        data = np.load(fr'{path2}\{files2[i]}')
        power = [float(i) for i in data['power_list']]
        time = (data['time_list'] - t0) / 3600
        if i == 0:
            plt.plot(time, power, 'blue', label='Pilot 1-2')
        elif i == 8:
            plt.plot(time[0:2085], power[0:2085], 'blue')
        elif i == 10:
            plt.plot(time[2:], power[2:], 'blue')
        else:
            plt.plot(time, power, 'blue')

    for i in range(len(files3)):
        data = np.load(fr'{path3}\{files3[i]}')
        power = [float(i) for i in data['power_list']]
        time = (data['time_list'] - t0) / 3600
        if i == 0:
            plt.plot(time, power, 'red', label='Pilot 1-3')
        elif i == 2:
            plt.plot(time[2:], power[2:], 'red')
        elif i == 10:
            plt.plot(time[0:2085], power[0:2085], 'red')
        elif i == 12:
            plt.plot(time[2:], power[2:], 'red')
        else:
            plt.plot(time, power, 'red')

    plt.legend(loc='best')
    plt.title('Laser Power vs Time in Oven')
    plt.xlabel('Time (hours)')
    plt.ylabel('Power (dBm)')
    plt.show()

def main():
    # paths to folders with all time and power data for Laser 1-2 and Laser 1-3
    # path2 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1-2Oven'
    # path3 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1-3Oven'
    #
    # # variables are named after the date the test ended
    # _14_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1657711680.0029278laser_3_power_stability_over_time.npz'
    # _15_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1657812817.1023185laser_3_power_stability_over_time.npz'
    # _18_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1657900226.9739132laser_3_power_stability_over_time.npz'
    # _20_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1658235847.4349232laser_3_power_stability_over_time.npz'
    # _25_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1658488586.4056013laser_3_power_stability_over_time.npz'
    # _28_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1658759175.1154587laser_3_power_stability_over_time.npz'
    # _29_07 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1659017203.9885507laser_3_power_stability_over_time.npz'
    # _02_08 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1659096534.5925732laser_3_power_stability_over_time.npz'
    # _04_08 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\1659457114.6733565laser_3_power_stability_over_time.npz'
    #
    # files = (_14_07, _15_07, _18_07, _20_07, _25_07, _28_07, _29_07, _02_08, _04_08)
    #
    # burn_in_time(files)
    # plot_power_vs_time(path2, path3)
    #
    # files2 = [file for file in listdir(path2) if isfile(join(path2, file))]
    # files3 = [file for file in listdir(path3) if isfile(join(path3, file))]
    #
    # data2 = np.load(fr'{path2}\{files2[-1]}')
    # power2 = [float(i) for i in data2['power_list']]
    # time2 = (data2['time_list'] - data2['time_list'][0]) / 3600
    #
    # data3 = np.load(fr'{path3}\{files3[-1]}')
    # power3 = [float(i) for i in data3['power_list']]
    # time3 = (data3['time_list'] - data2['time_list'][0]) / 3600
    #
    # start = (np.abs(time3 - 20.2)).argmin()
    # end = (np.abs(time3 - 20.7)).argmin()
    #
    # mean = np.mean(power3[start:end])
    # std = np.std(power3[start:end])
    # print(f'Mean Power at Room Temperature = {mean} ± {std}')

    # datetime(year, month, day, hour, minute, second, microsecond)
    a = datetime(2022, 8, 26, 8, 51, 8)
    b = datetime(2022, 8, 29, 4, 47, 39)
    c = b-a
    print(c.total_seconds()/3600)

    # list1 = (1,2,3,4,5)
    # list2 = (6,7,8,9,10)
    # list3 = list1 + list2[1:]
    # print(list1, '\n', list2, '\n', list3)

if __name__ == '__main__':
    main()

    print('Done!')
