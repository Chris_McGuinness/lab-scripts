import numpy as np
import os

path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\1250 um\WFR8-5-FP1250-A6\Spectra\Post cal"
for file in os.listdir(path):
    if file.endswith(".npz"):
        loc = file.split(".n")[0]
        # print(loc)
        data = np.load(f"{path}\{file}")
        # try:
        wavelength = data["arr_0"]
        power = data["arr_1"]
        header = data["arr_2"]

        new_file = f"{path}\{loc}.csv"

        # ---'w' is for writing to first line of the file
        f = open(new_file, 'w')
        for i in header:
            f.write(i)
            # f.write(i.split("\n")[0])
        f.write("Wavelength, Power")
        f.close()

        for i in range(len(wavelength)):
            data = f"\n{wavelength[i]}, {power[i]}"
            f = open(new_file, 'a')
            f.write(data)
            f.close()
