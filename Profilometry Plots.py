import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from uncertainties import ufloat

# Angled Slotted Structures
# file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2023.01.27\Slot0_7_x100_Green_X.csv"
# file2 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2023.01.27\Slot0_7_x100_Green_Y.csv"

# Ridge Top X
file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2023.01.27\Master_C3_Gain_Facet_X.csv"

# Slot Bottom X
file2 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2023.01.27\Master_C3_Gain_Ridge_X.csv"

# Ridge Top Y
file3 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2023.01.27\Master_C3_DBR_Facet_X.csv"


df1 = pd.read_csv(file1, sep=',', skiprows=3)
df2 = pd.read_csv(file2, sep=',', skiprows=3)
df3 = pd.read_csv(file3, sep=',', skiprows=3)
_df1 = df1.fillna(0)
_df2 = df2.fillna(0)
_df3 = df3.fillna(0)
# print(_df1)


### Finding average height of the top level
print("Top Level")
top_1 = _df1["Raw(µm)"].loc[(3 < _df1["Lateral(µm)"]) & (_df1["Lateral(µm)"] < 20)].to_numpy()[0]
_top1 = _df1["Raw(µm)"].loc[(33 < _df1["Lateral(µm)"]) & (_df1["Lateral(µm)"] < 45)].to_numpy()[0]
all_top_1 = np.array([top_1, _top1])
print(rf"{ufloat(np.mean(all_top_1), np.std(all_top_1)):.5f}")

top_2 = _df2["Raw(µm)"].loc[(3 < _df2["Lateral(µm)"]) & (_df2["Lateral(µm)"] < 20)].to_numpy()[0]
_top2 = _df2["Raw(µm)"].loc[(33 < _df2["Lateral(µm)"]) & (_df2["Lateral(µm)"] < 45)].to_numpy()[0]
all_top_2 = np.array([top_2, _top2])
print(rf"{ufloat(np.mean(all_top_2), np.std(all_top_2)):.5f}")

top_3 = _df3["Raw(µm)"].loc[(3 < _df3["Lateral(µm)"]) & (_df3["Lateral(µm)"] < 20)].to_numpy()[0]
_top3 = _df3["Raw(µm)"].loc[(33 < _df3["Lateral(µm)"]) & (_df3["Lateral(µm)"] < 45)].to_numpy()[0]
all_top_3 = np.array([top_3, _top3])
print(rf"{ufloat(np.mean(all_top_3), np.std(all_top_3)):.5f}")


### Finding average height of the trench
print("Trench Level")
trench_1 = _df1["Raw(µm)"].loc[(20.6 < _df1["Lateral(µm)"]) & (_df1["Lateral(µm)"] < 23.8)].to_numpy()[0]
_trench_1 = _df1["Raw(µm)"].loc[(28 < _df1["Lateral(µm)"]) & (_df1["Lateral(µm)"] < 31)].to_numpy()[0]
all_trench_1 = np.array([trench_1, _trench_1])
# print(rf"{ufloat(np.mean(all_trench_1), np.std(all_trench_1)):.5f}")

trench_2 = _df2["Raw(µm)"].loc[(21.3 < _df2["Lateral(µm)"]) & (_df2["Lateral(µm)"] < 24.8)].to_numpy()[0]
_trench_2 = _df2["Raw(µm)"].loc[(28.5 < _df2["Lateral(µm)"]) & (_df2["Lateral(µm)"] < 32)].to_numpy()[0]
all_trench_2 = np.array([trench_2, _trench_2])
# print(rf"{ufloat(np.mean(all_trench_2), np.std(all_trench_2)):.5f}")

trench_3 = _df3["Raw(µm)"].loc[(20.9 < _df3["Lateral(µm)"]) & (_df3["Lateral(µm)"] < 24.3)].to_numpy()[0]
_trench_3 = _df3["Raw(µm)"].loc[(27.8 < _df3["Lateral(µm)"]) & (_df3["Lateral(µm)"] < 31.4)].to_numpy()[0]
all_trench_3 = np.array([trench_3, _trench_3])
# print(rf"{ufloat(np.mean(all_trench_3), np.std(all_trench_3)):.5f}")


### Finding average height of the ridge
print("Ridge Level")
ridge_1 = _df1["Raw(µm)"].loc[(24.5 < _df1["Lateral(µm)"]) & (_df1["Lateral(µm)"] < 27)].to_numpy()
print(rf"{ufloat(np.mean(ridge_1), np.std(ridge_1)):.5f}")

ridge_2 = _df2["Raw(µm)"].loc[(26 < _df2["Lateral(µm)"]) & (_df2["Lateral(µm)"] < 27.8)].to_numpy()
print(rf"{ufloat(np.mean(ridge_2), np.std(ridge_2)):.5f}")

ridge_3 = _df3["Raw(µm)"].loc[(25.2 < _df3["Lateral(µm)"]) & (_df3["Lateral(µm)"] < 27.2)].to_numpy()
print(rf"{ufloat(np.mean(ridge_3), np.std(ridge_3)):.5f}")

###
print(rf"Gain Trench Depth 1 = {abs(np.mean(all_top_1)-np.mean(all_trench_1)):.5f} +/- "
      rf"{np.sqrt((np.std(all_top_1)**2)+(np.std(all_trench_1)**2)):.5f} µm")
print(rf"Gain Ridge Height 1 = {abs(np.mean(all_trench_1)-np.mean(ridge_1)):.5f} +/- "
      rf"{np.sqrt((np.std(all_trench_1)**2)+(np.std(ridge_1)**2)):.5f} µm")

print(rf"Centre Trench Depth 2 = {abs(np.mean(all_top_2)-np.mean(all_trench_2)):.5f} +/- "
      rf"{np.sqrt((np.std(all_top_2)**2)+(np.std(all_trench_2)**2)):.5f} µm")
print(rf"Centre Ridge Height 2 = {abs(np.mean(all_trench_2)-np.mean(ridge_2)):.5f} +/- "
      rf"{np.sqrt((np.std(all_trench_2)**2)+(np.std(ridge_2)**2)):.5f} µm")

print(rf"DBR Trench Depth 3 = {abs(np.mean(all_top_3)-np.mean(all_trench_3)):.5f} +/- "
      rf"{np.sqrt((np.std(all_top_3)**2)+(np.std(all_trench_3)**2)):.5f} µm")
print(rf"DBR Ridge Height 3 = {abs(np.mean(all_trench_3)-np.mean(ridge_3)):.5f} +/- "
      rf"{np.sqrt((np.std(all_trench_3)**2)+(np.std(ridge_3)**2)):.5f} µm")

print(f"Gain Facet: Ridge Width Bottom = {abs(23.8-28):.2f} µm, Top = {abs(24.5-27):.2f} µm,"
      f" Delta = {abs(abs(23.8-28) - abs(24.5-27)):.2f} µm")
print(f"Centre: Ridge Width Bottom = {abs(24.8-28.5):.2f} µm, Top = {abs(26-27.8):.2f} µm,"
      f" Delta = {abs(abs(24.8-28.5) - abs(26-27.8)):.2f} µm")
print(f"DBR Facet: Ridge Width Bottom = {abs(24.3-27.8):.2f} µm, Top = {abs(25.2-27.2):.2f} µm,"
      f" Delta = {abs(abs(24.3-27.8) - abs(25.2-27.2)):.2f} µm")


plt.figure(figsize=(10, 5))
plt.subplot(311)
plt.title("Device: Master C3")
plt.yticks(np.arange(-1.5, 0.75, step=0.5))
plt.xticks([])
plt.vlines((23.8, 28), ymin=-1.5, ymax=0.75, colors="red")
plt.vlines((24.5, 27), ymin=-1.5, ymax=0.75, colors="green")
plt.plot(_df1["Lateral(µm)"], _df1["Raw(µm)"])
plt.ylabel("Gain Facet \nHeight (µm)")
plt.subplot(312)
plt.yticks(np.arange(-1.5, 0.75, step=0.5))
plt.xticks([])
plt.vlines((24.8, 28.5), ymin=-1.5, ymax=0.75, colors="red")
plt.vlines((26, 27.8), ymin=-1.5, ymax=0.75, colors="green")
plt.plot(_df2["Lateral(µm)"], _df2["Raw(µm)"])
plt.ylabel("Centre Ridge \nHeight (µm)")
plt.subplot(313)
plt.yticks(np.arange(-1.5, 0.75, step=0.5))
plt.vlines((24.3, 27.8), ymin=-1.5, ymax=0.75, colors="red")
plt.vlines((25.2, 27.2), ymin=-1.5, ymax=0.75, colors="green")
plt.plot(_df3["Lateral(µm)"], _df3["Raw(µm)"])
plt.xlabel("Width (µm)")
plt.ylabel("DBR Facet \nHeight (µm)")
# plt.subplot(211)
# plt.title(r"Angled Slotted Device 0.7$\mu$m")
# plt.plot(_df1["Lateral(µm)"], _df1["Raw(µm)"])
# plt.xticks([])
# plt.ylabel("Ridge Height (µm)")
# plt.subplot(212)
# plt.plot(_df2["Lateral(µm)"], _df2["Raw(µm)"])
# plt.xlabel("Width (µm)")
# plt.ylabel("Slot Height (µm)")
plt.show()
