import pyvisa as visa
import time

class ILX_SCh:
    def __init__(self):
        rm = visa.ResourceManager()
        self.dev = rm.open_resource("GPIB0::12::INSTR")

        self.dev.clear()
        print("Current connected")
        time.sleep(0.2)

    def laser_on(self):
        self.dev.write(f"LAS:OUT ON")

    def laser_off(self):
        self.dev.write(f"LAS:OUT OFF")
        self.dev.write(f"LAS:I {0}")

    def set_current(self, current):
        self.dev.write(f"LAS:LDI {current}")

    def query_current(self):
        I = float(self.dev.query("LAS:LDI?"))
        return I
