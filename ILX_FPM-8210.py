import pyvisa as visa
import time
import numpy as np

########################################################################################################################
class ILX_PowerMeter(object):
    def __init__(self, address="GPIB0::5::INSTR"):
        rm = visa.ResourceManager()
        self.address = address
        self.PM = rm.open_resource(address)
        time.sleep(0.2)
        self.PM.write("*CLS")
        time.sleep(0.2)
        self.PM.write("DISP")
        time.sleep(0.2)
        self.PM.write("SENS:POW:RANG:AUTO")
        time.sleep(0.2)

    def set_wavelength(self, wavelength):
        self.PM.write(f"WAVE {wavelength}")

    def take_power_reading(self):
        power = float(self.PM.query("POW?")) * 1000  # converting to mW
        time.sleep(0.2)
        return power
