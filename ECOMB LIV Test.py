import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def read_excel_data(file):
    '''
    Creates pandas dataframe from Excel file.
    Takes file location as a string and Excel sheet name as a string.
    Returns Pandas DataFrame and lists of Light, Current and Voltage Values.
    '''

    df = pd.read_excel(file)

    L = []
    I = []
    V = []

    # print(df)

    # # appending data from dataframe
    for i in range(len(df.index)):
        if df['L (nW)'][i] != 'Low Signal':
            # converting L from dBm to mW
            # L.append(float(10**( df['L (dBm)'][i]/10 )))
            L.append(float(df['L (nW)'][i] * 1e-6))
            I.append(float(df['I (mA)'][i]))
            V.append(float(df['V (V)'][i]))

    return df, L, I, V

def main():
    file = r'C:\Users\ChrisMcGuinness\Documents\ECOMB LIV Test2 (15_08_22).xlsx'
    df, L, I, V = read_excel_data(file)

    plt.plot(I, V)
    plt.title('I vs V')
    plt.xlabel('Current (mA)')
    plt.ylabel('Voltage (V)')
    plt.grid(True)
    plt.show()

    plt.plot(I, L)
    plt.title('I vs L')
    plt.xlabel('Current (mA)')
    plt.ylabel('Power (mW)')
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    main()

    print('Done!')
