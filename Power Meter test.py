from instrs import ILX_FPM_8210
import time
# import pyvisa as visa

# rm = visa.ResourceManager()
# resources_info = rm.list_resources_info()
# print('VISA Resources:\n')
# print(rm.list_resources())

# set wavelenght, unit of power, reading speed
power_meter = ILX_FPM_8210(1350, 'dBm', 'FAST')
power_meter.config()

print('Reading Power for 20s')
for _ in range(20):
    reading = power_meter.readPower()
    print(reading, 'dBm')
    time.sleep(1)

print('Done reading Power')
