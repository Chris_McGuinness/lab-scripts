import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
# from scipy.optimize import curve_fit
from lmfit.models import LorentzianModel

def dbm_to_mw(x):
    return 10**(x/10)

# def std(x, a, x0, sigma):
#     return a * np.exp( -(x - x0)**2 /( 2*sigma**2 ) )

file0 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\380_Power\0dbm_Spectrum.txt"
file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\380_Power\5dbm_Spectrum.txt"
file2 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\380_Power\10dbm_Spectrum.txt"
file3 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\380_Power\15dbm_Spectrum.txt"
file4 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\380_Power\20dbm_Spectrum.txt"

# file0 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\Smallest Ring Temperature\T20_Spectrum.txt"
# file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\Smallest Ring Temperature\T25_Spectrum.txt"
# file2 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MRR\Smallest Ring Temperature\T30_Spectrum.txt"

# files = [file0, file1, file2, file3, file4]
# all_peaks = []

# for file in files:
df = pd.read_csv(file4, sep="\t", header=2)
wl = df["nm"].to_numpy()
dbm = dbm_to_mw(df["dBm"].to_numpy())

y = dbm*-1
peaks, _ = signal.find_peaks(y, prominence=4e-6)
peak_x = wl[peaks]
peak_y = dbm[peaks]
print(peak_x)
# all_peaks.append(peak_x.to_numpy())

plt.plot(wl, dbm)
plt.plot(peak_x, peak_y, "*")
plt.xlabel('Wavelength (nm)')
plt.ylabel('Power (dBm)')
plt.show()

# gaussian fits
window = 70
_x = wl[peaks[1]-window:peaks[1]+window]
_y = dbm[peaks[1]-window:peaks[1]+window]
# _x -= wl[peaks[0]]
_y -= _y[0]

model = LorentzianModel()
params = model.guess(_y, x=_x)
result = model.fit(_y, params, x=_x)
print(result.fit_report())

# popt, pcov = curve_fit(std, _x, _y)#, p0=(3, 0, 0.1, 50.6))
# print(popt)
# test_x = np.linspace(-5, 5, 100)
# plt.plot(_x, _y)
result.plot_fit()
# plt.plot(_x, std(_x, *popt))
plt.title("Second mode")
plt.show()

# print(all_peaks)

# for i in range(1, 3):
#     print(all_peaks[i] - all_peaks[i-1])
