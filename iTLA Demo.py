import numpy as np
import pyvisa as visa
import time

rm = visa.ResourceManager()

# --- Functions for the ILX 16 Channel -----------
def LDC3916_connection(channel):
    LDC = rm.open_resource("GPIB0::1::INSTR")
    time.sleep(0.2)
    LDC.write(f"CHAN {channel}")
    # LDC.clear()
    # print("Connected")
    return LDC


def LDC3916_setup(LDC, gain, temp):
    # --- Set Current Limit
    LDC.write(f"LAS:LIM:I {gain + 1}")

    # --- Set Current
    LDC.write(f"LAS:LDI {gain}")
    LDC.write("LAS:OUT 1")

    # --- Set TEC
    LDC.write(f"TEC:T {temp}")
    LDC.write(f"TEC:OUT ON")
# -----------------------------------------------


# --- Functions for the 4 Channel R&S Power Supply ----
def initialise_RandS():
    RandS = rm.open_resource("GPIB0::30::INSTR")
    return RandS


def initialise_section_voltage(device, section, voltage):
    time.sleep(0.2)
    device.write(f"INST:NSEL {section}")
    # time.sleep(0.2)
    device.write(f"VOLT {voltage}")
    # device.write(f"CURR 0.05")
    device.write(f"OUTP:SEL 1")
    device.write(f"OUTP 1")


def change_section_voltage(device, section, voltage):
    """
    This function is designed to change the voltage on the ring or other section of the device that needs voltage swept.
    Takes the section and the desired voltage.
    This will be a reverse biased voltage so -5V input will set section to -5V. +5V input will also set section to -5V.
    Will call functions for the R&S HMP4040 from instrs and set the section voltage.
    """
    # time.sleep(0.2)
    device.write(f"INST:NSEL {section}")
    # device.write(f"INST OUT{section}")
    # time.sleep(0.2)
    device.write(f"VOLT {voltage}")
# ------------------------------------------------------


# --- Function to connect to the OSA -------------
def OSA_connection():
    """
    Connect to the ANDO OSA
    """
    time.sleep(0.2)
    OSA_address = "GPIB0::8::INSTR"
    print('Trying to connect to %s' % OSA_address)
    OSA_object = rm.open_resource(OSA_address)
    # OSA_connection_test(OSA_object)
    return OSA_object
# ------------------------------------------------


# --- Function to make an single sweep ------------------
def single_sweep(OSA_object):
    # time.sleep(1)
    try:
        OSA_object.write("SGL")
    except visa.errors.VisaIOError:
        print("Timeout error avoided")
        pass
    # gives the OSA time to update before any data is taken
    # time.sleep(0.5)
# ---------------------------------------------------------


# points = [(1, 5), (6.7, 9.7), (4.2, 8.7), (0.7, 6.1), (2.2, 8), (1.1, 7.9), (0.2, 7.9),
#           (2, 9.5), (0.7, 8.8), (0.9, 9.6), (9.7, 1.4), (9.8, 5), (8.8, 0.8), (8.8, 3.5),
#           (7.3, 1), (8.9, 5.4), (7.5, 2.3), (8.5, 6.2), (8.6, 7.2), (7.6, 4.6), (6, 0.8),
#           (5.2, 1.2), (8.8, 8.7), (4, 0.8), (2.7, 1.9), (5.1, 5.4), (2.2, 3.2), (6.1, 7.1),
#           (0.4, 0.6), (2.5, 4.8), (0.7, 2.7)]
# expected_wavelengths = [1573.3, 1572.13, 1571.3, 1570.4, 1569.04, 1567.6, 1565.34,
#                         1564.74, 1562.5, 1561.1, 1565.88, 1563.04, 1562.20, 1560.78,
#                         1558.5, 1557.95, 1557.1, 1556.52, 1555.1, 1554.28, 1553.46,
#                         1552.0, 1550.9, 1548.4, 1547, 1546.4, 1545.6, 1545.06,
#                         1544.80, 1542.8, 1542.0]

select_points = [(1, 5), (4.2, 8.7), (0.7, 6.1), (2.2, 8), (1.1, 7.9), (9.7, 1.4),
                 (2, 9.5), (9.8, 5), (8.8, 0.8), (8.8, 3.5), (7.3, 1), (8.9, 5.4), (7.5, 2.3),
                 (8.5, 6.2), (8.6, 7.2), (7.6, 4.6), (6, 0.8), (5.2, 1.2), (8.8, 8.7), (4, 0.8),
                 (2.7, 1.9), (5.1, 5.4), (2.2, 3.2), (6.1, 7.1), (0.4, 0.6), (2.5, 4.8), (0.7, 2.7)]

rev_select_points = [(4.2, 8.7), (0.7, 6.1), (2.2, 8), (1.1, 7.9), (9.7, 1.4),
                     (2, 9.5), (9.8, 5), (8.8, 0.8), (8.8, 3.5), (7.3, 1), (8.9, 5.4), (7.5, 2.3),
                     (8.5, 6.2), (8.6, 7.2), (7.6, 4.6), (6, 0.8), (5.2, 1.2), (8.8, 8.7), (4, 0.8),
                     (2.7, 1.9), (5.1, 5.4), (2.2, 3.2), (6.1, 7.1), (0.4, 0.6), (2.5, 4.8)]
rev_select_points = np.flip(rev_select_points, 0)

select_wavelengths = [1573.3, 1572.13, 1571.3, 1570.4, 1569.04, 1567.6, 1565.88,
                      1564.74, 1563.04, 1562.20, 1560.78, 1558.5, 1557.95, 1557.1,
                      1556.52, 1555.1, 1554.28, 1553.46, 1552.0, 1550.9, 1548.4,
                      1547, 1546.4, 1545.6, 1545.06, 1544.80, 1542.8, 1542.0]


def main():
    RandS = initialise_RandS()

    initialise_section_voltage(RandS, 1, 0)
    initialise_section_voltage(RandS, 2, 0)
    initialise_section_voltage(RandS, 4, 0)

    ANDO = OSA_connection()
    try:
        while True:

            for i in range(0,len(select_wavelengths),2):
                time.sleep(0.1)
                R1 = select_points[i][0]
                R2 = select_points[i][1]
                change_section_voltage(RandS, 1, R1)
                change_section_voltage(RandS, 2, R2)

                single_sweep(ANDO)
                while int(ANDO.query("SWEEP?")) != 0:
                    time.sleep(0.1)

            for j in range(0,len(select_wavelengths)-2,2):
                time.sleep(0.1)
                _R1 = rev_select_points[j][0]
                _R2 = rev_select_points[j][1]
                change_section_voltage(RandS, 1, _R1)
                change_section_voltage(RandS, 2, _R2)

                single_sweep(ANDO)
                while int(ANDO.query("SWEEP?")) != 0:
                    time.sleep(0.1)

    except KeyboardInterrupt:
        exit()


if __name__ == "__main__":
    main()
