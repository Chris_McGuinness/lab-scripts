import N7711A as TL
import numpy as np
import time
import HP68120C as WM
import ANDO_AQ6317B as ANDO
import sys
sys.path.insert(1, r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\PythonScripts")
from new_instrs import ILX_PM_8210 as PM
import matplotlib.pyplot as plt
import scipy.constants

### want to pull data from APEX via ethernet

def freq(λ):
    c = scipy.constants.c
    λ = λ*(1e-9)
    return c/λ

def wavelength(f):
    c = scipy.constants.c
    λ = c/f
    return λ*(1e9)

laser = TL.Agilent_TL()
power = PM(1550, "Watt","FAST")
# wavemeter = WM.HPWavemeter()

def main():
    """
    Initial conditions
    """
    start_wl = 1555.6  # nm
    wl_coarse_step = 0.1  # nm
    wl_fine_step = 0.001  # nm
    start_freq = freq(start_wl)
    """
    Tunable laser setup
    """
    laser.set_power_mW(5)
    laser.set_grid_mode()
    laser.set_grid_reference(start_freq)
    laser.set_grid_spacing(abs(freq(1551)-freq(1550)))
    laser.laser_on()
    laser.set_grid_channel(0)
    laser.set_grid_offset(0)
    # print(wavemeter.read_power_and_wavelength()[0])
    print(power.readPower())
#     """
#     Loops the coarse and fine tuning
#     """
#     plt.ion()
#     figure, ax = plt.subplots(figsize=(7, 5))
#     ax.ticklabel_format(useOffset=False)
#     try:
#         k = 0
#         while True:
#             p = []
#             w = []
#             for i in range(10):
#                 """
#                 Coarse tuning loop. Performs nm steps.
#                 """
#                 coarse_wl = start_wl + (wl_coarse_step*-i)
#                 laser.laser_off()
#                 laser.set_grid_reference(freq(coarse_wl))
#                 laser.laser_on()
#                 laser.set_grid_channel(0)
#                 # laser.set_grid_offset(0)
#                 for j in range(100):
#                     """
#                     Fine tuning loop. Performs pm steps.
#                     """
#                     fine_wl = coarse_wl + (wl_fine_step*-j)
#                     freq_diff = abs(freq(fine_wl) - freq(coarse_wl))
#                     laser.set_grid_offset(freq_diff)
#                     # d = wavemeter.read_power_and_wavelength()
#                     _p = power.readPower()
#                     _w = laser.wavelength_query()
#                     p.append(_p)
#                     w.append(_w)
#                     # plt.scatter(_w, _p, color="blue")
#                     # plt.pause(0.02)
#                     print((100*i)+j)#_p, _w)
#             if k == 0:
#                 spectrum1, = ax.plot(w, p)
#                 spectrum1.set_xdata(w)
#                 spectrum1.set_ydata(p)
#                 figure.canvas.draw()
#                 figure.canvas.flush_events()
#                 time.sleep(0.5)
#             else:
#                 spectrum1.set_xdata(w)
#                 spectrum1.set_ydata(p)
#                 figure.canvas.draw()
#                 figure.canvas.flush_events()
#                 time.sleep(0.5)
#             k += 1
#         plt.show()
#     except KeyboardInterrupt:
#         print("Interrupted Loop")
#         laser.laser_off()
#         plt.show()
    """
    Loops only the fine tuning
    """
    plt.ion()
    figure, ax = plt.subplots(figsize=(7, 5))
    ax.ticklabel_format(useOffset=False)
    for i in range(1):
        """
        Coarse tuning loop. Performs nm steps.
        """
        coarse_wl = start_wl + (wl_coarse_step*-i)
        laser.laser_off()
        laser.set_grid_reference(freq(coarse_wl))
        laser.laser_on()
        laser.set_grid_channel(0)
        # laser.set_grid_offset(0)
        try:
            k = 0
            while True:
                p = []
                w = []
                for j in range(10):
                    """
                    Fine tuning loop. Performs pm steps.
                    """
                    fine_wl = coarse_wl + (wl_fine_step*-j)
                    freq_diff = abs(freq(fine_wl) - freq(coarse_wl))
                    laser.set_grid_offset(freq_diff)
                    # d = wavemeter.read_power_and_wavelength()
                    _p = power.readPower() 
                    _w = laser.wavelength_query()
                    p.append(_p)
                    w.append(_w)
                    print(j)#_p, _w)
                if k == 0:
                    spectrum1, = ax.plot(w, p)
                    spectrum1.set_xdata(w)
                    spectrum1.set_ydata(p)
                    figure.canvas.draw()
                    figure.canvas.flush_events()
                    time.sleep(0.5)
                else:
                    spectrum1.set_xdata(w)
                    spectrum1.set_ydata(p)
                    figure.canvas.draw()
                    figure.canvas.flush_events()
                    time.sleep(0.5)
                k += 1
        except KeyboardInterrupt:
            print("Interrupted Loop")
            laser.laser_off()
            plt.show()

if __name__ == "__main__":
    start = time.time()
    main()
    end = time.time()
    print(f"Done in {(end-start)/60} minutes")
