import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import time as time
from scipy import signal
import os
import pandas as pd
import APEX_Loop


# def findMaxPowerWL(power, wavelength, rounding=True):
#     """ Returns Max power in dBm, Wavelength in nm
#     :param spectrum: optional, pass results of previous spectrum_pull() as tuple/list
#     :param rounding=True Rounds to 4 decimal places, disable by setting to false
#     :returns wavelength in nm (float); max power in dBm (float)
#     """
#     try:
#         peak_power = np.max(power)
#         peak_wavelength = wavelength[np.argmax(power)] * 1e9
#         if rounding:
#             peak_wavelength = round(peak_wavelength, 4)
#         return peak_power, peak_wavelength
#     except Exception as Exc:
#         print(Exc)
#         return None, None


# def linear_fit(x, a, b):
#     return a*x + b
#
# # AU = [0.105, 0.936]
# # mW = [0.005012, 0.041856]
# #
# # popt, pcov = curve_fit(linear_fit, AU, mW)
# # print(f'Intercept = {popt[1]} mW \nSlope = {popt[0]} mW/AU')
#
# check1 = 2.70868720e-02 - 1.89811196e-05*713.52
# check2 = 2.70868720e-02 / 2
# print(check1, check2)
#
# # t25_mw = 2.70868720e-02/(2*1.89811196e-05)
# t25_mw = 0.708/(2*1.89811196e-05)
# t85_mw = 5.32206e-04/(2*8.21049e-06)
#
# # t25_dBm = (-15.61617 - 3)/(-0.00345)
# # t85_dBm = (-32.702 - 3)/(-1.09324e-01)
#
# frac = (1/85)-(1/25)
# k = float(8.617e-5)#float(1.38e-23)
# Ea_mw = k*(np.log(t85_mw/t25_mw))/frac
# # Ea_dBm = k*(np.log(t85_dBm/t25_dBm))/frac
#
# # print(Ea)
# # print(slope, intercept)
# print(f'T25_mw = {t25_mw} days, T85_mw = {t85_mw} days, Ea_mw = {Ea_mw} eV')
# # print(f'T25_dBm = {t25_dBm} days, T85_dBm = {t85_dBm} days, Ea_dBm = {Ea_dBm} eV')

# print(1.1/np.cos(55*np.pi/180))

# data = np.load(r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\3P_20.0_spectrum.npz')
# a = data['a']
# w = data['w']
#
# peaks = signal.argrelmax(a, order=2)  # find peaks
# sorted_peaks = np.sort(a[peaks[0]])  # sorts peaks (smallest first)
# print('SMSR: ',  sorted_peaks[-1] - sorted_peaks[-2])  # smsr is the amplitude of the last peak - 2nd last
#
#
# # plots spec and plots peaks as red dots.
# plt.plot(w, a)
# plt.plot(w[peaks[0]], a[peaks[0]], 'ro')
# plt.show()

# I = np.arange(5, 80, 5)
# L = [1.5e-5, 5.8e-5, 13.4e-5, 48e-5, 0.03, 0.069, 0.11, 0.15, 0.27, 0.34, 0.42, 0.46, 0.52, 0.57, 0.61]
# print(L)
#
# plt.plot(I, L)
# plt.title('I vs L')
# plt.xlabel('Current (mA)')
# plt.ylabel('Power (mW)')
# plt.grid(True)
# plt.show()

# file = r"C:\Users\ChrisMcGuinness\Downloads\test data\test data\high_fsr_2.0.npz"
#
# # data = np.load(r"C:\Users\ChrisMcGuinness\Downloads\2022-11-01_1427_stability_Lyra_2.0.npz")
# data = np.load(file)
# for i in data:
#     print(i)
# wavelength = data["wavelength"]
# power = data["power"]
#
# plt.plot(wavelength, power)
# plt.show()
# # timestamp = data["timestamp"]
#
# # wavelength = wavelength[100]
# # power = power[100]
#
# print(len(wavelength))
# print(len(power))
#
# peak_power = []
# peak_wavelengths = []
#
# # t0 = timestamp[0]
# # for i in range(len(timestamp)):
# #     timestamp[i] = (timestamp[i] - t0) / 3600
# #     peaks, _ = signal.find_peaks(power[i], height=-40)
# #     peak_wavelengths.append(wavelength[i][peaks])
# #     peak_power.append(power[i][peaks])
#
#
# # plt.plot(timestamp, peak_power)
# # plt.xlabel("Time (hours)")
# # # plt.ylabel("Wavelength (nm)")
# # # plt.ylim(1560e-9, 1565e-9)
# # # plt.title("Wavelength Stability")
# # plt.ylabel("Power (dB)")
# # plt.ylim(-40, 0)
# # plt.title("Power Stability")
# # plt.show()
# indices = [0, 200, 400, 600, 800]
#
#
# # --- Looping over multiple spectra estimating SMSR for each peak
# # for j in indices:
# peaks, _ = signal.find_peaks(power, height=-60, prominence=10)
# # --- since we are looking at combs, the peak separation should be a constant value
# peak_separation = int(np.mean(np.diff(peaks)))
# prominences = signal.peak_prominences(power, peaks, wlen=peak_separation)[0]
# # prominences = signal.peak_prominences(power, peaks, wlen=100)[0]
# contour_heights = power[peaks] - prominences
#
# background_peaks, _ = signal.find_peaks(power, prominence=(None, np.min(prominences)/2))
#
# SMSR_estimate1 = []
# SMSR_estimate2 = []
#
# # --- Looking for background for each peak to estimate SMSR for each peak
# for i in range(len(peaks)):
#     # --- first method of estimating SMSR is to subtract the power of the closest background peak
#     index = (np.abs(background_peaks - peaks[i])).argmin()
#     # # print(background_peaks[index])
#     SMSR1 = power[peaks[i]]-power[background_peaks[index]]
#     SMSR_estimate1.append(SMSR1)
#
#     # --- second method finds the tallest background peak less than half-way to the next peak
#     upper = int(peaks[i] + (peak_separation / 2))
#     lower = int(peaks[i] - (peak_separation / 2))
#     # --- check for the background peak with the greatest optical power in this range
#     # --- this list stores indices of the background peaks
#     bp_in_range = []
#     for k in range(lower, upper):
#         if k in background_peaks:
#             bp_in_range.append(k)
#     # --- If no background peaks in range, default to the closest background peak
#     if bp_in_range == []:
#         index = (np.abs(background_peaks - peaks[i])).argmin()
#         bp_in_range.append(background_peaks[index])
#
#     # --- check which of the background peaks has the greatest optical power
#     bg_power = []
#     for l in range(len(bp_in_range)):
#         bg_power.append(power[bp_in_range[l]])
#     main_bg_peak = np.argmax(bg_power)
#
#     SMSR2 = power[peaks[i]] - bg_power[main_bg_peak]
#
#     SMSR_estimate2.append(SMSR2)
#
# print(f"SMSR Estimate 1: {SMSR_estimate1}")
# print(f"SMSR Estimate 2: {SMSR_estimate2}")
# print(f"SMSR Estimate Prominence: {prominences}")
#
# plt.plot(wavelength, power)
# plt.plot(wavelength[peaks], power[peaks], "x")
# plt.plot(wavelength[background_peaks], power[background_peaks], "+", color="red")
# plt.vlines(x=wavelength[peaks], ymin=contour_heights, ymax=power[peaks], colors="lime")
# plt.show()

# volts1 = [-5, -1.5, -6]
# volts2 = [-5, -1, -2.9]
# volts3 = [-5, -5, -8.3]
# # v = (-8.7/65535)*counts - 0.6
#
#
# def volts_to_counts(volts):
#     return (65535/-8.7)*volts + 0.6
#
#
# counts = [0, 0, 0]
# for i in range(len(counts)):
#     counts[i] = volts_to_counts(volts3[i])
# print(f"Phase = {counts[0]}, R1 = {counts[1]}, R2 = {counts[2]}")

# print(rf"{os.getcwd()}\LIV_Test_{time.time()}.csv")

# phase_start = 0
# phase_end = 10
# phase_step = 1
# phase_values = np.array(np.arange(phase_start, abs(phase_end+phase_step), phase_step))
# print(phase_values)
# print(np.flip(phase_values))
# kk = 8*np.pi/(767e-9)
# krb= 8*np.pi/(780e-9)
# print((2*kk/(krb + kk))-(2*krb/(krb + kk)))

# path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\1250 um\WFR8-5-FP1250-A6\Spectra\Post cal\WFR8-5-FP1250-A6_temp_23oC_current_40_1670857744.130493_res_0.02nm.npz"
# path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\700 um\WFR8-5-FP700-H1\Spectra\WFR8-5-FP700-H1_temp_20oC_current_40_1670867067.226786.npz"
# path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\1250 um\WFR8-5-FP1250-A6\Spectra\WFR8-5-FP1250-A6_temp_20oC_current_35_1670612249.165919.npz"
# path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\1250 um\WFR8-5-FP1250-A6\Spectra\WFR8-5-FP1250-A6_temp_20oC_current_35_1670612354.9792042.npz"
# path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\1250 um\WFR8-5-FP1250-A6\Spectra\WFR8-5-FP1250-A6_temp_20oC_current_35_1670612424.6416197.npz"
# path = r"C:\Users\ChrisMcGuinness\PILOT PHOTONICS\PIC Design - Experimental measurements\FP\500 um\WFR8-6-FP500-C1\Spectra\WFR8-6-FP500-C1_temp_20oC_current_20_1670942044.1359265_res_0.05nm.npz"
# data = np.load(rf"{path}")
# try:
#     wavelength = data["arr_0"]
#     power = data["arr_1"]
#     header = data["arr_2"]
# except KeyError:
#     # print("Headers updated")
#     wavelength = data["wavelength"]
#     power = data["power"]
#     header = data["setup_information"]
#
# print(len(wavelength))
# important_wavelength = wavelength[1500:3500]
# important_power = power[1500:3500]
#
# peaks, _ = signal.find_peaks(important_power, height=-35, prominence=5)
# # --- since we are looking at combs, the peak separation should be a constant value
# peak_separation = float(np.mean(np.diff(important_wavelength[peaks])))
# print(peaks)
# print(peak_separation)
#
# # plt.plot(wavelength, power, linewidth = 0.5)
# plt.plot(important_wavelength, important_power, linewidth = 0.5)
# plt.plot(important_wavelength[peaks], important_power[peaks], "x")
# plt.title(f"Peak Separation = {peak_separation:.5f} nm")
# plt.xlabel("Wavelength (nm)")
# plt.ylabel("Power (dBm)")
# plt.ylim(-50, 10)
# plt.show()


# file_npz_1 = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_25_Duration_1_Time_1676646955.442234.npz"
# file_npz_2 = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_40_Duration_1_Time_1676647112.0160093.npz"
# file_npz_3 = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_50_Duration_1_Time_1676647308.2284498.npz"
# file_npz_4 = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_60_Duration_1_Time_1676647452.23679.npz"
# file_npz_5 = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_70_Duration_1_Time_1676647568.3612497.npz"
#
# data1 = np.load(file_npz_1)
# data2 = np.load(file_npz_2)
# data3 = np.load(file_npz_3)
# data4 = np.load(file_npz_4)
# data5 = np.load(file_npz_5)
#
# wl1 = data1["Wavelength"]
# wl2 = data2["Wavelength"]
# wl3 = data3["Wavelength"]
# wl4 = data4["Wavelength"]
# wl5 = data5["Wavelength"]
#
# pow1 = data1["Power"]
# pow2 = data2["Power"]
# pow3 = data3["Power"]
# pow4 = data4["Power"]
# pow5 = data5["Power"]
#
# print(f"Temp: 25, Mean Wavelngth: {np.mean(wl1)}+/-{np.std(wl1)}, Mean Power: {np.mean(pow1)}+/-{np.std(pow1)}")
# print(f"Temp: 40, Mean Wavelngth: {np.mean(wl2)}+/-{np.std(wl2)}, Mean Power: {np.mean(pow2)}+/-{np.std(pow2)}")
# print(f"Temp: 50, Mean Wavelngth: {np.mean(wl3)}+/-{np.std(wl3)}, Mean Power: {np.mean(pow3)}+/-{np.std(pow3)}")
# print(f"Temp: 60, Mean Wavelngth: {np.mean(wl4)}+/-{np.std(wl4)}, Mean Power: {np.mean(pow4)}+/-{np.std(pow4)}")
# print(f"Temp: 70, Mean Wavelngth: {np.mean(wl5)}+/-{np.std(wl5)}, Mean Power: {np.mean(pow5)}+/-{np.std(pow5)}")

# file = r"C:\Users\ChrisMcGuinness\Downloads\Mode_map_3-29-F11_pasted_1675789692.4549298.npz"
# data = np.load(file)
# # for i in data:
# #     print(data[i])
#
# map = data["w_arr"][0]
# print(map)
#
# fig = plt.figure()
# norm = plt.Normalize(vmin=1282, vmax=1322, clip=True)
# plot = plt.imshow(map, norm=norm, origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
# cbar = fig.colorbar(plot)
# plt.xlabel('Tuning Voltage Ring 1 (V)')
# plt.ylabel('Tuning Voltage Ring 2 (V)')
# plt.show()
# fig.savefig(r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Pictures\O-Band Map.png", dpi=900)

# file1 = r"C:\Users\ChrisMcGuinness\Downloads\Current_100_Temp_25.csv"
# file2 = r"C:\Users\ChrisMcGuinness\Downloads\Current_80_Temp_25.csv"
# file3 = r"C:\Users\ChrisMcGuinness\Downloads\Current_60_Temp_25.csv"
# # file2 = r"C:\Users\ChrisMcGuinness\Downloads\Current_100_Temp_40.csv"
# # file3 = r"C:\Users\ChrisMcGuinness\Downloads\Current_100_Temp_50.csv"
# # file4 = r"C:\Users\ChrisMcGuinness\Downloads\Current_100_Temp_60.csv"
#
# df1 = pd.read_csv(file1, header=None)
# print(df1)
# # c1 = df1["Current (mA)"]
# w1 = df1[0]
# p1 = df1[1]
#
# df2 = pd.read_csv(file2, header=None)
# # c2 = df2["Current (mA)"]
# w2 = df2[0]
# p2 = df2[1]
#
# df3 = pd.read_csv(file3, header=None)
# # c3 = df3["Current (mA)"]
# w3 = df3[0]
# p3 = df3[1]
#
# # df4 = pd.read_csv(file4, header=None)
# # # c4 = df4["Current (mA)"]
# # w4 = df3[0]
# # p4 = df3[1]
#
# # df5 = pd.read_csv(file5, sep=',')
# # c5 = df5["Current (mA)"]
# # v5 = df5["Voltage (V)"]
# # p5 = df5["Power (uW)"]
#
#
# # plt.plot(w1, p1, label='25')
# # plt.plot(w2, p2, label='40')
# # plt.plot(w3, p3, label='50')
# plt.plot(w1, p1, label='Current 100')
# plt.plot(w2, p2, label='Current 80')
# plt.plot(w3, p3, label='Current 60')
# # plt.plot(w4, p4, label='60')
# # plt.plot(c5, p5, label='70')
# plt.title('Power Comparison Plot with Current')
# plt.xlabel('Wavelength')
# plt.ylabel('Optical Power (dBm)')
# # plt.ylabel('Voltage (V)')
# plt.legend(loc="best")
# plt.ylim(-75, -45)
# plt.show()
# # fig, ax1 = plt.subplots()
# # ax2 = ax1.twinx()
# # ax1.plot(c5, p5, 'b-', label='Optical Power')
# # ax2.plot(c5, v5, 'g-', label='Voltage')
# # plt.title('LIV Plot 70°C')
# # ax1.set_xlabel('Input Current (mA)')
# # ax1.set_ylabel('Optical Power (uW)')
# # ax2.set_ylabel('Voltage (V)')
# # ax1.legend(bbox_to_anchor=(0.95, 0.2))
# # ax2.legend(bbox_to_anchor=(0.95, 0.3))
# # plt.show()
#
# # file = r"C:\Users\ChrisMcGuinness\Downloads\RF Beat Tone.npz"
# # data = np.load(file)
# # # for i in data:
# # #     print(i)
# # f = (data["Frequency"]/1000)
# # p = data["Power"]
# #
# # fig = plt.figure()
# # plt.plot(f, p)
# # # plt.title("Frequency vs Power")
# # plt.ylabel("Power (dBm)")
# # plt.xlabel("Frequency (GHz)")
# # # plt.xticks([12.49995, 12.499975, 12.5, 12.500025, 12.50005])
# # # plt.xticks([6.24995, 6.249975, 6.25, 6.250025, 6.25005])
# # # plt.xticks([-0.00005, -0.000025, 0, 0.000025, 0.00005])
# # plt.show()
# # fig.savefig(r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Pictures\Updated_RF Beat Tone.png", dpi=500)

# --- Subtracting room temperature at time of TEC power measurements
# def quadratic(x, a, b, c):
#     return a*(x-21)**2 + b*(x-21) + c
#
#
# temperature = [25, 40, 50, 60, 70]
# # temperature = [0, 15, 25, 35, 45]
# gain_power = [144.6, 145.2, 145.7, 146.2, 146.7]
# tec_power = [7.26, 18.48, 66.88, 135.45, 217.89]
# total_power = [gain_power[i] + tec_power[i] for i in range(len(temperature))]
#
# popt, pcov = curve_fit(quadratic, temperature, total_power)
# fit_x = np.linspace(24, 71, 101)
# fit_y = quadratic(fit_x, *popt)
# _sigma_fit = np.sqrt(np.diagonal(pcov))
# _bound_upper = quadratic(fit_x, *(popt + _sigma_fit))
# _bound_lower = quadratic(fit_x, *(popt - _sigma_fit))
#
# plt.plot(temperature, gain_power, label="Gain")
# plt.plot(temperature, tec_power, label="TEC")
# plt.plot(temperature, total_power, label="Total")
# plt.plot(fit_x, fit_y, label="Fitting")
# plt.fill_between(fit_x, _bound_lower, _bound_upper, color='r', alpha=0.3, label="Fitting Error")
# plt.legend(loc="best")
# plt.xlabel("TEC Temperature (°C)")
# plt.ylabel("Power (mW)")
# plt.title("Pilot 1-2 Power Consumption at 21°C Room Temperature")
# plt.show()

# # file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\LoLiTuLa Tunability Testing\Temp_25_Current_100_1677853226.611112.npz"
# file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Pilot 1-2 Further Testing\Spectra\Map_Oven_Temp_70_TEC_Temp_50_Current_100_1679916615.5132346.npz"
#
# data = np.load(file1)
# P = data["Data"][1] + 4.4
#
# # fig = plt.figure(figsize=(17, 5))
# # plt.subplot(131)
# # # norm1 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
# # plot1 = plt.imshow(data["Data"][0], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
# # cbar = fig.colorbar(plot1)
# # plt.title("Wavelength")
# # plt.xlabel("Ring 1 (V)")
# # plt.ylabel("Ring 2 (V)")
# # plt.subplot(132)
# # # norm2 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
# # plot2 = plt.imshow(P, origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
# # cbar = fig.colorbar(plot2)
# # plt.title("Power")
# # plt.xlabel("Ring 1 (V)")
# # plt.ylabel("Ring 2 (V)")
# # plt.subplot(133)
# # # norm3 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
# # plot3 = plt.imshow(data["Data"][2], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
# # cbar = fig.colorbar(plot3)
# # plt.title("SMSR")
# # plt.xlabel("Ring 1 (V)")
# # plt.ylabel("Ring 2 (V)")
# # plt.show()
#
# plt.hist(P, bins=10)
# plt.title(r"Power Histogram 50$^\circ$C: 10 Bins")
# plt.xlabel("Power dBm")
# plt.ylabel("Occurrences")
# plt.xlim(-30, -8)
# plt.show()

# mask = np.zeros(data["Data"][2].shape)
# _mask = np.zeros(data["Data"][2].shape)
# power = np.argwhere(P > -20)
# smsr = np.argwhere(data["Data"][2] > 30)
#
# for i in power:
#     mask[i[0], i[1]] += 1
# for j in smsr:
#     mask[j[0], j[1]] += 1
# plt.imshow(mask)
# plt.show()
#
# points = np.argwhere(mask == 2)
# wavelengths = []
# for i in points:
#     _mask[i[0], i[1]] = data["Data"][0][i[0], i[1]]
#     wavelengths.append(data["Data"][0][i[0], i[1]])
#
# text = f"Longest: {np.max(wavelengths):.3f} nm, Shortest: {np.min(wavelengths):.3f} nm," \
#        f" Range: {np.max(wavelengths)-np.min(wavelengths):.3f} nm"
# print(text)
#
# fig = plt.figure()
# norm = plt.Normalize(vmin=np.min(wavelengths), vmax=np.max(wavelengths), clip=True)
# plot = plt.imshow(_mask, norm=norm, origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
# cbar = fig.colorbar(plot)
# plt.title(text)
# plt.xlabel("Ring 1 (V)")
# plt.ylabel("Ring 2 (V)")
# plt.show()

# size = 5
# _array = np.zeros((size, size))
# for i in range(size):
#     for j in range(size):
#         if i != 0 or j != 0:
#             _array[i][j] += i+j
#         # if j != 0:
#         #     _array[i][j] += 1
#
# print(_array)
# print(np.gradient(_array))

########################################################################################################################

# # test_file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\TestSweep_7.npz"
# # test_file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\TestSweep_8.npz"
# # test_file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\PolarizerSweep_1551.85_to_1553.55.npz"
# data_file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\SmallestRingSweep_1558_to_1560_1683649390.0798056.npz"
# # data_file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\LargestRingSweep_1553_to_1563_1683645335.3657308.npz"
# # data_file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\PolarizerSweep_1553_to_1558.npz"
# # data0 = np.load(file0)
# # data0 = data0["arr_0"]#.astype(float)
# # data1 = np.load(file1)
# # data1 = data1["arr_0"]#.astype(float)
# # test = np.load(test_file)
# # test = test["arr_0"]#.astype(float)
# data = np.load(data_file)
# data = data["arr_0"]#.astype(float)
# print(data)
# # print(data)
# tx = []
# ty = []
# dx = []
# dy = []
#
# for i in range(len(data)):
#     # x.append(data0[i,0])
#     # y.append((data0[i,1]-data1[i,1]))
#     # print(data[i,0], data[i,1])
#     # plt.scatter(data[i,0], data[i,1])
#     # plt.ylim(-75, -20)
#     # tx.append(test[i, 0])
#     # ty.append(test[i, 1] * 1000)
#     dx.append(data[i, 0])
#     dy.append(data[i, 1] * 1000)
#
# fig, ax1 = plt.subplots()
# # ax2 = ax1.twinx()
# ax1.plot(dx, dy, 'g-')
# # ax2.plot(tx, ty, 'b-')
# ax1.set_xlabel('X data')
# ax1.set_ylabel('Y1 data', color='g')
# # ax2.set_ylabel('Y2 data', color='b')
# plt.show()
#
# set_power = 6  # mw

########################################################################################################################

# def freq(wl):
#     c = 3e8
#     wl = wl*(1e-9)
#     return c/wl


# def ghz(hz):
#     return hz*(1e-9)


# wl1 = 1560.209
# wl2 = 1557.101
# print(ghz(freq(wl1)), ghz(freq(wl2)))
# print(ghz(freq(wl2))-ghz(freq(wl1)))

# r = 58.813056e-6
# df = 380e9
# c = 3e8
# print(c/(df*2*np.pi*r))

# from new_instrs import ILX_PM_8210 as 000ILX
# PM = ILX(1553, "dBm", "FAST")
# # PM.config()
# # PM = ILX("1550", "mW", "SLOW")
# print(PM.readPower())

# APEX = APEX_Loop.APEX()
# # APEX.trace_setup(1550.1, 1550.3)

# # p, wl = APEX.pull_trace()
# # APEX.plot_trace(wl, p)

# for i in range(1):
#     p, wl = APEX.pull_trace()
#     peak_wl, peak_p = APEX.find_peak(wl, p)
#     smsr = APEX.calculate_SMSR(p, threshold=-50)
#     print(peak_wl, peak_p, smsr)
#     # time.sleep(1)
#     APEX.plot_trace(wl, p)

# APEX.osa_disconnect()

I_o = 10  # mA
I_f = 50  # mA
I_step = 0.1  # mA
I = np.arange(I_o, I_f, I_step)

T_o = 14  # oC
T_f = 30  # oC
T_step = 1  # oC
T = np.arange(T_o, T_f, T_step)

file = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\DFB_Map_1689784618.067472.npz"
data = np.load(file)
print(data["Data"])
# fig = plt.figure(figsize=(17, 5))
# plt.subplot(131)
# plot1 = plt.imshow(data["Data"][0], origin='lower', extent=(10, 50, 14, 29), cmap='YlGnBu')
# cbar = fig.colorbar(plot1)
# plt.title("Wavelength")
# plt.xlabel("Current (I)")
# plt.ylabel("Temperature (oC)")
# plt.subplot(132)
# plot2 = plt.imshow(data["Data"][1], origin='lower', extent=(10, 50, 14, 29), cmap='YlGnBu')
# cbar = fig.colorbar(plot2)
# plt.title("Power")
# plt.xlabel("Current (I)")
# plt.ylabel("Temperature (oC)")
# plt.subplot(133)
# plot3 = plt.imshow(data["Data"][2], origin='lower', extent=(10, 50, 14, 29), cmap='YlGnBu')
# cbar = fig.colorbar(plot3)
# plt.title("SMSR")
# plt.xlabel("Current (I)")
# plt.ylabel("Temperature (oC)")
# plt.show()
# print(data["Data"][0])

x = []
y = []

for i in data["Data"][0]:
    for j in i:
        if 1557 < j < 1557.5:
            print(np.where(data["Data"][0] == j))

# for i in range(6, 35):
#     print(data["Data"][0][0,i])

def line(x, a, b):
    return a*x + b

for i in range(0, 113):
    print(f"Temp: {T[13]}, Current: {I[i]}, WL: {data['Data'][0][13,i]}")
    x.append(I[i])
    y.append(data['Data'][0][13,i])

print(x)
print(y)

popt, pcov = curve_fit(line, x, y)
fit_x = np.linspace(10, 22, 1001)
fit_y = line(fit_x, *popt)
_sigma_fit = np.sqrt(np.diagonal(pcov))
_bound_upper = line(fit_x, *(popt + _sigma_fit))
_bound_lower = line(fit_x, *(popt - _sigma_fit))

print(popt)

plt.plot(x, y, label="Data")
plt.plot(fit_x, fit_y, label="Fitting")
plt.fill_between(fit_x, _bound_lower, _bound_upper, color='r', alpha=0.3, label="Fitting Error")
plt.legend(loc="best")
plt.xlabel("Current (I)")
plt.ylabel("Wavelength (nm)")
# plt.title("Pilot 1-2 Power Consumption at 21°C Room Temperature")
plt.show()

# plt.plot(x, y)
# plt.show()
