import numpy as np
import time
import pandas as pd
import sys
sys.path.insert(1, r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\PythonScripts")
# from pi_instrs import ILX_PM_8210 as PM
from new_instrs import ILX_PM_8210 as PM

minutes = 1  # minutes

power = []
t = []

def main():
    power = PM(1550, "dBm","FAST")

    start = time.time()
    file_npz = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Stability_Test_{start}.npz"
    file_csv = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Stability_Test_{start}.csv"

    t_end = start + 60 * minutes
    print(start, t_end)
    while time.time() < t_end:
        power.append(power.readPower())
        t.append(time.time()-start)
        print(time.time()-start)

    np.savez(file=file_npz, Power=power, Time=t)
    d = {"Power": power, "Time": t}
    df = pd.DataFrame(data=d)
    df.to_csv(file_csv)
