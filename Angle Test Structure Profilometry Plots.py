import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from uncertainties import ufloat

# Ridge Top X
file1 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2_13_23\1_3um_Ridge_Top_X_Profile.csv"
# Slot Bottom X
file2 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2_13_23\1_3um_Slot_Bottom_X_Profile.csv"
# Ridge Top Y
file3 = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Profilometry\2_13_23\1_3um_Ridge_Top_Y_Profile.csv"

df1 = pd.read_csv(file1, sep=',', skiprows=3)
df2 = pd.read_csv(file2, sep=',', skiprows=3)
df3 = pd.read_csv(file3, sep=',', skiprows=3)
df1 = df1.fillna(0)
df2 = df2.fillna(0)
df3 = df3.fillna(0)
print(df1)

x1 = df1["Lateral(µm)"].to_numpy()
y1 = df1["Raw(µm)"].to_numpy()
plt.figure(figsize=(10,5))
plt.plot(x1, y1)
plt.title("1.3um Ridge Height")
top1 = 0
trench1 = -1.23
ridge = 0.14
plt.hlines((top1, trench1, ridge), xmin=20, xmax=45, linestyles='dashed', colors="red")
text = f"Top: {top1}um\nTrench: {trench1}um\nRidge Height: {abs(ridge-trench1):.2f}um"
plt.text(0, -0.5, text, fontsize=15)
plt.show()

x2 = df2["Lateral(µm)"].to_numpy()
y2 = df2["Raw(µm)"].to_numpy()
plt.plot(x2, y2)
plt.title("1.3um Slot Height")
top2 = 0
trench2 = -1.24
slot = -0.62
plt.hlines((top2, slot, trench2), xmin=20, xmax=45, linestyles='dashed', colors="red")
text = f"Top: {top2}um\nTrench: {trench2}um\nRidge Height: {abs(trench2-slot):.2f}um"
plt.text(0, -0.5, text, fontsize=15)
plt.show()

x3 = df3["Lateral(µm)"].to_numpy()
y3 = df3["Raw(µm)"].to_numpy()
plt.plot(x3, y3)
plt.title("1.3um Slot Spacing")
plt.vlines((7.1, 12.1, 17.1, 22.1, 27.1), ymin=-1.1, ymax=0.15, linestyles='dashed', colors="red")
text = "Average Spacing: 5um"
plt.text(0, -1.15, text, fontsize=15)
plt.show()
