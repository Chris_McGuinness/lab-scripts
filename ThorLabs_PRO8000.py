import pyvisa as visa


class PRO8000:
    def __init__(self, section, laser_current, tec_current, laser_current_start, laser_current_end):
        """
        Initialising variables and connecting to the ThorLabs PRO8000 via GPIB cable.
        """
        self.section = section
        self.laser_current = laser_current
        self.tec_current = tec_current
        self.laser_current_start = laser_current_start
        self.laser_current_end = laser_current_end
        rm = visa.ResourceManager()
        # need to double check GPIB address
        try:
            self.PRO = rm.open_resource("GPIB0::12::INSTR")
        except Exception as e:
            print("ThorLabs PRO8000 not connected")
            print(e)

        try:
            self.config()
        except Exception as e:
            print(e)

    def config(self):
        """
        Configuring the ThorLabs PRO8000 to set and read values from the device.
        """
        print(self.PRO.write("*IDN?;*CLS"))
        # --- Operation completed query

    # ------ Can't automate the TEC apparently
    # def set_TEC(self, section, tec_current):
    #     """
    #     Sets the tec current for a chosen section of the device.
    #     """
    #     self.PRO.write(f":SLOT {int(section)}")
    #     # --- Might need to use PORT instead of SLOT (Needs to be tested)
    #     # self.PRO.write(f":PORT {int(section)}")
    #     self.PRO.query("OPC?")

    def change_and_measure_current(self, laser_current_start, laser_current_end):
        """
        Takes current values in exponential form.
        """
        # --- Resets the device for a new measurement at the beginning of each step
        self.PRO.write(":ELCH:RESET 0")
        # --- Sets the start and end of each sweep. We intend to have many sweeps of 1 step.
        self.PRO.write(f":ILD:START {laser_current_start:.2e}")
        self.PRO.write(f":ILD:START {laser_current_end:.2e}")
        self.PRO.write(f":ILD:STEPS {2}")
        # --- Will take 1 measurement per step
        self.PRO.write(f":ELCH:MEAS {1}")

        # --- Sets the device to step when triggered
        self.PRO.write(":ELCH:RUN 2")
        current = (self.PRO.query(":ELCH:TRIG?"))
        # --- Terminate the run
        self.PRO.write(":ELCH:RUN 0")

        return current

