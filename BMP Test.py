import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps
from scipy import ndimage
import lmfit
from lmfit.lineshapes import gaussian2d, lorentzian, gaussian
from scipy.interpolate import griddata


def characterize_img(img):
    xb, yb = ndimage.measurements.center_of_mass(img)
    print(f"\nCentre = ({xb},{yb}), Amplitude = {img[int(xb),int(yb)]}")
    rv = np.zeros(img.size)
    wv = np.zeros(img.size)
    icount=0
    for ki in range(0, img.shape[0]):
        for kj in range(0, img.shape[1]):
            if img[ki, kj] < 0:
                continue
            r = np.sqrt((ki-xb)**2 + (kj-yb)**2)
            rv[icount] = r
            wv[icount] = img[ki,kj]
            icount = icount + 1

    idx = np.argsort(rv)
    rv = rv[idx]
    wv = wv[idx]
    cs = np.cumsum(wv)
    cs = cs/cs[-1]
    # eeidx = np.where(cs <= 0.5)
    eeidx = np.where(cs <= 0.135)
    _1_e2 = rv[eeidx][-1]
    # ee50=2*r50
    # print("\nEE50: {0:.2f} px (r50={1:.2f})".format(ee50, r50), end=' ')

    pixel_size = 20/96  # microns per pixel
    print(f"\nHEW = {_1_e2*pixel_size} microns")

    return _1_e2*pixel_size


# def gaussian_2d(x, y, amplitude=1., centerx=0., centery=0., sigmax=1., sigmay=1.,
#                  rotation=0):
#     """
#     Return a two dimensional gaussian.
#
#     The maximum of the peak occurs at ``centerx`` and ``centery``
#     with widths ``sigmax`` and ``sigmay`` in the x and y directions
#     respectively. The peak can be rotated by choosing the value of ``rotation``
#     in radians.
#     """
#     xp = (x - centerx)*np.cos(rotation) - (y - centery)*np.sin(rotation)
#     yp = (x - centerx)*np.sin(rotation) + (y - centery)*np.cos(rotation)
#     R = (xp/sigmax)**2 + (yp/sigmay)**2
#
#     return 2*amplitude*gaussian(R)/(np.pi*sigmax*sigmay)
def gaussian_2d(x, y, amplitude=1., centerx=0., centery=0., sigmax=1., sigmay=1.,rotation=0):
    xp = (x - centerx) * np.cos(rotation) - (y - centery) * np.sin(rotation)
    yp = (x - centerx)*np.sin(rotation) + (y - centery)*np.cos(rotation)
    R = (xp/sigmax)**2 + (yp/sigmay)**2
    X = (xp ** 2) / (2 * (sigmax ** 2))
    Y = (yp ** 2) / (2 * (sigmay ** 2))

    return amplitude*gaussian(R)#amplitude*np.exp(X + Y)/(np.pi*sigmax*sigmay)




def gaussian_fit(image):
    xn = []
    yn = []
    for i in range(p[:,:,1].shape[0]):
        for j in range(p[:,:,1].shape[1]):
            xn.append(i)
            yn.append(j)

    xn = np.asarray(xn)
    yn = np.asarray(yn)
    image = image.flatten()
    image = image/255
    # z = gaussian2d(x, y, amplitude=30, centerx=2, centery=-.5, sigmax=.6, sigmay=.8)
    # print(len(z))
    # model = lmfit.models.Gaussian2dModel()
    model = lmfit.Model(gaussian_2d, independent_vars=['x', 'y'])
    # params = model.guess(image, xn, yn)
    params = model.make_params(amplitude=255, centerx=xn[np.argmax(image)],
                               centery=yn[np.argmax(image)])
    params['rotation'].set(value=.1, min=0, max=np.pi / 2)
    params['sigmax'].set(value=1, min=0)
    params['sigmay'].set(value=2, min=0)
    result = model.fit(image, x=xn, y=yn, params=params)#z, x=x, y=y, params=params, weights=1 / error)
    lmfit.report_fit(result)


file = r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Pictures\XYScan_Image.bmp"
image = Image.open(file)
bw = np.asarray(ImageOps.grayscale(image))
p = np.asarray(image)

characterize_img(p[:,:,1])
gaussian_fit(p[:,:,1])
plt.imshow(p[:,:,1])
plt.show()

p_r = ndimage.rotate(p, -75, reshape=False, order=5)
characterize_img(p_r[:,:,1])
gaussian_fit(p_r[:,:,1])
plt.imshow(p_r[:,:,1])
plt.show()

# characterize_img(p)
# plt.imshow(p)
# plt.show()

# characterize_img(bw, 2)
# plt.imshow(bw)
# plt.show()

# npoints = 10000
# x = np.random.rand(npoints)*10 - 4
# y = np.random.rand(npoints)*5 - 3
# z = gaussian_2d(x, y, amplitude=30, centerx=2, centery=-.5, sigmax=.6,
#                  sigmay=1.2, rotation=45*np.pi/180)
# z += 2*(np.random.rand(*z.shape)-.5)
# error = np.sqrt(z+1)
#
# X, Y = np.meshgrid(np.linspace(x.min(), x.max(), 100),
#                    np.linspace(y.min(), y.max(), 100))
# Z = griddata((x, y), z, (X, Y), method='linear', fill_value=0)
#
# fig, ax = plt.subplots()
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# art = ax.pcolor(X, Y, Z, shading='auto')
# plt.colorbar(art, ax=ax, label='z')
# plt.show()
#
# model = lmfit.Model(gaussian_2d, independent_vars=['x', 'y'])
# params = model.make_params(amplitude=10, centerx=x[np.argmax(z)],
#                            centery=y[np.argmax(z)])
# params['rotation'].set(value=.1, min=0, max=np.pi/2)
# params['sigmax'].set(value=1, min=0)
# params['sigmay'].set(value=2, min=0)
#
# result = model.fit(z, x=x, y=y, params=params, weights=1/error)
# lmfit.report_fit(result)
