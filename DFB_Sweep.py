# import N7711A as TL
import numpy as np
import time
import HP68120C as WM
import ANDO_AQ6317B as ANDO
import sys
sys.path.insert(1, r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\PythonScripts")
from new_instrs import ILX_PM_8210 as PM
# from new_instrs import ldx_3207b
import matplotlib.pyplot as plt
import scipy.constants
# import APEX_Loop
import pyvisa as visa

rm = visa.ResourceManager()

# laser = TL.Agilent_TL()
# ILX = ldx_3207b()
# APEX = APEX_Loop.APEX()
power = PM(1555, "dBm","FAST")
# wavemeter = WM.HPWavemeter()

def current_setup():
    # LDC = rm.open_resource("GPIB0::1::INSTR")
    LDC = rm.open_resource("GPIB0::12::INSTR")
    LDC.clear()
    print("Current connected")
    time.sleep(0.2)
    LDC.write(f"LAS:LDI 0")
    LDC.write(f"LAS:OUT 1")
    return LDC

def TEC_setup():
    # LDC = rm.open_resource("GPIB0::1::INSTR")
    T = rm.open_resource("GPIB0::8::INSTR")
    T.clear()
    print("TEC connected")
    return T

def set_TEC(LDC, temp):
    LDC.write(f"TEC:T {temp}")
    LDC.write(f"TEC:OUT ON")

def check_temp(LDC, temp):
    _t = LDC.query(f"TEC:T?")
    while _t != temp:
        _t = LDC.query(f"TEC:T?")
        time.sleep(0.5)

def set_current(LDC, current):
    # time.sleep(0.05)
    LDC.write(f"LAS:LDI {current}")
    # time.sleep(0.2)
    # LDC.write(f"LAS:OUT ON")

def query_current(LDC):
    time.sleep(0.2)
    # LDC.write(f"CHAN {channel}")
    I = float(LDC.query("LAS:LDI?"))
    # I = 6
    return I

def current_shutdown(LDC):
    # LDC.write(f"CHAN {channel}")

    LDC.write(f"LAS:OUT OFF")
    LDC.write(f"LAS:I {0}")

def current_to_wl(I):
    # return I*(4.96273890e-02) + 1.55495401e+03
    return I*(4.13911892e-02) + 1.55662122e+03

"""
Current in the range 10.6-13.4 mA give a wavelength of 1555.48-1555.62 nm at 14 oC
"""
def main():
    """
    Initial conditions
    """
    # channel = 5
    T = 27
    I = np.arange(15, 15.3, 0.01)
    """
    DFB laser setup
    """
    # ILX.swap_channel(channel)
    # ILX.TEC_set_temp(T)
    # ILX.TEC_on()
    # ILX.LD_off()
    # ILX.LD_set_current(I[0])

    CC = current_setup()
    TEC = TEC_setup()

    set_TEC(TEC, T)
    set_current(CC, I[0])
    time.sleep(10)
    """
    Loops only the fine tuning
    """
    plt.ion()
    figure, ax1 = plt.subplots(figsize=(7, 5))
    ax1.ticklabel_format(useOffset=False)
    ax2 = ax1.twinx()
    ax1.set_ylim(-10, -2)
    ax2.set_ylim(-10, -2)
    plt.grid(True)
    
    try:
        k = 0
    # for j in range(1):
        p1 = np.zeros(len(I))
        w1 = np.zeros(len(I))
        p2 = np.zeros(len(I))
        w2 = np.zeros(len(I))
        while True:
            for i in range(len(I)):
                """
                Fine tuning loop. Performs pm steps.
                """
                # ILX.LD_set_current(I[i])
                set_current(CC, I[i])

                # d = wavemeter.read_power_and_wavelength()
                _p = power.readPower()
                # _w = current_to_wl(ILX.LD_read_current())
                # _w = current_to_wl(query_current(CC))
                _w = current_to_wl(I[i])
                # query current, then convert current to wl
                p2[i] = p1[i]
                w2[i] = w1[i]
                p1[i] = _p
                w1[i] = _w
                print(i)#_p, _w)
            if k == 0:
                spectrum1, = ax1.plot(w1, p1, 'r')
                spectrum2, = ax2.plot(w1, p1, 'b')
                # spectrum1.set_xdata(w1)
                spectrum1.set_ydata(p1)
                # spectrum2.set_xdata(w1)
                spectrum2.set_ydata(p1)
                figure.canvas.draw()
                figure.canvas.flush_events()
                print(f"Wavelength: {w1}")
                print(f"Power: {p1}")
                # time.sleep(0.2)
                # w2 = w1
                # p2 = p1
            else:
                # spectrum1, = ax1.plot(w1, p1, 'r')
                # spectrum2, = ax2.plot(w2, p2, 'b')
                # spectrum1.set_xdata(w1)
                spectrum1.set_ydata(p1)
                # spectrum2.set_xdata(w2)
                spectrum2.set_ydata(p2)
                figure.canvas.draw()
                figure.canvas.flush_events()
                # print(f"Wavelength: {w1}")
                # print(f"Power: {p1}")
                print(p1)
                # print(p2)
                # time.sleep(0.2)
                # w2 = w1
                # p2 = p1
            print(k)
            k += 1
            time.sleep(2)
    except KeyboardInterrupt:
        print("Interrupted Loop")
        time.sleep(0.2)
        # APEX.osa_disconnect()
        # time.sleep(0.2)
        # ILX.LD_off()
        # current_shutdown(CC)
        time.sleep(0.2)
        plt.show()

if __name__ == "__main__":
    start = time.time()
    main()
    end = time.time()
    print(f"Done in {(end-start)/60} minutes")
