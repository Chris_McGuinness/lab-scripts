import numpy as np
import matplotlib.pyplot as plt
import pyvisa as visa
import time

rm = visa.ResourceManager()


# --- Function to test ESA connection -------------
def ESA_connection_test(ESA_object):
    ESA_object.clear()
    # Asks the device what it is

    # this command does not work with this ESA
    print(ESA_object.write('*IDN?'))
    return ESA_object
# ------------------------------------------------


# --- Function to connect to the ESA -------------
def ESA_connection(ESA_address):
    print('Trying to connect to %s' % ESA_address)
    ESA_object = rm.open_resource(ESA_address)
    # ESA_connection_test(ESA_object)
    return ESA_object
# ------------------------------------------------


# --- Function to write the ESA for trace conditions before tracing data
def get_settings(ESA_object):
    sf = float(ESA_object.query(f"FA?"))
    ef = float(ESA_object.query(f"FB?"))
    cf = float(ESA_object.query(f"CF?"))
    sp = float(ESA_object.query(f"SP?"))
    rl = float(ESA_object.query(f"RL?"))
    au = ESA_object.query(f"AUNITS?")
    rb = float(ESA_object.query(f"RB?"))
    vb = float(ESA_object.query(f"VB?"))
    st = float(ESA_object.query(f"ST?"))
    settings = (sf, ef, cf, sp, rl, au, rb, vb, st)
    print(f"Settings are now: \nStart Frequency = {sf/1e6} MHZ \nEnd Frequency = {ef/1e6} MHZ "
          f"\nCentre Frequency = {cf/1e6} MHZ \nFrequency Span = {sp/1e6} MHZ "
          f"\nReference Level = {rl}dBm \nAmplitude Units = {au}Resolution Bandwidth = {rb/1e6}MHZ "
          f"\nVideo Bandwidth = {vb/1e6}MHZ \nSweep Time = {st}s \n")
    return settings
# ----------------------------------------------------------------------


# --- Function to set the trace conditions of the ESA --------------------------------
def frequency_range(ESA_object, start_frequency, end_frequency):
    ESA_object.write(f"FA {start_frequency}MHZ")
    ESA_object.write(f"FB {end_frequency}MHZ")
    setup = get_settings(ESA_object)
    # gives the ESA time to update before any data is taken
    time.sleep(2)
    return setup
# ------------------------------------------------------------------------------------


# --- Function to set the trace conditions of the ESA --------------------------------
def frequency_span(ESA_object, centre_wavelength, wavelength_span):
    ESA_object.write(f"CF {centre_wavelength}MHZ")
    ESA_object.write(f"SP {wavelength_span}MHZ")
    setup = get_settings(ESA_object)
    # gives the ESA time to update before any data is taken
    time.sleep(3)
    return setup
# ------------------------------------------------------------------------------------


# --- Function to change sweep time -----------
def sweep_time(ESA_object, sweep_time):
    """
    Takes ESA object and sweep time in seconds.
    Changes the sweep time of the ESA.
    """
    ESA_object.write(f"ST {sweep_time}")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ---------------------------------------------


# --- Function to change to continuous sweep --------------
def continuous_sweep(ESA_object):
    ESA_object.write("CONTS")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ---------------------------------------------------------


# --- Function to change to single sweep ------------------
def single_sweep(ESA_object):
    ESA_object.write("SNGLS")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ---------------------------------------------------------


# --- Function to full frequency range sweep --------------
def full_span(ESA_object):
    ESA_object.write("FS")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ---------------------------------------------------------


# --- Function to change log scale of the ESA -----------------
def log_scale(ESA_object, scale):
    """
    Takes an ESA object and a log scale in dB.
    Changes the log scaling of the ESA to the specified value.
    """
    ESA_object.write(f"LG {scale}DB")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ------------------------------------------------------------


# --- Function to change reference level of the ESA -----------------
def reference_level(ESA_object, level):
    """
    Takes an ESA object and a reference level in dB.
    Changes the reference level of the ESA to the specified value.
    """
    ESA_object.write(f"RL {level}DB")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ------------------------------------------------------------


# --- Function to change resolution bandwidth of the ESA -----
def resolution_bandwidth(ESA_object, rb):
    """
    Takes an ESA object and a resolution bandwidth in MHZ.
    Changes the log scaling of the ESA to the specified value.
    """
    ESA_object.write(f"RB {rb}MHZ")
    # gives the ESA time to update before any data is taken
    time.sleep(2)
# ------------------------------------------------------------


def power_bandwidth(ESA_object):
    """
        Takes an ESA object.
        Calculates the bandwidth at 99% of peak power,
        corresponding to -20dB.
    """
    ESA_object.write("OCCUP 99.0")
    bandwidth = ESA_object.query("PWRBW?")
    return bandwidth


# --- Function to take trace data and settings from the ESA trace A ---------------------------------
def trace_ESA_A(ESA_object):
    """
    Takes trace data from the ESA
    """
    # checks setting values are correct
    trace_settings = get_settings(ESA_object)
    # setup_analyzer(ESA_object, *trace_settings)
    trace_data = ESA_object.query("TDF P;TRA?")
    frequency_data = np.linspace(trace_settings[0]/1e6, trace_settings[1]/1e6, 601)
    return trace_data, frequency_data, trace_settings
# ---------------------------------------------------------------------------------------------------


# --- Function to take trace data and settings from the ESA trace B ---------------------------------
# def trace_ESA_B(ESA_object, centre_frequency, frequency_span):
#     """
#     Takes Centre Frequency and Frequency span in MHz
#     """
#     # gives setting values on the ESA
#     input_settings = ESA_object.write(f"IP;CF {centre_frequency}MHZ;SP {frequency_span}MHZ;SNGLS;TS")
#     # checks setting values are correct
#     trace_settings = get_settings(ESA_object)
#     trace_data = ESA_object.query("TDF P;TRB?")
#     # return np.array([trace_data]), trace_settings
#     return trace_data, trace_settings
# ---------------------------------------------------------------------------------------------------


def main():
    # print('Devices found:')
    # print(rm.list_resources())  # use this to figure out the address of the OSA
    # print('Connecting to OSA...')

    # --- CHANGE THIS TO MATCH YOUR OSA:
    ESA_address = "GPIB0::18::INSTR"
    # ----------------------------------

    # connects to the device and tests connection
    HP_ESA = ESA_connection(ESA_address)

    centre_frequency = 275  # MHz
    freq_span = 40  # MHz
    start_freq = 250  # MHz
    end_freq = 300  # MHz
    sweep_length = 1  # S
    axis_log_scale = 10  # DB
    res_bandwidth = 0.3  # MHZ
    ref_level = 8  # DBM

    sweep_time(HP_ESA, sweep_length)
    # single_sweep(HP_ESA)
    continuous_sweep(HP_ESA)
    log_scale(HP_ESA, axis_log_scale)
    resolution_bandwidth(HP_ESA, res_bandwidth)
    reference_level(HP_ESA, ref_level)
    get_settings(HP_ESA)

    print(f"Bandwidth at 99% power: {power_bandwidth(HP_ESA)}")

    # set frequency
    # frequency_span(HP_ESA, centre_frequency, freq_span)
    # frequency_range(HP_ESA, start_freq, end_freq)
    # full_span(HP_ESA)

    # take data
    # raw_data, trace_freq, settings = trace_ESA_A(HP_ESA)
    # reduced_data = []
    # for i in raw_data.split(","):
    #     reduced_data.append(float(i))
    #
    # plt.plot(trace_freq, reduced_data)
    # plt.show()

if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()

    print(f'Done in {end - start} seconds')
