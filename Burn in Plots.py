import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def read_map_data_excel(file , sheet_name):
    '''
    Creates pandas dataframe from Excel file.
    Takes file location as a string and Excel sheet name as a string.
    Returns mode map data as a numpy array.
    '''

    df = pd.read_excel(file, sheet_name=sheet_name)

    # create 257x257 array of zeros
    data = np.zeros((256, 256))

    # filling empty array with mode map data
    for i in range(len(df.index)):
        x = df['i'][i]
        y = df['j'][i]
        wavelength = df['Wavelength (nm)'][i]
        data[x, y] = wavelength

    return data


def read_map_data_csv(file):
    '''
    Creates pandas dataframe from .csv file.
    Takes file location as a string.
    Returns mode map data as a numpy array.
    '''

    df = pd.read_csv(file, sep='\t')

    # create 257x257 array of zeros
    data = np.zeros((256, 256))

    # filling empty array with mode map data
    for i in range(len(df.index)):
        x = df['i'][i]
        y = df['j'][i]
        wavelength = df['Wavelength (nm)'][i]
        data[x, y] = wavelength

    return data

def read_files(files):
    '''
    Takes a list of strings, where each string is a file location.
    Returns a list containing numpy arrays, where each array is contains mode map data.
    '''

    data = []

    # only the first of the data files should contain an Excel file. The rest should be in .csv format
    for i in range(len(files)):
        if i == 0:
            data.append(read_map_data_excel(files[i], sheet_name='Pilot 1-3 Phase 33142 Counts_5 '))
        else:
            data.append(read_map_data_csv(files[i]))

    return data

def wavelength_change(data, test_duration):
    '''
    Takes a list of mode maps and a list of burn-in test durations.
    Returns a list of mode maps showing the change in wavelength for each pixel in pm/hour
    '''

    mode_map_difference = []

    for i in range(1, len(data)):
        mode_map_difference.append(((data[i] - data[i-1])/ test_duration[i-1]) * 1000)

    return mode_map_difference


def plot_data(data, oven_time):
    '''
    Takes a list containing numpy data arrays and a list with time spent in the oven for each mode map.
    Plots each array with matplotlib.pyplot.imshow(), with burn-in time as a title.
    '''

    for i in range(len(data)):
        fig = plt.figure()
        norm = plt.Normalize(vmin=1530, vmax=1575, clip=True)
        plot = plt.imshow(data[i], norm=norm, extent=(0, -10, 0, -10), origin='lower')
        fig.colorbar(plot)
        plt.title(rf'Wavelength Map: With {oven_time[i]} Hour Burn-In')
        plt.xlabel('Tuning Voltage Ring 1 (V)')
        plt.ylabel('Tuning Voltage Ring 2 (V)')
        plt.show()

def plot_mode_map_difference(mode_map_difference, test_duration):
    '''
    Takes a list containing difference between mode maps and a list of burn-in duration between mode maps.
    Plots difference between arrays with matplotlib.pyplot.imshow(), with both burn-in times in the title.
    '''

    for i in range(len(mode_map_difference)):
        fig = plt.figure()
        norm = plt.Normalize(vmin=-50, vmax=50, clip=True)
        plot = plt.imshow(mode_map_difference[i], norm=norm, extent=(0, -10, 0, -10), origin='lower', cmap="bwr")
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        cbar = fig.colorbar(plot)
        cbar.set_label('Wavelength Change (pm/Day)', fontsize=15)
        plt.title(rf'$\Delta$ $\lambda$ (pm/hour): Difference between'
                  rf' {test_duration[i]} and {test_duration[i+1]} Hour Burn-In')
        plt.xlabel('Tuning Voltage Ring 1 (V)', fontsize=15)
        plt.ylabel('Tuning Voltage Ring 2 (V)', fontsize=15)
        if i == 0 or i ==4:
            fig.savefig(rf'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Pictures\Diff_Map_{i}',
                            dpi=1200)
        plt.show()

def plot_coordinate_values(data, total_oven_time):
    '''
    Takes list of mode map data and a list of total burn-in time for each mode map.
    Makes two plots. First, plots the absolute wavelength drift of each coordinate.
    Second, plots absolute drift of two points with high resolution
    '''

    no_of_tests = len(data)

    _50_75 = np.zeros(no_of_tests)
    _100_130 = np.zeros(no_of_tests)
    _135_200 = np.zeros(no_of_tests)
    _150_110 = np.zeros(no_of_tests)
    _190_70 = np.zeros(no_of_tests)
    _75_20 = np.zeros(no_of_tests)

    for i in range(no_of_tests):
        _50_75[i] = data[i][75, 50]
        _100_130[i] = data[i][130,100]
        _135_200[i] = data[i][200,135]
        _150_110[i] = data[i][110,150]
        _190_70[i] = data[i][70,190]
        _75_20[i] = data[i][20,75]

    plt.figure()
    plt.title('Wavelength vs Time for chosen points')
    plt.plot(total_oven_time, _50_75)
    plt.plot(total_oven_time, _100_130)
    plt.plot(total_oven_time, _135_200)
    plt.plot(total_oven_time, _150_110)
    plt.plot(total_oven_time, _190_70)
    plt.plot(total_oven_time, _75_20)
    plt.ylabel(r'$\lambda$ (nm)')
    plt.xlabel('Time (hours)')
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(total_oven_time, _50_75)
    plt.ylim(1571.7, 1571.77)
    plt.title('Point (50, 75)')
    plt.ylabel(r'$\lambda$ (nm)')
    plt.xlabel('Time (hours)')
    plt.subplot(122)
    plt.ylim(1542.6, 1542.72)
    plt.title('Point (150, 110)')
    plt.plot(total_oven_time, _150_110)
    plt.ylabel(r'$\lambda$ (nm)')
    plt.xlabel('Time (hours)')
    plt.show()


def plot_normalized_wavelength_change(mode_map_difference, tests_oven_time, test_duration, sigma1, sigma2):
    '''
    Takes list of mode map data and a list of total burn-in time for each mode map, total oven time and uncertainties.
    Makes three plots. First, plots the relative wavelength drift per hour between tests for each coordinate.
    Second, plots the relative wavelength drift per day between tests for each coordinate.
    Third, plots absolute drift of two points with high resolution
    '''

    days_in_oven = tests_oven_time / 24

    no_of_tests = len(mode_map_difference)
    sigma = np.zeros(no_of_tests)
    _50_75 = np.zeros(no_of_tests)
    _100_130 = np.zeros(no_of_tests)
    _135_200 = np.zeros(no_of_tests)
    _150_110 = np.zeros(no_of_tests)
    _190_70 = np.zeros(no_of_tests)
    _75_20 = np.zeros(no_of_tests)

    _50_75_day = np.zeros(no_of_tests)
    _100_130_day = np.zeros(no_of_tests)
    _135_200_day = np.zeros(no_of_tests)
    _150_110_day = np.zeros(no_of_tests)
    _190_70_day = np.zeros(no_of_tests)
    _75_20_day = np.zeros(no_of_tests)

    for i in range(no_of_tests):
        sigma[i] = error(sigma1, sigma2, test_duration[i])
        _50_75[i] = mode_map_difference[i][75, 50]
        _100_130[i] = mode_map_difference[i][130, 100]
        _135_200[i] = mode_map_difference[i][200, 135]
        _150_110[i] = mode_map_difference[i][110, 150]
        _190_70[i] = mode_map_difference[i][70, 190]
        _75_20[i] = mode_map_difference[i][20, 75]

        _50_75_day[i] = mode_map_difference[i][75, 50] * 24
        _100_130_day[i] = mode_map_difference[i][130, 100] * 24
        _135_200_day[i] = mode_map_difference[i][200, 135] * 24
        _150_110_day[i] = mode_map_difference[i][110, 150] * 24
        _190_70_day[i] = mode_map_difference[i][70, 190] * 24
        _75_20_day[i] = mode_map_difference[i][20, 75] * 24

    plt.figure()
    plt.title('Wavelength Change per hour for chosen points')
    plt.plot(tests_oven_time, _50_75)
    plt.plot(tests_oven_time, _100_130)
    plt.plot(tests_oven_time, _135_200)
    plt.plot(tests_oven_time, _150_110)
    plt.plot(tests_oven_time, _190_70)
    plt.plot(tests_oven_time, _75_20)
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / hour)')
    plt.xlabel('Time (hour)')
    plt.show()

    plt.figure()
    plt.title('Wavelength Change per day for chosen points')
    plt.plot(days_in_oven, _50_75_day)
    plt.plot(days_in_oven, _100_130_day)
    plt.plot(days_in_oven, _135_200_day)
    plt.plot(days_in_oven, _150_110_day)
    plt.plot(days_in_oven, _190_70_day)
    plt.plot(days_in_oven, _75_20_day)
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / day)')
    plt.xlabel('Time (day)')
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(tests_oven_time, _50_75)
    plt.errorbar(tests_oven_time, _50_75, yerr=sigma, capsize=4)
    # plt.ylim(-0.1, 0.11)
    plt.title('Point (50, 75)')
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / hour)')
    plt.xlabel('Time (hours)')
    plt.subplot(122)
    # plt.ylim(-0.1, 0.11)
    plt.title('Point (150, 110)')
    plt.plot(tests_oven_time, _150_110)
    plt.errorbar(tests_oven_time, _150_110, yerr=sigma, capsize=4)
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / hour)')
    plt.xlabel('Time (hours)')
    plt.show()


def error(sigma1, sigma2, t):
    return np.sqrt( ( (-1000/t)*sigma1 )**2 + ( (-1000/t)*sigma2 )**2 )


def main():
    ### Naming all files to be used
    file0 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5 volts.map.xlsx'
    file1 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_after_3Day_burn_in.map'
    file2 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_after_7_Day_burn_in.map Phase 65535 Counts_9.3 volts.map'
    file3 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July8.map'
    file4 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July12.map'
    file5 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July15.map'
    file6 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July18.map'
    file7 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July20.map'
    file8 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July25.map'
    file9 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July28.map'
    file10 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_July29.map'
    file11 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August02.map'
    file12 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August04.map'
    # file13 has no additional time spent in the oven
    # file13 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August05.map'

    ### Listing all files and defining time values for each test
    # files_Aug05 = (file0, file1, file2, file3, file4, file5, file6, file7,
    #          file8, file9, file10, file11, file12, file13)
    # # file13 has no additional time spent in the oven so the time difference between mode maps is given
    # total_oven_time_Aug05 = [0, 89.9, 203.8, 270.9, 363.8, 411.15, 476.62, 500.45,
    #                    570.93, 637.78, 657.98, 753.22, 797.33, 819.74]
    # tests_oven_time_Aug05 = np.array([89.9, 203.8, 270.9, 363.8, 411.15, 476.62, 500.45,
    #                             570.93, 637.78, 657.98, 753.22, 797.33, 819.74])
    # test_duration_Aug05 = np.array([89.9, 113.9, 67.1, 92.9, 47.35, 65.47, 23.83,
    #                           70.48, 66.85, 20.20, 95.24, 44.11, 22.41])

    files = (file0, file1, file2, file3, file4, file5, file6, file7,
             file8, file9, file10, file11, file12)
    # file13 has no additional time spent in the oven so the time difference between mode maps is given
    total_oven_time = [0, 89.9, 203.8, 270.9, 363.8, 411.15, 476.62, 500.45,
                       570.93, 637.78, 657.98, 753.22, 797.33]
    tests_oven_time = np.array([89.9, 203.8, 270.9, 363.8, 411.15, 476.62, 500.45,
                                570.93, 637.78, 657.98, 753.22, 797.33])
    test_duration = np.array([89.9, 113.9, 67.1, 92.9, 47.35, 65.47, 23.83,
                              70.48, 66.85, 20.20, 95.24, 44.11])
    # coords = ([75, 50], [130,100], [200,135], [110,150], [70,190], [20,75])
    # error value for the Bristol 871B Wavemeter is 0.0008nm at 1000nm
    sigma1 = 0.0011625  # nm
    sigma2 = 0.0011625  # nm

    ### Running functions to analyse and plot all data
    data = read_files(files)
    mode_map_difference = wavelength_change(data, test_duration)

    plot_data(data, total_oven_time)
    plot_mode_map_difference(mode_map_difference, total_oven_time)
    plot_coordinate_values(data, total_oven_time)
    plot_normalized_wavelength_change(mode_map_difference, tests_oven_time, test_duration, sigma1, sigma2)

if __name__ == '__main__':
    main()

    print('Done!')
