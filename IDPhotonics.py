import time
import numpy as np
from decimal import Decimal
from datetime import datetime
import pyvisa as visa
# import matplotlib
# matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.animation import FuncAnimation

# TODO: docString for main
# TODO: docString for ID_OSA class
# TODO: Continuous Scan Graph
# TODO: Plotting Method


class OSA:
    """ OSA Class for ID Photonics OSA """

    def __init__(self, ip="10.100.3.100", com=None, port="HighSens", tmout=5000, termination=";"):
        """

        :param ip: str; specifies the IP address of the device for connection over IP; default "10.100.3.100"
        :param comm: int; specifies the COMPort of the device for connection over USB; default None; overrides IP conn if set
        :param port: str; specifies the Optical Input Port, one of "HighSens", "LowPower", "StandardSens", "HighPower"
        :param tmout: int; specifies the device communication timeout in milliseconds; default 5000
        :param termination: str; specifies device read termination; default ";"
        """
        self.ip_address = ip
        self.comm_port = com
        self.terminator = termination
        rm = visa.ResourceManager('@py')
        if self.comm_port:
            print(f'Initializing ID Photonics OSA at COM{self.comm_port}')
            self.visa_resource_str = f'ASRL{self.comm_port}::INSTR'
        else:
            self.visa_resource_str = f'TCPIP::{self.ip_address}::2000::SOCKET'
            print(f'Initializing ID Photonics OSA at {self.ip_address}')
        try:
            self.dev = rm.open_resource(self.visa_resource_str, read_termination=self.terminator)
        except Exception as Exc:
            print('\nERROR: Device initialization failed\nCheck COM Port Number/IP Address & Device is powered on\n')
            raise Exc
        self.dev.timeout = tmout
        if port in ["HighSens", "LowPower", "StandardSens", "HighPower"]:
            self.setPort(port=port)
        else:
            self.setPort()
        self.x_data_array = None
        self.y_data_array = None
        print("ID Photonics Device Initialized\n")

    def error_catch(self):
        bool_ = False
        data = self.dev.read()
        # print(data)

        if "ERR" in data:
            data = None
        else:
            bool_ = True
        return bool_, data

    def query_format(self):
        query_bool = False
        self.dev.write("form?;")
        while not query_bool:
            query_bool, query_result = self.error_catch()
            print("Queried format", query_bool)
            time.sleep(0.5)
        print(query_result)

    def query_scan_number(self):
        scan_bool = False
        self.dev.write("numb?;")
        while not scan_bool:
            scan_bool, scan_number = self.error_catch()
            print("Scan number queried", scan_bool)
            time.sleep(0.5)
        # print(self.scan_number)
        return scan_number

    def setStartStopFreq(self, start, stop):
        """ Sets start and stop frequency of the OSA in THz. """
        # center = (start+stop)*1./2
        # span=stop-start
        # self.setCenterSpanFreq(center, span)

        # Minimum start freq for ID OSA is 191.25THz
        if start < 191.25:
            start = 191.25

        start = "{:.5E}".format(Decimal(str(start * 1e12)))
        stop = "{:.5E}".format(Decimal(str(stop * 1e12)))

        start_bool = False
        self.dev.write(f"star {start};")
        while not start_bool:
            start_bool, start_result = self.error_catch()
            print("Setting start wavelength", start_bool)
            time.sleep(0.5)

        stop_bool = False
        self.dev.write(f"stop {stop};")
        while not stop_bool:
            stop_bool, stop_result = self.error_catch()
            print("Setting stop wavelength", stop_bool)
            time.sleep(0.5)

    def setCenterSpanFreq(self, center, span):
        """ Sets Center frequency and span of the OSA in THz. """
        start = center - (span * 1. / 2)
        stop = center + (span * 1. / 2)
        self.setStartStopFreq(start, stop)

    def setStartStopWL(self, startWLnm, stopWLnm):
        """ Sets start and stop wavelength of the OSA in nm. """
        c = 299792458.0  # speed of light
        start = c / stopWLnm / 1000
        stop = c / startWLnm / 1000
        center = (start + stop) * (1. / 2)
        span = stop - start
        self.setCenterSpanFreq(center, span)

    def setCenterSpanWL(self, centerWLnm, spanWLnm):
        """ Sets center wavelength and span of the OSA in nm. """
        c = 299792458.0  # speed of light
        stopWLnm = centerWLnm + spanWLnm * 1. / 2
        startWLnm = centerWLnm - spanWLnm * 1. / 2
        start = c / stopWLnm / 1000
        stop = c / startWLnm / 1000
        center = (start + stop) * 1. / 2
        span = stop - start
        self.setCenterSpanFreq(center, span)

    def single_sweep(self):
        self.dev.write("SGL;")
        single_sweep_bool = False
        while not single_sweep_bool:
            single_sweep_bool, single_sweep_result = self.error_catch()
            print("Swept", single_sweep_bool)
            time.sleep(0.5)

    def spectrum_pull(self, refresh=False):
        if refresh is True:
            self.single_sweep()

        x_bool = False
        y_bool = False

        self.dev.write("X?;")
        while not x_bool:
            x_bool, x_data = self.error_catch()
            print("Swept x", x_bool)
            time.sleep(0.5)

        self.dev.write("Y?;")
        while not y_bool:
            y_bool, y_data = self.error_catch()
            print("Swept y", y_bool)
            time.sleep(0.5)

        self.x_data_array = x_data.split(",")
        self.x_data_array = np.asarray(self.x_data_array, dtype=float)

        self.y_data_array = y_data.split(",")
        self.y_data_array = np.asarray(self.y_data_array, dtype=float)

        return self.x_data_array, self.y_data_array

    def setPort(self, port="HighSens"):
        if port == "HighSens" or port == "LowPower":
            print("High sensitivity port selected")
            self.dev.write("inpu 2;")
            high_bool = False
            while not high_bool:
                high_bool, high_result = self.error_catch()
                print("High port", high_bool)
                time.sleep(0.5)
        elif port == "StandardSens" or port == "HighPower":
            print("Standard sensitivity port selected")
            self.dev.write("inpu 1;")
            standard_bool = False
            while not standard_bool:
                standard_bool, standard_result = self.error_catch()
                print("Standard port", standard_bool)
                time.sleep(0.5)
        else:
            print('Error: please select "HighSens"/"LowPower" or "StandardSens"/"HighPower')

    def findMaxPowerWL(self, rounding=True, spectrum=None):
        """ Returns Max power in dBm, Wavelength in nm

        :param spectrum: optional, pass results of previous spectrum_pull() as tuple/list
        :param rounding=True Rounds to 4 decimal places, disable by setting to false
        :returns wavelength in nm (float); max power in dBm (float)
        """
        if isinstance(spectrum, (tuple, list)) and len(spectrum) == 2:
            print('INFO: Using passed spectrum')
            x, y = spectrum[0], spectrum[1]
        else:
            x, y = self.spectrum_pull(refresh=True)
        try:
            power = np.max(y)
            wavelength = x[np.argmax(y)] * 1e9
            if rounding:
                wavelength = round(wavelength, 4)
            return wavelength, power
        except Exception as Exc:
            print(Exc)
            return None, None

    def save_to_csv(self, filename='idp_osa_saved_spectrum', refresh=True):
        if not isinstance(filename, str):
            raise TypeError('Argument filename must be a string')
        now = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        fname = f'{filename}_{now}.csv'
        try:
            x, y = self.spectrum_pull(refresh=refresh)
            pd.DataFrame(np.stack((x * 1e9, y), axis=1), columns=['wavelength', 'power']).to_csv(fname, index=False)
        except Exception as e:
            print(e)
            print(f'\nERROR: Spectrum Data Not saved')
        print(f'\nINFO: Spectrum Data successfully saved to working directory as {fname}')

    def __continuous_display(self, mode="HighSens", start=1550, stop=1568):

        self.setPort(port=mode)
        self.setStartStopWL(start, stop)
        self.single_sweep()
        fig, ax = plt.subplots()

        def update_data(i):
            print(i)
            x, y = self.spectrum_pull()
            ax.clear()
            ax.plot(x*1e9, y)

        ani = FuncAnimation(fig, update_data, interval=200, blit=False)
        plt.draw()

    def __display_test(self, mode="HighSens", start=1553, stop=1563):
        self.setPort(port=mode)
        self.setStartStopWL(start, stop)
        self.single_sweep()
        fig, ax = plt.subplots()
