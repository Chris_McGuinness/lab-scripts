import numpy as np
import matplotlib.pyplot as plt
import pyvisa as visa
import time

rm = visa.ResourceManager()


# --- Function to connect to the OSA -------------
def OSA_connection():
    time.sleep(0.2)
    OSA_address = "GPIB0::8::INSTR"
    print('Trying to connect to %s' % OSA_address)
    OSA_object = rm.open_resource(OSA_address)
    # OSA_connection_test(OSA_object)
    return OSA_object
# ------------------------------------------------


# --- Function to test OSA connection -------------
def OSA_connection_test(OSA_object):
    time.sleep(1)
    OSA_object.clear()
    # Asks the device what it is

    # this command does not work with the ID Photonics OSA
    print(OSA_object.query('*IDN?'))
    return OSA_object
# ------------------------------------------------


# --- Function to set the light measurement mode to Continuous Wave mode
def continuous_wave_setting(OSA_object):
    time.sleep(1)
    OSA_object.write(f"CLMES")
    time.sleep(1.5)
# ----------------------------------------------------------------------


# --- Function to set the light measurement mode to Pulsed mode
def pulsed_wave_setting(OSA_object):
    time.sleep(1)
    OSA_object.write(f"PLMES")
    time.sleep(1.5)
# ----------------------------------------------------------------------


# --- Function to make an auto sweep ------------------
def auto_sweep(OSA_object):
    time.sleep(1)
    OSA_object.write("ATCTR1")
    OSA_object.write("AUTO")
    # gives the OSA time to update before any data is taken
    time.sleep(1.5)
# ---------------------------------------------------------


# --- Function to make an single sweep ------------------
def single_sweep(OSA_object):
    time.sleep(1)
    OSA_object.write("SGL")
    # gives the OSA time to update before any data is taken
    # time.sleep(1.5)
    while int(OSA_object.query("SWEEP?")) != 0:
        time.sleep(0.1)
# ---------------------------------------------------------


# --- Function to change to continuous sweep --------------
def continuous_sweep(OSA_object):
    time.sleep(1)
    OSA_object.write("RPT")
    # gives the OSA time to update before any data is taken
    time.sleep(1.5)
# ---------------------------------------------------------


# --- Function to change sweep time -----------
def sweep_time(OSA_object, s_time):
    """
    Takes OSA object and sweep time in seconds from 0 to 50.
    Changes the sweep time of the OSA.
    """
    time.sleep(1)
    OSA_object.write(f"ZSWPT{float(s_time):.2f}")
    # gives the OSA time to update before any data is taken
    time.sleep(2)
# ---------------------------------------------


# --- Function to set waveform peak to centre wavelength -----------
def centre(OSA_object):
    """
    Takes OSA object and sets waveform peak to centre wavelength.
    """
    time.sleep(1)
    OSA_object.write(f"CTR=P")
    time.sleep(2)
# ---------------------------------------------


# --- Function to set the number of measurements to average over ---------------
def average_measurements(OSA_object, measurements):
    """
    Takes OSA object and number of measurements to average over, from 1 to 1000.
    """
    time.sleep(1)
    OSA_object.write(f"AVG{int(measurements)}")
    time.sleep(1)
# ------------------------------------------------------------------------------


# --- Function to set number of sampling points ---------------------------
def sampling_points(OSA_object, points):
    """
    Takes OSA object and number of points.
    Changes number of sampling points for each OSA measurement.
    Takes values in the range 11 to 20001. O sets number of points to auto.
    """
    time.sleep(1)
    OSA_object.write(f"SMPL{int(points)}")
    time.sleep(1)
# --------------------------------------------------------------------------


# --- Function to set the trace conditions of the OSA --------------------------------
def wavelength_range(OSA_object, start_wavelength, end_wavelength):
    """
        Takes an OSA Objet, start wavelength in nm and end wavelength in nm
        Sets the OSA to these settings and takes a single sweep
    """
    time.sleep(1)
    OSA_object.write(f"STAWL{float(start_wavelength):.2f}")
    time.sleep(1)
    OSA_object.write(f"STPWL{float(end_wavelength):.2f}")
    # gives the OSA time to update before any data is taken
    time.sleep(1)
    # single_sweep(OSA_object)
# ------------------------------------------------------------------------------------


# --- Function to set the trace conditions of the OSA --------------------------------
def wavelength_span(OSA_object, centre_wavelength, span_w):
    """
    Takes an OSA Objet, centre wavelength in nm and a wavelength span in nm
    Sets the OSA to these settings and takes a single sweep
    """
    time.sleep(1)
    OSA_object.write(f"CTRWL{float(centre_wavelength):.2f}")
    time.sleep(2)
    OSA_object.write(f"SPAN{float(span_w):.1f}")
    # gives the OSA time to update before any data is taken
    time.sleep(1)
    # single_sweep(OSA_object)
# ------------------------------------------------------------------------------------


# --- Function to set the trace conditions of the OSA --------------------------------
def frequency_range(OSA_object, start_frequency, end_frequency):
    """
        Takes an OSA Objet, start frequency in THz and end frequency in THz
        Sets the OSA to these settings and takes a single sweep
    """
    time.sleep(1)
    OSA_object.write(f"STAF{float(start_frequency):.3f}")
    time.sleep(1)
    OSA_object.write(f"STPF{float(end_frequency):.3f}")
    # gives the OSA time to update before any data is taken
    time.sleep(1)
    # single_sweep(OSA_object)
# ------------------------------------------------------------------------------------


# --- Function to set the trace conditions of the OSA --------------------------------
def frequency_span(OSA_object, centre_frequency, span_f):
    """
    Takes an OSA Objet, centre frequency in THz and a frequency span in THz
    Sets the OSA to these settings and takes a single sweep
    """
    time.sleep(1)
    OSA_object.write(f"CTRF{float(centre_frequency):.2f}")
    time.sleep(1)
    OSA_object.write(f"SPANF{float(span_f):.1f}")
    # gives the OSA time to update before any data is taken
    time.sleep(1)
    # single_sweep(OSA_object)
# ------------------------------------------------------------------------------------


# --- Function to pull the spectrum ------------------
def get_trace_A_spectrum(OSA_object):
    """
    Takes an OSA Object. Returns
    Returns a tuple of arrays. The first array contains the wavelengths in nanometers.
    The second array contains the optical power in dBm.
    """
    time.sleep(1)
    OSA_object.write(f"WRTA")
    time.sleep(0.5)
    power_string = OSA_object.query(f'LDATA')
    time.sleep(1)
    power = np.array(power_string[:-2].split(','))
    power = power.astype(np.float)[2:]

    wavelength_string = OSA_object.query(f'WDATA')
    time.sleep(1)
    wavelength = np.array(wavelength_string[:-2].split(','))
    wavelength = wavelength.astype(np.float)[2:]

    return wavelength, power
# ---------------------------------------------------------


# --- Function to pull the spectrum ------------------
def get_trace_B_spectrum(OSA_object):
    """
    Takes an OSA Object. Returns
    Returns a tuple of arrays. The first array contains the wavelengths in nanometers.
    The second array contains the optical power in dBm.
    """
    time.sleep(1)
    OSA_object.write(f"WRTB")
    time.sleep(0.5)
    power_string = OSA_object.query(f'LDATB')
    time.sleep(1)
    power = np.array(power_string[:-2].split(','))
    power = power.astype(np.float)[2:]

    wavelength_string = OSA_object.query(f'WDATB')
    time.sleep(1)
    wavelength = np.array(wavelength_string[:-2].split(','))
    wavelength = wavelength.astype(np.float)[2:]

    return wavelength, power
# ---------------------------------------------------------


# --- Function to pull the spectrum ------------------
def get_trace_C_spectrum(OSA_object):
    """
    Takes an OSA Object. Returns
    Returns a tuple of arrays. The first array contains the wavelengths in nanometers.
    The second array contains the optical power in dBm.
    """
    time.sleep(1)
    OSA_object.write(f"WRTC")
    time.sleep(0.5)
    power_string = OSA_object.query(f'LDATC')
    time.sleep(1)
    power = np.array(power_string[:-2].split(','))
    power = power.astype(float)[2:]

    wavelength_string = OSA_object.query(f'WDATC')
    time.sleep(1)
    wavelength = np.array(wavelength_string[:-2].split(','))
    wavelength = wavelength.astype(float)[2:]

    return wavelength, power
# ---------------------------------------------------------


# --- Function to change log scale of the OSA -----------------
def log_reference_level(OSA_object, level):
    """
    Takes an OSA object and a reference level in dBm from -90 to 20.
    Changes the log scaling of the OSA to the specified value.
    """
    time.sleep(1)
    OSA_object.write(f"REFL{float(level):.1f}")
    # gives the OSA time to update before any data is taken
    time.sleep(1.5)
# ------------------------------------------------------------


# --- Function to change linear scale of the OSA -----------------
def linear_reference_level(OSA_object, level):
    """
    Takes an OSA object and a reference level in mW from 1 to 100.
    Changes the log scaling of the OSA to the specified value.
    """
    time.sleep(1)
    OSA_object.write(f"REFLM{float(level):.1f}")
    # gives the OSA time to update before any data is taken
    time.sleep(1.5)
# ------------------------------------------------------------


# --- Function to change resolution bandwidth of the OSA -----
def resolution_bandwidth_w(OSA_object, resolution):
    """
    Takes an OSA object and a resolution in nm from 0.01 to 2.0.
    Changes the OSA resolution.
    """
    time.sleep(1)
    OSA_object.write(f"RESLN{float(resolution):.2f}")
    time.sleep(1.5)
# ------------------------------------------------------------


# --- Function to change resolution bandwidth of the OSA -----
def resolution_bandwidth_f(OSA_object, resolution):
    """
    Takes an OSA object and a resolution in GHZ.
    Takes values 2, 4, 10, 20, 40, 100, 200, 400.
    Changes the OSA resolution.
    """
    time.sleep(1)
    OSA_object.write(f"RESLNF{int(resolution)}")
    time.sleep(1.5)
# ------------------------------------------------------------


# --- Function to change OSA sensitivity ------------
def sensitivity(OSA_object, level):
    """
    Takes an OSA object and a level, from 1 to 6.
    Level 1 is the most sensitive, level 6 is the least sensitive.
    Changes the OSA sensitivity to one of six levels
    """
    time.sleep(1)
    if int(level) == 1:
        OSA_object.write(f"SHI1")
    elif int(level) == 2:
        OSA_object.write(f"SHI2")
    elif int(level) == 3:
        OSA_object.write(f"SHI3")
    elif int(level) == 4:
        OSA_object.write(f"SNHD")
    elif int(level) == 5:
        OSA_object.write(f"SNAT")
    elif int(level) == 6:
        OSA_object.write(f"SMID")
    else:
        print("Give a valid sensitivity level")
        exit()
    time.sleep(1.5)
# ----------------------------------------------------


# --- Function to measure SMSR ------------------------------
def SMSR(OSA_object):
    """
    Calculates peak SMSR. There are two modes to choose from.
    """
    time.sleep(1)
    OSA_object.write(f"SSMSK{99.99}")
    # OSA_object.query("SMSR1")
    peak_string = OSA_object.query("SMSR2")
    time.sleep(0.5)
    smsr = OSA_object.query("ANA?")
    return smsr
# -----------------------------------------------------------


########################################################################################################################


def main():
    # print('Devices found:')
    # print(rm.list_resources())  # use this to figure out the address of the OSA
    print('Connecting to OSA...')

    # --- CHANGE THIS TO MATCH YOUR OSA:
    OSA_address = "GPIB0::8::INSTR"
    # ----------------------------------

    # connects to the device and tests connection
    ANDO_OSA = OSA_connection()
    # OSA_connection_test(ANDO_OSA)

    centre_w = 1550  # nm
    span_w = 50  # nm
    # centre_frequency = 275  # MHz
    # freq_span = 40  # MHz
    # start_freq = 250  # MHz
    # end_freq = 300  # MHz
    sweep_length = 50  # S
    axis_log_scale = 5  # DB
    res_bandwidth_w = 0.1  # nm
    ref_level = 0  # dBm
    averages = 1
    samples = 1000
    sensitivity_level = 1

    # log_reference_level(ANDO_OSA, ref_level)
    # wavelength_span(ANDO_OSA, centre_w, span_w)
    # centre(ANDO_OSA)
    # resolution_bandwidth_w(ANDO_OSA, res_bandwidth_w)
    # sampling_points(ANDO_OSA, samples)
    # average_measurements(ANDO_OSA, averages)
    # sweep_time(ANDO_OSA, sweep_length)
    single_sweep(ANDO_OSA)
    # continuous_sweep(ANDO_OSA)

    while int(ANDO_OSA.query("SWEEP?")) != 0:
        time.sleep(1)

    # take data
    wavelength, power = get_trace_C_spectrum(ANDO_OSA)

    ########### SMSR finding code below needs to be tested

    # print(f"SMSR = {SMSR(ANDO_OSA)}")
    # print(len(SMSR(ANDO_OSA)))
    # --- SMSR function currently returns a list of peaks
    smsr = SMSR(ANDO_OSA)
    # --- np.in1d: Test whether each element of a 1-D array is also present in a second array.
    # --- Getting the indices of the peaks found with the SMSR function
    # --- to find them in the lists of power and wavelength. From power we can estimate SMSR.

    print(smsr)

    plt.plot(wavelength, power)
    plt.show()

# if __name__ == '__main__':
#     start = time.time()
#     main()
#     end = time.time()
#
#     print(f'Done in {end - start} seconds')