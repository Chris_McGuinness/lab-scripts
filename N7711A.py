import numpy as np
import time
import pyvisa as visa
import scipy.constants

########################################################################################################################


class Agilent_TL(object):
    def __init__(self, address="GPIB0::19::INSTR"):
        rm = visa.ResourceManager()
        self.address = address
        self.TL = rm.open_resource(address)
        self.TL.write("*CLS")

    def operation_complete_query(self):
        while int(self.TL.query("*OPC?")) != 1:
            time.sleep(0.05)

    def laser_on(self):
        self.TL.write(":CHANnel1:POWer:STATe 1")
        self.operation_complete_query()
        # time.sleep(1)

    def laser_off(self):
        self.TL.write(":CHANnel1:POWer:STATe 0")

    def power_query(self):
        power = self.TL.query(":CHANnel1:POWer:LEVel:IMMediate:AMPLitude?")
        print(power)

    def set_power_min(self):
        self.TL.write(":CHANnel1:POWer:LEVel:IMMediate:AMPLitude MIN")

    def set_power_max(self):
        self.TL.write(":CHANnel1:POWer:LEVel:IMMediate:AMPLitude MAX")

    def set_power_dBm(self, power):
        self.TL.write(f":CHANnel1:POWer:LEVel:IMMediate:AMPLitude {power} DBM")

    def set_power_mW(self, power):
        self.TL.write(f":CHANnel1:POWer:LEVel:IMMediate:AMPLitude {power} mW")

    def power_unit_query(self):
        unit = self.TL.query(":CHANnel1:POWer:UNit?")
        if int(unit) == 1:
            print("W")
        elif int(unit) == 0:
            print("dBm")
        else:
            print("String received is not recognised")

    def set_unit(self, unit):
        if unit == "DBM":
            self.TL.write(":CHANnel1:POWer:UNit 0")
        elif unit == "W":
            self.TL.write(":CHANnel1:POWer:UNit 1")
        else:
            print("Unit string is not recognised")

    def wavelength_query(self):
        wl = self.TL.query(":CHANnel1:WAVelength:CW?")
        self.operation_complete_query()
        return float(wl)

    def min_wavelength_query(self):
        wl = self.TL.query(":CHANnel1:WAVelength:CW? MIN")
        return wl

    def max_wavelength_query(self):
        wl = self.TL.query(":CHANnel1:WAVelength:CW? MAX")
        return wl

    def set_auto_mode(self):
        self.laser_off()
        self.TL.write(":CHANnel1:WAVelength:AUTO 1")
        self.laser_on()

    def set_grid_mode(self):
        self.laser_off()
        self.TL.write(":CHANnel1:WAVelength:AUTO 0")
        self.laser_on()

    def set_wavelength(self, wl):
        """
        Takes a wavelength value in nanometres
        """
        # self.laser_off()
        self.TL.write(f":CHANnel1:WAVelength:CW {float(wl):.4f} NM")
        # self.laser_on()
        self.operation_complete_query()

    def set_frequency(self, f):
        """
        Takes frequency in HZ
        """
        self.laser_off()
        self.TL.write(f"CHAN1:FREQ {f} HZ")
        self.laser_on()

    def frequency_query(self):
        f = self.TL.query(":CHAN1:FREQ?")
        # self.operation_complete_query()
        print(f)
        return f

    def set_grid_reference(self, f0):
        """
        Sets f0 for the grid of discrete frequency points in hertz.
        """
        self.TL.write(f":CHAN1:FREQ:REF {f0} HZ")
        self.operation_complete_query()

    def set_grid_spacing(self, df):
        """
        Sets the spacing for the frequency grid.
        """
        self.TL.write(f"CHAN1:FREQ:GRID {df}")

    def set_grid_channel(self, channel):
        """
        Sets the grid to a chosen grid number. Must be an integer.
        """
        self.TL.write(f"CHAN1:FREQ:CHAN {channel}")
        self.operation_complete_query()

    def query_grid_channel(self):
        print(self.TL.query(":CHAN1:FREQ:CHAN?"))

    def set_grid_offset(self, off):
        """
        Sets the frequency offset used by the instrument.
        """
        self.TL.write(f"CHAN1:FREQ:OFFSet {off} HZ")
        self.operation_complete_query()

    def query_frequency_offset(self):
        off = self.TL.write(f"CHAN1:FREQ:OFFSet?")
        min = self.TL.write(f"CHAN1:FREQ:OFFSet? MIN")
        max = self.TL.write(f"CHAN1:FREQ:OFFSet? MAX")
        print(f"Current Offset: {off} HZ\nMin Offset: {min} HZ\nMax Offset: {max} HZ")

    def return_grid_information(self):
        """
        Returns information about the defined grid frequencies.
        """
        f = self.frequency_query()
        ref = self.TL.query(":CHAN1:FREQ:REF?")
        grid = self.TL.query(":CHAN1:FREQ:GRID?")
        chan = self.TL.query(":CHAN1:FREQ:CHAN?")
        off = self.TL.query(f"CHAN1:FREQ:OFFSet?")
        print(f"Current Frequency: {f} HZ\nGrid Reference Frequency: {ref} HZ\nGrid Spacing: {grid} HZ"
              f"\nCurrent Grid Channel: {chan}\nFrequency Offset: {off} HZ")


def wavelength_to_hz(wl):
    c = scipy.constants.c
    freq = c/wl
    return freq

########################################################################################################################
