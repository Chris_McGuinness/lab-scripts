# import random
# import pandas as pd
# import json
import numpy as np
import time
import csv
import gitlab
import threading
from IDPhotonics import OSA
from gpd_2303s import gpd_2303s

"""
Initializing requirements to commit to GitLab.
Do not let anyone outside of Pilot see this code.
The access token is for my work GitLab account and gives remote access.
"""
# url = r'https://gitlab.com/Chris_McGuinness/lab-scripts/Test_Data.git'
full_access_token = 'glpat-ydA7MQE8kdCHynTeN_8J' # access token must be made in account settings
# id = 37050623 # ID of project commit is going to
id = 37247739
gl = gitlab.Gitlab(private_token=full_access_token)#url=url,  private_token=full_access_token)

project = gl.projects.get(id)

# Defining Functions ###################################################################################################
########################################################################################################################


def run_single_test(ring_1_voltages, ring_2_voltages, power_supply, osa):
    """
    Will run a single set of tests, cycling through the three voltages supplied in steps of 0.1V.
    Will then return an array with the results of each test. One voltage supply per array.
    """

    power = np.zeros((len(ring_1_voltages), len(ring_2_voltages)))
    wavelength = np.zeros((len(ring_1_voltages), len(ring_2_voltages)))
    time_array = np.zeros((len(ring_1_voltages), len(ring_2_voltages)))

    for i in range(len(ring_1_voltages)):
        power_supply.set_voltage(1, ring_1_voltages[i])
        print("i = ", i)
        for j in range(len(ring_2_voltages)):
            power_supply.set_voltage(2, ring_2_voltages[j])
            print("j = ", j)
            time.sleep(0.1)
            try:
                w, p = osa.findMaxPowerWL()

            except:
                w, p = 9.99e99, 9.99e99

            wavelength[i, j] = w
            power[i, j] = p
            time_array[i, j] = time.time()

    # printing values to debug if necessary
    print(power)
    print(wavelength)
    print(time_array)
    # returning the three arrays of output powers
    return power, wavelength, time_array

def initial_commit(project, file):
    """
    Initialize file on GitLab
    """

    # create a data.json to package the file for committing to GitLab
    data = {
        'branch': 'main',
        'path': 'lab-scripts/Test_Data',
        'commit_message': 'Initialize Test Data File',
        'actions': [
            {
                'action': 'create',
                'file_path': file,
                'content': open(file).read()
            }
        ]
    }

    # command that commits data file
    commit = project.commits.create(data)

def update_commit_thread(project, file, tests_run):
    """
    Update file on GitLab. To be run in its own thread.
    """
    try:
        print('Updating File on GitLab...')
         # create a data.json to package the file for committing to GitLab
        data = {
            'branch': 'main',
            'path': 'lab-scripts/Test_Data',
            'commit_message': f'Test Data Update: Tests Run ={tests_run}',
            'actions': [
                {
                    'action': 'update',
                    'file_path': file,
                    'content': open(file).read()
                }
            ]
        }
        # command that commits data file
        commit = project.commits.create(data)
        print('File Updated!')
    except:
        print('Failed to upload File to GitLab')

def final_update_commit(project, file, tests_run):
    """
    Update file on GitLab
    """
    # create a data.json to package the file for committing to GitLab
    try:
        data = {
            'branch': 'main',
            'path': 'lab-scripts/Test_Data',
            'commit_message': f'Final Test Data Update: Tests Run = {tests_run}',
            'actions': [
                {
                    'action': 'update',
                    'file_path': file,
                    'content': open(file).read()
                }
            ]
        }

        # command that commits data file
        commit = project.commits.create(data)
    except:
        print('Failed final upload of File to GitLab')

# Defining main() Function #############################################################################################
########################################################################################################################

def main():
    print('Beginning Script...')
    start = time.time()
    tests_run = 0

    # initializing devices
    ring_1_voltages = np.arange(0, 10, 0.1)
    ring_2_voltages = np.arange(0, 10.1, 0.1)

    power_supply = gpd_2303s('COM16')
    osa = OSA(ip='169.254.9.7')
    osa.setPort("HighPower")
    osa.setStartStopWL(1540, 1560)

    power_supply.turn_on_off(1)

    # Initialize data file on local storage and GitLab
    header = ['Time', 'Test Number', 'Power', 'Wavelength', 'Time Array']
    with open(fr'Test_Data\{start}.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')

        # write the header
        writer.writerow(header)
        f.close()
    try:
        print('Initial GitLab Commit...')
        initial_commit(project, fr'Test_Data\{start}.csv')
    except:
        print('File was not initialized on GitLab')

    # running tests until keyboard interrupt
    try:
        while True:
            power, wavelength, time_array = run_single_test(ring_1_voltages, ring_2_voltages, power_supply, osa)

            tests_run += 1
            print(f'Tests run = {tests_run}')
            test_end = time.time()

            data = [test_end, tests_run, power, wavelength, time_array]

            with open(fr'Test_Data\{start}.csv', 'a', newline='') as f:
                writer = csv.writer(f, delimiter=',')

                # write the data
                writer.writerow(data)
                f.close()
            if tests_run%10 == 0:
                np.savez(fr'Test_Data\Test = {tests_run}.npz', data)
                update_thread = threading.Thread(update_commit_thread(project, fr'Test_Data\{start}.csv', tests_run))
                update_thread.start()

    # keyboard interrupt and final commit to GitLab
    except KeyboardInterrupt:
        end = time.time()
        try:
            print('Updating file on GitLab...')
            final_update_commit(project, fr'Test_Data\{start}.csv', tests_run)
            print('File Updated!')
        except:
            print('Failed final upload to GitLab')
        print('Done in', float(end - start), 'seconds \nTests Run =', tests_run)
        pass

# Running main() Function ##############################################################################################
########################################################################################################################

if __name__ == '__main__':
    main()
