import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# from scipy.optimize import curve_fit

def read_map_data_excel(file , sheet_name):
    '''
    Creates pandas dataframe from Excel file.
    Takes file location as a string and Excel sheet name as a string.
    Returns mode map data as a numpy array.
    '''

    df = pd.read_excel(file, sheet_name=sheet_name)

    # create 257x257 array of zeros
    data = np.zeros((256, 256))

    # filling empty array with mode map data
    for i in range(len(df.index)):
        x = df['i'][i]
        y = df['j'][i]
        wavelength = df['Wavelength (nm)'][i]
        data[x, y] = wavelength

    return data


def read_map_data_csv(file):
    '''
    Creates pandas dataframe from .csv file.
    Takes file location as a string.
    Returns mode map data as a numpy array.
    '''

    df = pd.read_csv(file, sep='\t')

    # create 257x257 array of zeros
    data = np.zeros((256, 256))

    # filling empty array with mode map data
    for i in range(len(df.index)):
        x = df['i'][i]
        y = df['j'][i]
        wavelength = df['Wavelength (nm)'][i]
        data[x, y] = wavelength

    return data

def read_files(files):
    '''
    Takes a list of strings, where each string is a file location.
    Returns a list containing numpy arrays, where each array is contains mode map data.
    '''

    data = []

    # only the first of the data files should contain an Excel file. The rest should be in .csv format
    for i in range(len(files)):
        try:
            data.append(read_map_data_csv(files[i]))
        except:
            print('Something went wrong reading files')

    return data

def wavelength_change(data, test_duration):
    '''
    Takes a list of mode maps and a list of burn-in test durations.
    Returns a list of mode maps showing the change in wavelength for each pixel in pm/hour
    '''

    mode_map_difference = []

    for i in range(1, len(data)):
        mode_map_difference.append(((data[i] - data[i-1])/ test_duration[i-1]) * 1000)

    return mode_map_difference


def plot_data(data, oven_time):
    '''
    Takes a list containing numpy data arrays and a list with time spent in the oven for each mode map.
    Plots each array with matplotlib.pyplot.imshow(), with burn-in time as a title.
    '''

    for i in range(len(data)):
        fig = plt.figure()
        norm = plt.Normalize(vmin=1530, vmax=1575, clip=True)
        plot = plt.imshow(data[i], norm=norm, origin='lower')
        fig.colorbar(plot)
        plt.title(rf'Wavelength Map: {oven_time[i]} Hours Reliability Testing')
        plt.show()

def plot_mode_map_difference(mode_map_difference, test_duration):
    '''
    Takes a list containing difference between mode maps and a list of burn-in duration between mode maps.
    Plots difference between arrays with matplotlib.pyplot.imshow(), with both burn-in times in the title.
    '''

    for i in range(len(mode_map_difference)):
        fig = plt.figure()
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        norm = plt.Normalize(vmin=-50, vmax=50, clip=True)
        plot = plt.imshow(mode_map_difference[i], norm=norm, extent=(0, -10, 0, -10), origin='lower', cmap="bwr")
        cbar = fig.colorbar(plot)
        cbar.set_label('Wavelength Change (pm/Day)', fontsize=15)
        plt.title(rf'$\Delta$ $\lambda$ (pm/hour): '#Difference between'
                  rf' {test_duration[i]} and {test_duration[i+1]} Hour Reliability Testing')
        plt.xlabel('Tuning Voltage Ring 1 (V)', fontsize=15)
        plt.ylabel('Tuning Voltage Ring 2 (V)', fontsize=15)
        # if i == 6:
        #     fig.savefig(rf'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Pictures\Map_For_Poster_{i}',
        #                 dpi=1200)
        plt.show()

def plot_coordinate_values(data, total_oven_time):
    '''
    Takes list of mode map data and a list of total burn-in time for each mode map.
    Makes two plots. First, plots the absolute wavelength drift of each coordinate.
    Second, plots absolute drift of two points with high resolution
    '''

    no_of_tests = len(data)

    _50_75 = np.zeros(no_of_tests)
    _100_130 = np.zeros(no_of_tests)
    _135_200 = np.zeros(no_of_tests)
    _150_110 = np.zeros(no_of_tests)
    _190_70 = np.zeros(no_of_tests)
    _75_20 = np.zeros(no_of_tests)

    for i in range(no_of_tests):
        _50_75[i] = data[i][75, 50]
        _100_130[i] = data[i][130,100]
        _135_200[i] = data[i][200,135]
        _150_110[i] = data[i][110,150]
        _190_70[i] = data[i][70,190]
        _75_20[i] = data[i][20,75]

    plt.figure()
    plt.title('Wavelength vs Time for chosen points')
    plt.plot(total_oven_time, _50_75, label='50_75')
    plt.plot(total_oven_time, _100_130, label='100_130')
    plt.plot(total_oven_time, _135_200, label='135_200')
    plt.plot(total_oven_time, _150_110, label='150_110')
    plt.plot(total_oven_time, _190_70, label='_190_70')
    plt.plot(total_oven_time, _75_20, label='75_20')
    plt.legend(loc='best')
    plt.ylabel(r'$\lambda$ (nm)')
    plt.xlabel('Time (hours)')
    plt.show()

    fig = plt.figure()
    plt.subplot(121)
    plt.plot(total_oven_time, _50_75)
    # plt.ylim(1571.7, 1571.77)
    plt.title('Point (50, 75)')
    plt.ylabel(r'$\lambda$ (nm)')
    plt.xlabel('Time (hours)')
    plt.subplot(122)
    # plt.ylim(1542.6, 1542.72)
    plt.title('Point (150, 110)')
    plt.plot(total_oven_time, _150_110)
    plt.ylabel(r'$\lambda$ (nm)')
    plt.xlabel('Time (hours)')
    plt.show()

def exponential(x, a, b, c, d):
    return a * np.exp(-b * x) + c*x + d

def coth_fit(x, a, b):
    return a*( np.sinh(x)/np.cosh(x) ) + b

def plot_normalized_wavelength_change(mode_map_difference, tests_oven_time, test_duration, sigma1, sigma2):
    '''
    Takes list of mode map data and a list of total burn-in time for each mode map, total oven time and uncertainties.
    Makes three plots. First, plots the relative wavelength drift per hour between tests for each coordinate.
    Second, plots the relative wavelength drift per day between tests for each coordinate.
    Third, plots absolute drift of two points with high resolution
    '''

    days_in_oven = tests_oven_time / 24

    no_of_tests = len(mode_map_difference)
    sigma = np.zeros(no_of_tests)
    _50_75 = np.zeros(no_of_tests)
    _100_130 = np.zeros(no_of_tests)
    _135_200 = np.zeros(no_of_tests)
    _150_110 = np.zeros(no_of_tests)
    _190_70 = np.zeros(no_of_tests)
    _75_20 = np.zeros(no_of_tests)

    _50_75_day = np.zeros(no_of_tests)
    _100_130_day = np.zeros(no_of_tests)
    _135_200_day = np.zeros(no_of_tests)
    _150_110_day = np.zeros(no_of_tests)
    _190_70_day = np.zeros(no_of_tests)
    _75_20_day = np.zeros(no_of_tests)

    for i in range(no_of_tests):
        sigma[i] = error(sigma1, sigma2, test_duration[i])
        _50_75[i] = mode_map_difference[i][75, 50]
        _100_130[i] = mode_map_difference[i][130, 100]
        _135_200[i] = mode_map_difference[i][200, 135]
        _150_110[i] = mode_map_difference[i][110, 150]
        _190_70[i] = mode_map_difference[i][70, 190]
        _75_20[i] = mode_map_difference[i][20, 75]

        _50_75_day[i] = mode_map_difference[i][75, 50] * 24
        _100_130_day[i] = mode_map_difference[i][130, 100] * 24
        _135_200_day[i] = mode_map_difference[i][200, 135] * 24
        _150_110_day[i] = mode_map_difference[i][110, 150] * 24
        _190_70_day[i] = mode_map_difference[i][70, 190] * 24
        _75_20_day[i] = mode_map_difference[i][20, 75] * 24

    plt.figure()
    plt.title('Wavelength Change per hour for chosen points')
    plt.plot(tests_oven_time, _50_75)
    plt.plot(tests_oven_time, _100_130)
    plt.plot(tests_oven_time, _135_200)
    plt.plot(tests_oven_time, _150_110)
    # plt.plot(tests_oven_time, _190_70)
    plt.plot(tests_oven_time, _75_20)
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / hour)')
    plt.xlabel('Time (hour)')
    plt.show()

    wavelengths = [_50_75_day, _100_130_day, _135_200_day, _150_110_day, _75_20_day]
    extrapolate_x = np.linspace((days_in_oven[0] - 0.1), (days_in_oven[-1] + 2), 1000)
    wavelength_per_day = []
    # sigma = []
    # for i in range(len(_50_75_day)):
    #     mean = np.mean(np.array([_50_75_day[i], _100_130_day[i], _135_200_day[i], _150_110_day[i], _75_20_day[i]]))
    #     err = np.std(np.array([_50_75_day[i], _100_130_day[i], _135_200_day[i], _150_110_day[i], _75_20_day[i]]))
    #     sigma.append(np.sqrt(err))
    #     wavelength_per_day.append(mean)
    #
    # t0 = days_in_oven[0]
    # days_in_oven -= t0
    # extrapolate_x -= t0
    # popt, pcov = curve_fit(coth_fit, days_in_oven, wavelength_per_day, sigma=sigma)
    # print(popt)
    # fit_y = coth_fit(extrapolate_x, *popt)
    # extrapolate_x += t0
    # days_in_oven += t0

    # sigma_fit = np.sqrt(np.diagonal(pcov))
    # bound_upper = exponential(extrapolate_x, *(popt + sigma_fit))
    # bound_lower = exponential(extrapolate_x, *(popt - sigma_fit))

    plt.figure()
    plt.title('Wavelength Change per day for chosen points')
    plt.plot(days_in_oven, _50_75_day)
    plt.plot(days_in_oven, _100_130_day)
    plt.plot(days_in_oven, _135_200_day)
    plt.plot(days_in_oven, _150_110_day)
    # plt.plot(days_in_oven, _190_70_day)
    plt.plot(days_in_oven, _75_20_day)
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / day)')
    plt.xlabel('Time (day)')
    # plt.plot(extrapolate_x, fit_y, '--r')
    # plt.fill_between(extrapolate_x, bound_lower, bound_upper,
    #                  color='r', alpha=0.3)
    # plt.ylim(0, 3.1)
    # convergence_info = f"Fitting Converges at {fit_y[-1]:.3f} pm/day\nAfter {extrapolate_x[-1]:.3f} days"
    # plt.text(8, 2, convergence_info)
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(tests_oven_time, _50_75)
    plt.errorbar(tests_oven_time, _50_75, yerr=sigma, capsize=4)
    # plt.ylim(-0.1, 0.11)
    plt.title('Point (50, 75)')
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / hour)')
    plt.xlabel('Time (hours)')
    plt.subplot(122)
    # plt.ylim(-0.1, 0.11)
    plt.title('Point (150, 110)')
    plt.plot(tests_oven_time, _150_110)
    plt.errorbar(tests_oven_time, _150_110, yerr=sigma, capsize=4)
    plt.ylabel(r'$\Delta$ $\lambda$ (pm / hour)')
    plt.xlabel('Time (hours)')
    plt.show()


def error(sigma1, sigma2, t):
    return np.sqrt( ( (-1000/t)*sigma1 )**2 + ( (-1000/t)*sigma2 )**2 )


def main():
    ### Naming all files to be used
    file0 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August10_Normal Regime'
    file1 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August15_Normal Regime'
    file2 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August16_Normal Regime'
    file3 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August18_Normal Regime'
    file4 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August22_Normal Regime'
    file5 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August23_Normal Regime'
    file6 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August26_Normal Regime'
    file7 = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_August29_Normal Regime'

    files = (file0, file1, file2, file3, file4, file5, file6, file7)
    total_time_delta = [0, 118.76, 139.61, 187.6, 283.49, 310.55, 383.25, 451.19]
    test_time_delta = np.array([118.76, 139.61, 187.6, 283.49, 310.55, 383.25, 451.19])
    time_between_tests = np.array([118.76, 20.85, 47.99, 95.89, 27.06, 72.70, 67.94])


    # error value for the Bristol 871B Wavemeter is 0.0008nm at 1000nm
    sigma1 = 0.0011625  # nm
    sigma2 = 0.0011625  # nm

    ### Running functions to analyse and plot all data
    data = read_files(files)
    mode_map_difference = wavelength_change(data, time_between_tests)

    plot_data(data, total_time_delta)
    plot_mode_map_difference(mode_map_difference, total_time_delta)
    # plot_coordinate_values(data, total_time_delta)
    # plot_normalized_wavelength_change(mode_map_difference, test_time_delta, time_between_tests, sigma1, sigma2)

if __name__ == '__main__':
    main()

    print('Done!')
