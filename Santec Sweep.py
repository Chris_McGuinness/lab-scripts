import time
import numpy as np
import new_instrs as nins


def main():
    start = time.time()
    data = []

    start_wl = 1553
    end_wl = 1556
    step = 0.001
    wavelengths = np.arange(start_wl, end_wl + step, step)
    # print(wavelengths)
    midpoint = (end_wl+start_wl)/2
    print(start_wl, end_wl, midpoint)

    TL = nins.TL()
    PM = nins.ILX_PM_8210(int(midpoint), "Watt", "FAST")

    pow = 4
    TL.set_wl((end_wl-start_wl)/2)
    TL.set_laser_on()
    # TL.set_pow_mW(pow)
    TL.set_pow_dBm(pow)

    # initialise loop
    for i in range(20, 0, -1):
        TL.set_wl(start_wl-i)
        TL.set_pow_mW(pow)
        time.sleep(0.1)
        power = PM.readPower()

    for i in wavelengths:
        TL.set_wl(i)
        time.sleep(0.5)
        # TL.set_pow_mW(pow)
        # time.sleep(0.5)
        # power = [0, 0, 0]
        # for j in range(3):
        #     power[j] = PM.readPower()
        # print(i, np.mean(power))
        # data.append([i, np.mean(power)])
        power = PM.readPower()
        data.append([i, power])

    TL.set_laser_off()
    np.savez(
        rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\SmallestRingSweep_{start_wl}_to_{end_wl}_{time.time()}.npz",
        data)
    # np.savez(
    #     rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\MicroRing\PolarizerSweep_{start_wl}_to_{end_wl}.npz",
    #     data)
    end = time.time()
    print(f"Done in {(end - start) / 60} minutes")


if __name__ =="__main__":
    main()
