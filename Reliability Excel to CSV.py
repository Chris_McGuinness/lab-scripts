import pandas as pd
import numpy as np
from datetime import datetime
import time

def read_csv_data(file):
    '''
    Creates pandas dataframe from Excel file.
    Takes file location as a string and Excel sheet name as a string.
    Returns Pandas DataFrame and lists of Light, Current and Voltage Values.
    '''

    df = pd.read_csv(file)

    # print(df)

    time = np.zeros(len(df['Timestamp']))
    wavelength = np.zeros(len(df['Timestamp']))
    intensity = np.zeros(len(df['Timestamp']))

    # if t0==False:
    #     t0 = datetime.strptime(df['Timestamp'][0], "%Y-%m-%dT%H:%M:%S.%f").timestamp()
    # else:
    #     t0 = t0
    t0 = datetime.strptime(df['Timestamp'][0], "%Y-%m-%dT%H:%M:%S.%f").timestamp()

    for i in range(len(df['Timestamp'])):
        t = datetime.strptime(df['Timestamp'][i], "%Y-%m-%dT%H:%M:%S.%f").timestamp() - t0
        time[i] = t / (24*3600)
        wavelength[i] = df['Instrument Wavelength'][i]
        intensity[i] = df['Instrument Intensity'][i]

    return time, wavelength, intensity

def main():
    # file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test1.csv'
    # file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test2.csv'
    # file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test3.csv'
    # file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test4.csv'
    # file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test5.csv'
    # file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test6.csv'
    file = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test7.csv'

    # path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test1 Data.csv'
    # path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test2 Data.csv'
    # path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test3 Data.csv'
    # path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test4 Data.csv'
    # path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test5 Data.csv'
    # path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test6 Data.csv'
    path = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test7 Data.csv'

    time, wavelength, intensity = read_csv_data(file)

    data = {'Test Time': time, 'Test Wavelength': wavelength, 'Test Intensity': intensity}

    df = pd.DataFrame(data=data)

    print(df)

    df.to_csv(path)

if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()

    print(f'Done in {end-start} seconds')