import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# from datetime import datetime
import time
from scipy.optimize import curve_fit
from scipy import signal
# from uncertainties import ufloat

def read_csv_data(file):
    '''
    Creates pandas dataframe from Excel file.
    Takes file location as a string and Excel sheet name as a string.
    Returns Pandas DataFrame and lists of Light, Current and Voltage Values.
    '''

    df = pd.read_csv(file)

    # print(df)

    # time = np.zeros(len(df['Test Time']))
    # wavelength = np.zeros(len(df['Test Time']))
    # intensity = np.zeros(len(df['Test Time']))
    #
    # # t0 = df['Test Time'][0]
    #
    # for i in range(len(df['Test Time'])):
    #     time[i] = df['Test Time'][i] #- t0
    #     # time[i] = t / (24*3600)
    #     wavelength[i] = df['Test Wavelength'][i]
    #     intensity[i] = df['Test Intensity'][i]

    return df#time, wavelength, intensity


def exponential_decay(x, a, b, c):
    return a * (1-np.exp(-x/b)) + c

def fitting_derivative(x, a, b, c):
    c = c
    return (a * np.exp(-x/b))/b

def tanh_fit(x, a, b):
    return a*np.tanh(x) + b

def log(x, a, b):
    return a * np.log(x) + b

def coth_fit(x, a, b):
    return a*( np.sinh(x)/np.cosh(x) ) + b

def find_slope(x, y, step_size):
    i = 0
    slope = []

    try:
        while i < len(y):
            delta_x = (x[i+step_size]-x[i])
            delta_y = (y[i+step_size]-y[i])
            slope.append(delta_y/delta_x)
            i += step_size
    except:
        pass

    return slope


def filtering(df0, df1, df2, df3, df4, df5, df6):
    time0 = df0['Test Time']
    wavelength0 = df0['Test Wavelength']
    time1 = df1['Test Time']
    wavelength1 = df1['Test Wavelength']
    time2 = df2['Test Time']
    wavelength2 = df2['Test Wavelength']
    time3 = df3['Test Time']
    wavelength3 = df3['Test Wavelength']
    time4 = df4['Test Time']
    wavelength4 = df4['Test Wavelength']
    time5 = df5['Test Time']
    wavelength5 = df5['Test Wavelength']
    time6 = df6['Test Time']
    wavelength6 = df6['Test Wavelength']

    time1 += time0[len(time0) - 1]
    time2 += time1[len(time1) - 1]
    time3 += time2[len(time2) - 1]
    time4 += time3[len(time3) - 1]
    time5 += time4[len(time4) - 1]
    time6 += time5[len(time5) - 1]

    # removed second set of data as an outlier test
    # time = [time0, time2, time3, time4, time5, time6]
    # wavelength = [wavelength0, wavelength2, wavelength3, wavelength4, wavelength5, wavelength6]
    time = [time0, time2, time3, time4, time6]
    wavelength = [wavelength0, wavelength2, wavelength3, wavelength4, wavelength6]
    all_time = []
    all_wavelengths = []

    for i in range(len(time)):
        all_time.extend(time[i])
        all_wavelengths.extend(wavelength[i])

    for i in range(len(all_time)):
        all_time[i] = all_time[i] - all_time[0]

    limit = 30
    extrapolate_x = np.linspace(-0.1, limit, 1000000)

    popt, pcov = curve_fit(exponential_decay, all_time[::100], all_wavelengths[::100], p0=[0.2,0.7,1570])
    print(f'Converge popt = {popt}')
    converge = exponential_decay(extrapolate_x, *popt)
    _sigma_fit = np.sqrt(np.diagonal(pcov))
    _bound_upper = exponential_decay(extrapolate_x, *(popt + _sigma_fit))
    _bound_lower = exponential_decay(extrapolate_x, *(popt - _sigma_fit))

    Zn = signal.savgol_filter(all_wavelengths[::1000], window_length=2001, polyorder=2, deriv=0)
    # dx = all_time[1] - all_time[0]
    # Zf = signal.savgol_filter(all_wavelengths[::1000], window_length=2001, polyorder=2, deriv=1, delta=dx)

    wavelength_fig = plt.figure()
    plt.plot(all_time, all_wavelengths)
    plt.plot(all_time[::1000], Zn, linestyle='-', color='lime')
    plt.plot(extrapolate_x, converge, '--r')
    plt.fill_between(extrapolate_x, _bound_lower, _bound_upper,
                     color='r', alpha=0.3)
    plt.title('Wavelength Change with Time')
    plt.xlabel('Time (days)')
    plt.ylabel(r'$\lambda$ (nm)')
    plt.grid(True)
    plt.ylim(1570.335, 1570.355)
    fit_info = f"Best Fit Parameters: \nA = {popt[0]:.5f} \nB = {popt[1]:.5f} \nC = {popt[2]:.5f}"
    plt.text(5, 1570.34, fit_info)
    wavelength_fig.savefig(r'C:\Users\ChrisMcGuinness\Pictures\Data Analysis\Reliability Tests Fit (Filtering).png')
    plt.show()

    y = fitting_derivative(extrapolate_x, *popt)
    bound_upper = fitting_derivative(extrapolate_x, *(popt + _sigma_fit))
    bound_lower = fitting_derivative(extrapolate_x, *(popt - _sigma_fit))

    # converting from nm/day to pm/day
    for i in range(len(y)):
        y[i] = y[i] * 1000
        bound_upper[i] = bound_upper[i] * 1000
        bound_lower[i] = bound_lower[i] * 1000

    aim_value_index = (np.abs(y - 0.02)).argmin()

    slope_fig = plt.figure()
    plt.plot(extrapolate_x, y, '--r')
    plt.fill_between(extrapolate_x, bound_lower, bound_upper,
                     color='r', alpha=0.3)

    plt.title('Rate of Change for Wavelength with Time')
    plt.xlabel('Time (days)')
    plt.ylabel(r'$\Delta$ $\lambda$ / $\Delta$ T (pm/day)')
    plt.grid(True)
    plt.vlines(extrapolate_x[aim_value_index], -0.1, 4)
    plt.ylim(-0.1, 4)
    convergence_info = f"Derivative Reaches {y[aim_value_index]:.3f} pm/day\nAfter {extrapolate_x[aim_value_index]:.3f} days"
    plt.text(5, 2.5, convergence_info)
    slope_fig.savefig(r'C:\Users\ChrisMcGuinness\Pictures\Data Analysis\Reliability Slope Fitting (Filtering).png')
    plt.show()

    print(f'Reaches {y[aim_value_index]} pm/day after {extrapolate_x[aim_value_index]} days')

# def Polynomial_fits(df0, df1, df2, df3):
#     time0 = df0['Test Time']
#     wavelength0 = df0['Test Wavelength']
#     time1 = df1['Test Time']
#     wavelength1 = df1['Test Wavelength']
#     time2 = df2['Test Time']
#     wavelength2 = df2['Test Wavelength']
#     time3 = df3['Test Time']
#     wavelength3 = df3['Test Wavelength']
#
#     time1 += time0[len(time0) - 1]
#     time2 += time1[len(time1) - 1]
#     time3 += time2[len(time2) - 1]
#     x0 = np.linspace(-0.01, 3.8, 10000)
#     x1 = np.linspace(3.8, 4.55, 10000)
#     x2 = np.linspace(4.55, 6.45, 10000)
#     x3 = np.linspace(6.45, 10.4, 10000)
#     # get polynomial parameters
#     poly0 = np.polyfit(time0, wavelength0, 15)
#     poly1 = np.polyfit(time1, wavelength1, 10)
#     poly2 = np.polyfit(time2, wavelength2, 10)
#     poly3 = np.polyfit(time3, wavelength3, 23)
#     # get polynomial functions and polynomial derivatives
#     p0 = np.poly1d(poly0)
#     ps0 = np.poly1d.deriv(p0)
#     p1 = np.poly1d(poly1)
#     ps1 = np.poly1d.deriv(p1)
#     p2 = np.poly1d(poly2)
#     ps2 = np.poly1d.deriv(p2)
#     p3 = np.poly1d(poly3)
#     ps3 = np.poly1d.deriv(p3)
#
#     wavelength_fig = plt.figure()
#     plt.plot(time0, wavelength0)
#     plt.plot(x0, p0(x0), 'r-')
#     plt.plot(time1, wavelength1)
#     plt.plot(x1, p1(x1), 'r-')
#     plt.plot(time2, wavelength2)
#     plt.plot(x2, p2(x2), 'r-')
#     plt.plot(time3, wavelength3, 'y')
#     plt.plot(x3, p3(x3), 'r-')
#     plt.title('Wavelength Change with Time')
#     plt.xlabel('Time (days)')
#     plt.ylabel(r'$\lambda$ (nm)')
#     plt.grid(True)
#     # plt.ylim(1570.335, 1570.346)
#     # plt.ylim(1570.342, 1570.345)
#     wavelength_fig.savefig(r'C:\Users\ChrisMcGuinness\Pictures\Data Analysis\Reliability Tests Fit (Polynomial).png')
#     plt.show()
#
#     poly_slope0 = np.zeros(10000)
#     poly_slope1 = np.zeros(10000)
#     poly_slope2 = np.zeros(10000)
#     poly_slope3 = np.zeros(10000)
#
#     for i in range(10000):
#         poly_slope0[i] = ps0(x0[i]) * 1000
#         poly_slope1[i] = ps1(x1[i]) * 1000
#         poly_slope2[i] = ps2(x2[i]) * 1000
#         poly_slope3[i] = ps3(x3[i]) * 1000
#
#     all_slope_x = []  # x0 + x1[1000:]
#     all_slope_y = []  # poly_slope0 + poly_slope1[1000:]
#
#     for i in range(len(x0) - 500):
#         all_slope_x.append(x0[i])
#         all_slope_y.append(poly_slope0[i])
#
#     for i in range(900, len(x1)):
#         all_slope_x.append(x1[i])
#         all_slope_y.append(poly_slope1[i])
#
#     for i in range(500, len(x2) - 500):
#         all_slope_x.append(x2[i])
#         all_slope_y.append(poly_slope2[i])
#
#     for i in range(500, len(x3) - 500):
#         all_slope_x.append(x3[i])
#         all_slope_y.append(poly_slope3[i])
#
#     limit = 15
#     extrapolate_x = np.linspace(-0.2, limit, len(all_slope_y))
#
#     popt, pcov = curve_fit(coth_fit, all_slope_x, all_slope_y)
#     fit_y = coth_fit(extrapolate_x, *popt)
#
#     # calculating confidence intervals
#     sigma_fit = np.sqrt(np.diagonal(pcov))
#     bound_upper = coth_fit(extrapolate_x, *(popt + sigma_fit))
#     bound_lower = coth_fit(extrapolate_x, *(popt - sigma_fit))
#
#     slope_fig = plt.figure()
#     plt.plot(x0[:-500], poly_slope0[:-500], 'r-')
#     plt.plot(x1[900:], poly_slope1[900:], 'r-')
#     plt.plot(x2[500:-500], poly_slope2[500:-500], 'r-')
#     plt.plot(x3[500:-500], poly_slope3[500:-500], 'r-')
#     plt.plot(extrapolate_x, fit_y, 'g--')
#     # plotting the confidence intervals
#     plt.fill_between(extrapolate_x, bound_lower, bound_upper,
#                      color='g', alpha=0.3)
#
#     convergence = min(fit_y, key=lambda x: abs(x - 0.2))
#     for i in range(len(fit_y)):
#         if fit_y[i] == convergence:
#             print(f'Slope value {convergence} at index {i}')
#
#     plt.title('Rate of Change for Wavelength with Time')
#     plt.xlabel('Time (days)')
#     plt.ylabel(r'$\Delta$ $\lambda$ / $\Delta$ T (pm/day)')
#     # ax.tick_params(axis='both', which='major', labelsize=10)
#     # ax.tick_params(axis='both', which='minor', labelsize=8)
#     plt.grid(True)
#     plt.ylim(-5, 20)
#     convergence_info = f"Fitting Converges at {convergence:.3f} pm/day\nAfter {limit} days"
#     plt.text(4.2, 15.2, convergence_info)
#     slope_fig.savefig(r'C:\Users\ChrisMcGuinness\Pictures\Data Analysis\Reliability Slope Fitting (Polynomial).png')
#     plt.show()

def intensity(df0, df1, df2, df3, df4, df5, df6):
    time0 = df0['Test Time']
    intensity0 = df0['Test Intensity']
    time1 = df1['Test Time']
    intensity1 = df1['Test Intensity']
    time2 = df2['Test Time']
    intensity2 = df2['Test Intensity']
    time3 = df3['Test Time']
    intensity3 = df3['Test Intensity']
    time4 = df4['Test Time']
    intensity4 = df4['Test Intensity']
    time5 = df5['Test Time']
    intensity5 = df5['Test Intensity']
    time6 = df6['Test Time']
    intensity6 = df6['Test Intensity']

    time1 += time0[len(time0) - 1]
    time2 += time1[len(time1) - 1]
    time3 += time2[len(time2) - 1]
    time4 += time3[len(time3) - 1]
    time5 += time4[len(time4) - 1]
    time6 += time5[len(time5) - 1]

    intensity_fig = plt.figure()
    plt.plot(time0, intensity0)
    plt.plot(time1, intensity1)
    plt.plot(time2, intensity2)
    plt.plot(time3, intensity3)
    plt.plot(time4, intensity4)
    plt.plot(time5, intensity5)
    plt.plot(time6, intensity6)
    plt.title('Intensity Change with Time')
    plt.xlabel('Time (days)')
    plt.ylabel(r'Power (au)')
    plt.grid(True)
    intensity_fig.savefig(r'C:\Users\ChrisMcGuinness\Pictures\Data Analysis\Reliability Tests Intensity.png')
    plt.show()


def main():
    file0 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test1 Data.csv'
    file1 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test2 Data.csv'
    file2 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test3 Data.csv'
    file3 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test4 Data.csv'
    file4 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test5 Data.csv'
    file5 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test6 Data.csv'
    file6 = r'C:\Users\ChrisMcGuinness\Documents\Parker Hannifan\Reliability Test7 Data.csv'
    df0 = read_csv_data(file0)
    df1 = read_csv_data(file1)
    df2 = read_csv_data(file2)
    df3 = read_csv_data(file3)
    df4 = read_csv_data(file4)
    df5 = read_csv_data(file5)
    df6 = read_csv_data(file6)

    # need to update Polynomial_fits function to include data sets from df4 onwards
    # Polynomial_fits(df0, df1, df2, df3, df4)
    # filtering(df0, df1, df2, df3, df4, df5, df6)
    filtering(df0, df1, df2, df3, df4, df5, df6)

    # intensity(df0, df1, df2, df3, df4, df5, df6)

if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()

    print(f'Done in {end-start} seconds')
