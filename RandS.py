import pyvisa as visa
import time


class RandS(object):
    def __init__(self, address="GPIB0::30::INSTR"):

        rm = visa.ResourceManager()
        self.address = address
        # --- Need to double-check GPIB address
        try:
            self.RS = rm.open_resource(address)
        except Exception as e:
            print("ThorLabs PRO8000 not connected")
            print(e)

    def initialise_section_voltage(self, section, voltage):
        # time.sleep(0.1)
        self.RS.write(f"INST:NSEL {section}")
        # time.sleep(0.1)
        self.RS.write(f"VOLT {voltage}")
        self.RS.write(f"CURR 0.05")
        self.RS.write(f"OUTP:SEL 1")
        self.RS.write(f"OUTP 1")

    def change_section_voltage(self, section, voltage):
        """
        This function is designed to change the voltage on the ring or other section of the device that needs voltage swept.
        Takes the section and the desired voltage.
        This will be a reverse biased voltage so -5V input will set section to -5V. +5V input will also set section to -5V.
        Will call functions for the R&S HMP4040 from instrs and set the section voltage.
        """
        # time.sleep(0.1)
        self.RS.write(f"INST:NSEL {section}")
        # device.write(f"INST OUT{section}")
        # time.sleep(0.1)
        self.RS.write(f"VOLT {voltage}")

    def shutdown_section_voltage(self, section):
        """
        Function to shut down the voltage on each section when the test is completed.
        Takes device object and section number 1, 2, 3, 4.
        """
        # time.sleep(0.1)
        self.RS.write(f"INST:NSEL {section}")
        # time.sleep(0.1)
        self.RS.write(f"OUTP:SEL 0")
        self.RS.write(f"OUTP 0")
