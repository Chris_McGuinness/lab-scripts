import pyvisa as visa
import time
import re


########################################################################################################################
class HPWavemeter(object):
    def __init__(self, address="GPIB0::20::INSTR"):
        rm = visa.ResourceManager()
        self.address = address
        self.wavemeter = rm.open_resource(address)
        self.wavemeter.write("*CLS")
        self.continuous_sweep_off()
        self.wavemeter.query("MEAS:ARR:POW?")
        self.wavemeter.query("FETCh:ARR:POW:WAV?")

    def check_if_complete(self):
        print(self.wavemeter.query("*OPC?"))

    def system_test(self):
        print(self.wavemeter.query("*TST?"))

    def continuous_sweep_on(self):
        # time.sleep(1)
        self.wavemeter.write(':INITiate:CONTinuous ON')

    def continuous_sweep_off(self):
        # time.sleep(1)
        self.wavemeter.write(":INIT:CONT OFF")

    def corrected_power(self):
        self.wavemeter.write(":CALC2")

    def set_threshold(self, threshold):
        self.wavemeter.write(f":CALC2:PTHR {threshold}")

    def read_power_and_wavelength(self):
        """
        Measure first then fetch second so both data points come from the same sweep of the wavemeter.
        Must measure power then fetch wavelength to ensure list of peaks is in order of power.
        Must fetch wavelength data before measuring S/N data as these are part of separate systems.
        """
        power = self.wavemeter.query("MEAS:ARR:POW?")
        # power = wavemeter.query('FETCh:ARR:POW?')
        self.wavemeter.query("*OPC?")
        power = re.split(",|\n", power)
        # wavelength = wavemeter.query("MEAS:ARR:POW:WAV?")
        wavelength = self.wavemeter.query("FETCh:ARR:POW:WAV?")
        wavelength = re.split(",|\n", wavelength)
        self.wavemeter.query("*OPC?")

        # print(power)
        # print(wavelength)

        if int(power[0]) == 0:
            peak_pw = 0
            peak_wl = 0
            smsr = 0
        elif int(power[0]) == 1:
            peak_pw = float(power[1])
            peak_wl = float(wavelength[1]) * 1e9
            smsr = 40
        else:
            peak_pw = float(power[1])
            peak_wl = float(wavelength[1]) * 1e9
            smsr = float(power[1]) - float(power[2])

        print(f"Peak Wavelength: {peak_wl} Peak Power: {peak_pw} dBm SMSR: {smsr} dBm")
        self.wavemeter.query("*OPC?")
        # print("All Power:", power)
        # print("All Wavelength:", wavelength)

        return peak_wl, peak_pw, smsr

    def read_wavelength(self):
        # wavelength = wavemeter.query("FETCh:SCAL:POW:WAV?")
        wavelength = self.wavemeter.query("MEAS:ARR:POW:WAV?")
        re.split(",|\n", wavelength)
        peak_wl = float(wavelength[1]) * 1e9
        print("Wavelength:", wavelength, float(peak_wl))
        return peak_wl

    def read_power(self):
        # power = wavemeter.query("FETCh:SCAL:POW?")
        power = self.wavemeter.query('MEAS:ARR:POW?')
        # power = re.split(",|\n", power)
        print("Power:", power, float(power))
        return power

    def signal_noise_ratio(self):
        sn = self.wavemeter.write(":CALC3:SNR:STAT ON")
        # sn = wavemeter.query(":CALC3:POIN?")
        # time.sleep(0.5)
        sn = self.wavemeter.query(":CALC3:DATA? POW")
        print("Signal/Noise:", sn)
        return sn

########################################################################################################################
# def main():
#     wavemeter = HP68120C_setup()
#
#     continuous_sweep_off(wavemeter)
#     set_threshold(wavemeter, 40)
#
#     # corrected_power(wavemeter)
#     # start = time.time()
#     for i in range(10000):
#         meas_time = time.time()
#         wavelength, power, smsr = read_power_and_wavelength(wavemeter)
#     # end = time.time()
#     # duration = (end - start)
#     # print(f"Done in {duration} seconds")
#
#
# if __name__ == "__main__":
#     main()
