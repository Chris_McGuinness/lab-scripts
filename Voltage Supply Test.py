from instrs import rs_hmp2030
import pyvisa as visa

rm = visa.ResourceManager()
instruments = rm.list_resources()
print("Connected devices \n", instruments, "\n")

Voltage_supply = rs_hmp2030(comm="COM3", tmout=1000)

# channel 1
# Voltage_supply.set_voltage(channel=1, voltage=11)

# channel 2
# Voltage_supply.set_voltage(2, 1)

# channel 3
# Voltage_supply.set_voltage(3, 1)
