from instrs import ldx3207b
import time
import pyvisa as visa

rm = visa.ResourceManager()
instruments = rm.list_resources()
print("Connected devices \n", instruments, "\n")

Current_supply = ldx3207b()

### Trying to change output to on or off
print('Setting output on...')
Current_supply.set_output_on()
time.sleep(2)
print('Setting output off...')
Current_supply.set_output_off()

### Trying to read values from the Current Supply
# print('Initialisation test: \n',
#       'Current Limit =', Current_supply.return_current_lim())
print('Setting current limit...')
Current_supply.set_current_lim(50)
print('New Current Limit =', Current_supply.return_current_lim())
# time.sleep(5)
# print('\n Voltage at start =', Current_supply.return_measured_voltage())
# # # time.sleep(5)
# print('\n Current at start =', Current_supply.return_measured_current())

# Trying to change the current
# Current_supply.set_current(50)
# print('New Current =',  Current_supply.return_measured_current())
