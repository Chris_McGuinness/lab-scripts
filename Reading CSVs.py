import csv
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def make_dataframe(file):
    """
    Takes a test file.
    Prints number of tests in file.
    Returns a Pandas Dataframe.
    """

    f = open(file, 'r')
    data = f.readlines()
    f.close()

    # read data as pandas dataframe
    df = pd.read_csv(file)
    tests = len(df['Test Number'])
    print(f'Number of tests in file = {tests}')
    return df

def examine_test(dataframe, test_number):
    """
    Takes a Pandas Dataframe to pull values from.
    Returns three arrays, being: Voltages tested over, Power from each of the three test sets.
    """

    # reading chosen test from dataframe

    voltage = np.zeros(len(dataframe.iloc[test_number]['Voltage'][1:-1].split()))
    power_1 = np.zeros(len(dataframe.iloc[test_number]['Power 1'].split()))
    power_2 = np.zeros(len(dataframe.iloc[test_number]['Power 2'].split()))
    power_3 = np.zeros(len(dataframe.iloc[test_number]['Power 3'].split()))

    for i in range(len(dataframe.iloc[test_number]['Power 1'].split())):
        if i == 0:
            power_1[i] = float(dataframe.iloc[test_number]['Power 1'].split()[i][1:])
            power_2[i] = float(dataframe.iloc[test_number]['Power 2'].split()[i][1:])
            power_3[i] = float(dataframe.iloc[test_number]['Power 3'].split()[i][1:])
        elif i == len(dataframe.iloc[test_number]['Power 1'].split())-1:
            power_1[i] = float(dataframe.iloc[test_number]['Power 1'].split()[i][:-1])
            power_2[i] = float(dataframe.iloc[test_number]['Power 2'].split()[i][:-1])
            power_3[i] = float(dataframe.iloc[test_number]['Power 3'].split()[i][:-1])
        else:
            power_1[i] = float(dataframe.iloc[test_number]['Power 1'].split()[i])
            power_2[i] = float(dataframe.iloc[test_number]['Power 2'].split()[i])
            power_3[i] = float(dataframe.iloc[test_number]['Power 3'].split()[i])
        voltage[i] = float(dataframe.iloc[test_number]['Voltage'][1:-1].split()[i])

    # print(voltage)
    # print(power_1)
    # print(power_2)
    # print(power_3)

    return voltage, power_1, power_2, power_3

def plot_test_data(voltage, power_1, power_2, power_3):
    """
    Takes four arrays. Voltage and power values from tests.
    Plots the three tests.
    Does not return
    """


    plt.figure()
    plt.plot(voltage, power_1, '-', label='Voltage 1')
    plt.plot(voltage, power_2, '-', label='Voltage 2')
    plt.plot(voltage, power_3, '-', label='Voltage 3')
    plt.title('Voltage (V) vs Power (W)')
    plt.xlabel('Voltage (V)')
    plt.ylabel('Power (W)')
    plt.legend(loc='best')
    plt.grid()
    plt.show()

def main():
    # file = r'Test_Data/1655998742.7744484.csv'
    file = r'Test_Data/downloaded.csv'
    dataframe = make_dataframe(file)

    voltage, power_1, power_2, power_3 = examine_test(dataframe, 0)
    plot_test_data(voltage, power_1, power_2, power_3)

if __name__ == '__main__':
    main()
