import random
import numpy as np
import time
import csv
import gitlab
import threading
import pandas as pd
import json

"""Brainstorming initial ideas for the code for the thermal experiment"""


"""Initializing requirements to commit to GitLab"""
url = r'https://gitlab.com/Chris_McGuinness/lab-scripts/Test Data.git'
full_access_token = 'glpat-ydA7MQE8kdCHynTeN_8J' # access token must be made in account settings
# id = 37050623 # ID of project commit is going to
id = 37247739
gl = gitlab.Gitlab(private_token=full_access_token)#url=url,  private_token=full_access_token)

project = gl.projects.get(id)


def current_supply(amp=50.0):
    """
    Set current in mA. Defaults to 50mA
    """
    return amp

def voltage_1(volt=10.0):
    """
    Sets voltage supply 1 in V. Defaults to 10V
    """
    # sleeps for 0.1 seconds to simulate the time needed to change voltage value
    time.sleep(0.001)
    return volt

def voltage_2(volt=10.0):
    """
    Sets voltage supply 2 in V. Defaults to 10V
    """
    # sleeps for 0.1 seconds to simulate the time needed to change voltage value
    time.sleep(0.001)
    return volt

def voltage_3(volt=10.0):
    """
    Sets voltage supply 3 in V. Defaults to 10V
    """
    # sleeps for 0.1 seconds to simulate the time needed to change voltage value
    time.sleep(0.001)
    return volt

def laser_power(c, v1, v2, v3):
    """
    Takes current and three voltage values as arguments. Returns laser power in W
    """
    # placeholder function with some random floating point generation
    # adding some basic exception handling in case a reading cannot be made
    try:
        p = random.uniform(99.5, 100.5) + (1*v1 + 2*v2 + 3*v3) / c
    except:
        p = 1e40
    return p

def run_single_test():
    """
    Will run a single set of tests, cycling through the three voltages supplied in steps of 0.1V.
    Will then return an array with the results of each test. One voltage supply per array.
    """

    # defining the input voltages that will be swept over
    volt_steps = np.arange(0, 10.1, 0.1)

    # creating the arrays which will be returned at the end of the test cycle
    v1_power = np.zeros(len(volt_steps))
    v2_power = np.zeros(len(volt_steps))
    v3_power = np.zeros(len(volt_steps))

    for i in range(len(volt_steps)):
        # function that sets voltage, sleeps then reads power output
        # the resulting power is written into one of the previously defined arrays
        power = laser_power(current_supply(50.0),
                            voltage_1(volt_steps[i]), voltage_2(10), voltage_3(10))
        v1_power[i] = power

    # cycling through voltages for the other two voltage supplies
    for j in range(len(volt_steps)):
        power = laser_power(current_supply(50.0),
                            voltage_1(10), voltage_2(volt_steps[j]), voltage_3(10))
        v2_power[j] = power

    for k in range(len(volt_steps)):
        power = laser_power(current_supply(50.0),
                            voltage_1(10), voltage_2(10), voltage_3(volt_steps[k]))
        v3_power[k] = power

    # returning the three arrays of output powers
    return volt_steps, v1_power, v2_power, v3_power

def initial_commit(project, file):
    """
    Initialize file on GitLab
    """

    # create a data.json to package the file for committing to GitLab
    data = {
        'branch': 'main',
        'path': 'lab-scripts/Test Data',
        'commit_message': 'Initialize Test Data File',
        'actions': [
            {
                'action': 'create',
                'file_path': file,
                'content': open(file).read()
            }
        ]
    }

    # command that commits data file
    commit = project.commits.create(data)

def final_update_commit(project, file, tests_run):
    """
    Update file on GitLab
    """
    # create a data.json to package the file for committing to GitLab
    data = {
        'branch': 'main',
        'path': 'lab-scripts/Test Data',
        'commit_message': f'Final Test Data Update: Tests Run = {tests_run}',
        'actions': [
            {
                'action': 'update',
                'file_path': file,
                'content': open(file).read()
            }
        ]
    }

    # command that commits data file
    commit = project.commits.create(data)

def update_commit_thread(project, file1, #file2,
                         tests_run):
    """
    Update file on GitLab. To be run in its own thread.
    """
    try:
        print('Updating File 1 on GitLab...')
         # create a data.json to package the file for committing to GitLab
        data = {
            'branch': 'main',
            'path': 'lab-scripts/Test Data',
            'commit_message': f'Test Data Update: Tests Run ={tests_run}',
            'actions': [
                {
                    'action': 'update',
                    'file_path': file1,
                    'content': open(file1).read()
                }
            ]
        }
        # command that commits data file
        commit = project.commits.create(data)
        print('File 1 Updated!')
    except:
        print('Failed to upload File 1 to GitLab')

    # try:
    #     print('Creating File 2 on GitLab...')
    #      # create a data.json to package the file for committing to GitLab
    #     data = {
    #         'branch': 'main',
    #         'path': 'lab-scripts/Test Data',
    #         'commit_message': f'Test Data: Tests Run ={tests_run}',
    #         'actions': [
    #             {
    #                 'action': 'create',
    #                 'file_path': file2,
    #                 'content': np.load(file2, allow_pickle=True)['arr_0']
    #             }
    #         ]
    #     }
    #     # command that commits data file
    #     commit = project.commits.create(data)
    #     print('File 2 Uploaded!')
    # except:
    #     print('Failed to upload File 2 to GitLab')
    # print('Creating File 2 on GitLab...')
    # # create a data.json to package the file for committing to GitLab
    # data = {
    #     'branch': 'main',
    #     'path': 'lab-scripts/Test Data',
    #     'commit_message': f'Test Data: Tests Run ={tests_run}',
    #     'actions': [
    #         {
    #             'action': 'create',
    #             'file_path': file2,
    #             'content': json.dumps({'file2': np.load(file2, allow_pickle=True)['arr_0'].tolist()})
    #         }
    #     ]
    # }
    # # command that commits data file
    # commit = project.commits.create(data)

def main():
    print('Beginning Script...')
    start = time.time()
    tests_run = 0

    # start dataframe
    # d = {'Time': [], 'Test Number': [], 'Voltage': [],'Power 1': [], 'Power 2': [], 'Power 3': []}
    # df = pd.DataFrame(data=d)

    # Initialize data file on local storage and GitLab
    header = ['Time', 'Test Number', 'Voltage', 'Power 1', 'Power 2', 'Power 3']
    # with open(fr'C:\Users\mcgui\OneDrive\Software\lab-scripts\Test Data\{start}.csv',
    #           'w', newline='') as f:
    with open(fr'Test Data\{start}.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')

        # write the header
        writer.writerow(header)
        f.close()
    try:
        ### will fill this try-except with logging
        print('Initial GitLab Commit...')
        initial_commit(project, fr'Test Data\{start}.csv')
    except:
        print('File was not initialized on GitLab')

    # thread should update the GitLab file
    # update_thread = threading.Thread(update_commit_thread(project, fr'Test Data\{start}.csv'))
    # update_thread.start()

    # running tests until keyboard interrupt
    try:
        while True:
            v, p1, p2, p3 = run_single_test()

            # might want to record intended voltage supplied vs actual voltage reading to see if there's a difference

            # print(p1, '\n', p2, '\n', p3)
            tests_run += 1
            print(f'Tests run = {tests_run}')
            test_end = time.time()

            # d = {'Time':[test_end], 'Test Number':[tests_run], 'Voltage':[np.array(v)],
            #      'Power 1':[np.array(p1)], 'Power 2':[np.array(p2)], 'Power 3':[np.array(p3)]}
            # df2 = pd.DataFrame(data=d)
            # df = pd.concat([df, df2], ignore_index = True)
            # df.to_csv('Test Data\dataframe_test.csv')

            data = [test_end, tests_run, v, p1, p2, p3]

            with open(fr'Test Data\{start}.csv',
                      'a', newline='') as f:#, encoding='UTF8') as f:
                writer = csv.writer(f, delimiter=',')

                # write the data
                writer.writerow(data)
                f.close()
            if tests_run%10 == 0:
                np.savez(fr'Test Data\Test = {tests_run}.npz', data)
                update_thread = threading.Thread(update_commit_thread(project,
                                                                      fr'Test Data\{start}.csv',
                                                                      # fr'Test Data\Test = {tests_run}.npz',
                                                                      tests_run))
                update_thread.start()
                # try:
                #     print('Updating file on GitLab...')
                #     update_commit(project, fr'Test Data\{start}.csv')
                # except:
                #     print('Failed to upload to GitLab')

    # keyboard interrupt and final commit to GitLab
    except KeyboardInterrupt:
        end = time.time()
        try:
            print('Updating file on GitLab...')
            final_update_commit(project, fr'Test Data\{start}.csv', tests_run)
            print('File Updated!')
        except:
            print('Failed final upload to GitLab')
        print('Done in', float(end - start), 'seconds \nTests Run =', tests_run)
        pass

if __name__ == '__main__':
    main()
