import pyvisa as visa
import time
import numpy as np


########################################################################################################################
class HP_ESA(object):
    def __init__(self, address="GPIB0::18::INSTR"):
        rm = visa.ResourceManager()
        self.address = address
        self.ESA = rm.open_resource(address)
        self.ESA.write("*CLS")

    def get_settings(self):
        sf = float(self.ESA.query(f"FA?"))
        ef = float(self.ESA.query(f"FB?"))
        cf = float(self.ESA.query(f"CF?"))
        sp = float(self.ESA.query(f"SP?"))
        rl = float(self.ESA.query(f"RL?"))
        au = self.ESA.query(f"AUNITS?")
        rb = float(self.ESA.query(f"RB?"))
        vb = float(self.ESA.query(f"VB?"))
        st = float(self.ESA.query(f"ST?"))
        settings = (sf, ef, cf, sp, rl, au, rb, vb, st)
        print(f"Settings are now: \nStart Frequency = {sf / 1e6} MHZ \nEnd Frequency = {ef / 1e6} MHZ "
              f"\nCentre Frequency = {cf / 1e6} MHZ \nFrequency Span = {sp / 1e6} MHZ "
              f"\nReference Level = {rl}dBm \nAmplitude Units = {au}Resolution Bandwidth = {rb / 1e6}MHZ "
              f"\nVideo Bandwidth = {vb / 1e6}MHZ \nSweep Time = {st}s \n")
        return settings

    def frequency_range(self, start_frequency, end_frequency):
        self.ESA.write(f"FA {start_frequency}MHZ")
        self.ESA.write(f"FB {end_frequency}MHZ")
        setup = self.get_settings()
        # gives the ESA time to update before any data is taken
        time.sleep(2)
        return setup

    def frequency_span(self, centre_frequency, frequency_span):
        self.ESA.write(f"CF {centre_frequency}MHZ")
        self.ESA.write(f"SP {frequency_span}MHZ")
        setup = self.get_settings()
        # gives the ESA time to update before any data is taken
        time.sleep(3)
        return setup

    def sweep_time(self, sweep_time):
        """
        Takes ESA object and sweep time in seconds.
        Changes the sweep time of the ESA.
        """
        self.ESA.write(f"ST {sweep_time}")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def continuous_sweep(self):
        self.ESA.write("CONTS")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def single_sweep(self):
        self.ESA.write("SNGLS")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def full_span(self):
        self.ESA.write("FS")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def log_scale(self, scale):
        """
        Takes an ESA object and a log scale in dB.
        Changes the log scaling of the ESA to the specified value.
        """
        self.ESA.write(f"LG {scale}DB")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def reference_level(self, level):
        """
        Takes an ESA object and a reference level in dB.
        Changes the reference level of the ESA to the specified value.
        """
        self.ESA.write(f"RL {level}DB")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def resolution_bandwidth(self, rb):
        """
        Takes an ESA object and a resolution bandwidth in MHZ.
        Changes the log scaling of the ESA to the specified value.
        """
        self.ESA.write(f"RB {rb}MHZ")
        # gives the ESA time to update before any data is taken
        time.sleep(2)

    def power_bandwidth(self):
        """
            Takes an ESA object.
            Calculates the bandwidth at 99% of peak power,
            corresponding to -20dB.
        """
        self.ESA.write("OCCUP 99.0")
        bandwidth = self.ESA.query("PWRBW?")
        return bandwidth

    def pull_trace_A_spectrum(self):
        """
        Takes trace data from the ESA
        """
        # checks setting values are correct
        trace_settings = self.get_settings()
        # setup_analyzer(ESA_object, *trace_settings)
        trace_data = self.ESA.query("TDF P;TRA?")
        frequency_data = np.linspace(trace_settings[0] / 1e6, trace_settings[1] / 1e6, 601)
        return trace_data, frequency_data, trace_settings

    def pull_trace_B_spectrum(self, centre_frequency, frequency_span):
        """
        Takes Centre Frequency and Frequency span in MHz
        """
        # checks setting values are correct
        trace_settings = self.get_settings()
        trace_data = self.ESA.query("TDF P;TRB?")
        # return np.array([trace_data]), trace_settings
        return trace_data, trace_settings
########################################################################################################################

# --- Example Below
# def main():
#     # print('Devices found:')
#     # print(rm.list_resources())  # use this to figure out the address of the OSA
#     # print('Connecting to OSA...')
#
#     # --- CHANGE THIS TO MATCH YOUR OSA:
#     ESA_address = "GPIB0::18::INSTR"
#     # ----------------------------------
#
#     # connects to the device and tests connection
#     HP_ESA = ESA_connection()
#
#     centre_frequency = 275  # MHz
#     freq_span = 40  # MHz
#     start_freq = 250  # MHz
#     end_freq = 300  # MHz
#     sweep_length = 1  # S
#     axis_log_scale = 10  # DB
#     res_bandwidth = 0.3  # MHZ
#     ref_level = 8  # DBM
#
#     sweep_time(HP_ESA, sweep_length)
#     # single_sweep(HP_ESA)
#     continuous_sweep(HP_ESA)
#     log_scale(HP_ESA, axis_log_scale)
#     resolution_bandwidth(HP_ESA, res_bandwidth)
#     reference_level(HP_ESA, ref_level)
#     get_settings(HP_ESA)
#
#     print(f"Bandwidth at 99% power: {power_bandwidth(HP_ESA)}")
#
#     # set frequency
#     # frequency_span(HP_ESA, centre_frequency, freq_span)
#     # frequency_range(HP_ESA, start_freq, end_freq)
#     # full_span(HP_ESA)
#
#     # take data
#     # raw_data, trace_freq, settings = pull_trace_A_spectrum(HP_ESA)
#     # reduced_data = []
#     # for i in raw_data.split(","):
#     #     reduced_data.append(float(i))
#     #
#     # plt.plot(trace_freq, reduced_data)
#     # plt.show()
#
# if __name__ == '__main__':
#     start = time.time()
#     main()
#     end = time.time()
#
#     print(f'Done in {end - start} seconds')
