import gitlab
import time

def initial_commit(file):
    # create a data.json to package the file for committing to GitLab
    data = {
        'branch': 'main',
        'path': 'lab-scripts/Test_Data',
        'commit_message': 'commit with gitlab-python',
        'actions': [
            {
                'action': 'create',
                'file_path': file,
                'content': open(file).read()
            }
        ]
    }

    # command that commits data file
    commit = project.commits.create(data)


def update_commit(file):
    # create a data.json to package the file for committing to GitLab
    data = {
        'branch': 'main',
        'path': 'lab-scripts/Test_Data',
        'commit_message': 'commit with gitlab-python',
        'actions': [
            {
                'action': 'update',
                'file_path': file,
                'content': open(file).read()
            }
        ]
    }

    # command that commits data file
    commit = project.commits.create(data)

# the project id, and an access token are both required for access to the project you wish to commit to
url = r'https://gitlab.com/Chris_McGuinness/lab-scripts/Test_Data.git'
full_access_token = 'glpat-ydA7MQE8kdCHynTeN_8J' # access token must be made in account settings
# id = 37050623 # ID of project commit is going to
id = 37247739
gl = gitlab.Gitlab(private_token=full_access_token)#url=url,  private_token=full_access_token)

project = gl.projects.get(id)
# commits = project.commits.list(all=True)
# print('Project Attributes:\n', project.attributes)
# print('Commits:\n', commits)
# branches = project.branches.list()
# print('Branches:\n', branches)

# location of file to be sent to GitLab
# file = r'C:\Users\mcgui\OneDrive\Software\lab-scripts\Test_Data\1655822303.8464978.csv'
file = r'Test_Data\1655822303.8464978.csv'

# create a data.json to package the file for committing to GitLab
# data = {
#     'branch': 'main',
#     'path': 'lab-scripts/Test_Data',
#     'commit_message': 'commit with gitlab-python',
#     'actions': [
#         {
#             'action': 'create',
#             'file_path': file,
#             'content': open(file).read()
#         }
#     ]
# }

# command that commits data file
# commit = project.commits.create(data)

print('Initial Commit...')
initial_commit(file)
time.sleep(10)
print('Update Commit...')
update_commit(file)