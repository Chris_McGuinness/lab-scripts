import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

file = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\GE Photodiode Spectral Response.csv'
df = pd.read_csv(file)

print(df)

x = df['Wavelength']
y = df['Response']

R = np.abs(x - 1310).argmin()
# print(x[R], y[R])
text = f'Responsivity at 1310nm = {y[R]:.3f}'

plt.plot(x, y)
plt.plot(x[R], y[R], color='red', marker='x')
plt.xlabel('Wavelength (nm)')
plt.ylabel('Responsivity')
plt.title('GE Photodiode Responsivity')
plt.text(800, 0.8, text)
plt.show()