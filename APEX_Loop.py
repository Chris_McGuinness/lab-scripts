from PyApex import AP2XXX
from matplotlib import pyplot as plt
import numpy as np
from scipy import signal

class APEX(object):
    def __init__(self):
        self.MyAP2XXX = AP2XXX("10.100.3.86")
        self.MyOSA = self.MyAP2XXX.OSA()

    def trace_setup(self, start_wl, end_wl):#, resolution=0.16):
        s = float(start_wl)
        e = float(end_wl)
        self.MyOSA.SetScaleXUnit("nm")
        self.MyOSA.SetStartWavelength(s)
        self.MyOSA.SetStopWavelength(e)
        self.MyOSA.DeactivateAutoNPoints()
        npoints = int(abs(s-e)*10000)
        self.MyOSA.SetNPoints(npoints)

    def pull_trace(self):
        Trace = self.MyOSA.Run()
        # If the single is good (Trace > 0), we get the data in a list Data = [[Power Data], [Wavelength Data]]
        bASCII_data = True
        if Trace > 0:
            if bASCII_data == True:
                Data = self.MyOSA.GetData("nm","log",1)
            else:
                Data = self.MyOSA.GetDataBin("nm","log",1)
        return Data[0], Data[1]
    
    # def find_peak(self):
    #     peak = self.MyOSA.FindPeak(ThresholdValue=-50, Axis="Y", Find="MAX")
    #     return peak
    
    def find_peak(self, wavelength, power):
        try:
            peak_power = np.max(power)
            peak_wavelength = wavelength[power.index(peak_power)]
            return peak_wavelength, peak_power
        except Exception as Exc:
            print(Exc)
            return None, None
        
    def calculate_SMSR(self, power, threshold):
        try:
            peaks, _ = signal.find_peaks(power, height=threshold, prominence=10)
            prominences = signal.peak_prominences(power, peaks)
            background_peaks, _ = signal.find_peaks(power, prominence=(None, 1))
            # SMSR_estimate
            # --- Looking for background for each peak to estimate SMSR for each peak
            for i in range(len(peaks)):
                # --- first method of estimating SMSR is to subtract the power of the closest background peak
                index = (np.abs(background_peaks - peaks[i])).argmin()
                # # print(background_peaks[index])
                SMSR1 = power[peaks[i]]-power[background_peaks[index]]

            return SMSR1
        except:
            print("No peak or background peak found")
            return -100
    
    def plot_trace(self, wavelength, power):
        plt.grid(True)
        plt.plot(wavelength, power)
        plt.xlabel("Wavelength (nm)")
        plt.ylabel("Power (dBm)")
        plt.show()

    def osa_disconnect(self):
        # The connection with the OSA is closed
        self.MyAP2XXX.Close()
