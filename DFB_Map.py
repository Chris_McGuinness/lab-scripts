import numpy as np
import matplotlib.pyplot as plt
import time
import sys
sys.path.insert(1, r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\PythonScripts")
from new_instrs import ldx3207b
# from new_instrs import HP_wavemeter
import APEX_Loop

channel = 5
I_o = 0  # mA
I_f = 20  # mA
I_step = 1  # mA
I = np.arange(I_o, I_f, I_step)

T_o = 20  # oC
T_f = 30  # oC
T_step = 5  # oC
T = np.arange(T_o, T_f, T_step)

data = np.zeros((3, len(T), len(I)))

"""
Initialise connection to devices
"""
ILX = ldx3207b()
# HP = HP_wavemeter()
APEX = APEX_Loop.APEX()
APEX.trace_setup(1550.1, 1550.3)

file = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\DFB_Map_{time.time()}.npz"


def main():
    """
    Initialise the ILX Laser current and TEC for the chosen channel
    """
    ILX.swap_channel(channel)
    ILX.TEC_set_temp(T_o)
    ILX.TEC_on()
    ILX.LD_off()
    ILX.LD_set_current(I_o)

    T_now = ILX.TEC_read_temp()
    I_now = ILX.LD_read_current()

    """
    Wait until the Current and Temperature have settled
    """

    while I_now != I_o:
        I_now = ILX.LD_read_current()

    while T_now != T_o:
        T_now = ILX.TEC_read_temp()

    ILX.LD_on()

    """
    Main loop "i" changes temperature. Nested loop "j" changes current
    """
    for i in range(len(T)):
        ILX.TEC_set_temp(T[i])
        while T_now != T[i]:
            T_now = ILX.TEC_read_temp()

        for j in range(len(I)):
            ILX.LD_set_current(I[j])
            while I_now != I_o:
                I_now = ILX.LD_read_current()
            """
            Read data from device
            """
            # d = HP.read_peaks()
            p, wl = APEX.pull_trace()
            peak_wl, peak_p = APEX.find_peak(wl, p)
            smsr = APEX.calculate_SMSR(p, threshold=-50)
            print(peak_wl, peak_p, smsr)
            time.sleep(0.5)
            # APEX.plot_trace(wl, p)

            data[0,i,j] = peak_wl  # d[0]
            data[1,i,j] = peak_p  # d[1]
            data[2,i,j] = smsr  # d[2]

    """
    Switch devices off
    """
    ILX.LD_off()
    APEX.osa_disconnect()
    """
    Saving and plotting data
    """

    # np.savez(file=file, Data=data)

    print(data)
    # fig = plt.figure(figsize=(17, 5))
    # plt.subplot(131)
    # # norm1 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
    # plot1 = plt.imshow(data[0], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
    # cbar = fig.colorbar(plot1)
    # plt.title("Wavelength")
    # plt.xlabel("Current (I)")
    # plt.ylabel("Temperature (oC)")
    # plt.subplot(132)
    # # norm2 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
    # plot2 = plt.imshow(data[1], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
    # cbar = fig.colorbar(plot2)
    # plt.title("Power")
    # plt.xlabel("Current (I)")
    # plt.ylabel("Temperature (oC)")
    # plt.subplot(133)
    # # norm3 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
    # plot3 = plt.imshow(data[2], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
    # cbar = fig.colorbar(plot3)
    # plt.title("SMSR")
    # plt.xlabel("Current (I)")
    # plt.ylabel("Temperature (oC)")
    # plt.show()
    # fig.savefig(
    #     rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Ring Laser Maps\Device_{device}_{start}.png",
    #     dpi=800)
    


if __name__ == "__main__":
    main()
