import pyvisa as visa
import time
import HP68120C as WM
import numpy as np
import matplotlib.pyplot as plt

rm = visa.ResourceManager()

temp = 70
test_duration = 1  # minutes

test_start = time.time()
test_end = test_start + (60 * test_duration)

file_npz = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_{temp}_Duration_{test_duration}_Time_{test_start}.npz"
file_csv = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-2 Thermal Stability Tests\Zero_Point_Temp_{temp}_Duration_{test_duration}_Time_{test_start}.csv"

def main():
    wavemeter = WM.HP68120C_setup()
    WM.continuous_sweep_off(wavemeter)
    WM.set_threshold(wavemeter, 40)

    i = 0
    W = np.array([])
    P = np.array([])
    S = np.array([])


    while time.time() < test_end:
        wavelength, power, smsr = WM.read_power_and_wavelength(wavemeter)
        # if i%10 == 0:
        W = np.append(W, wavelength)
        P = np.append(P, power)
        S = np.append(S, smsr)

    T = np.linspace(test_start, test_end, len(W))
    np.savez(file_npz, Wavelength=W, Power=P, SMSR=S, Time=T)

    f = open(file_csv, 'w')
    f.write("Wavelength,Power,SMSR\n")
    f.close()

    f = open(file_csv, 'a')
    for i in range(len(W)):
        data = f"{W[i]:.5f},{P[i]:.5f},{S[i]:.5f}\n"
        f.write(data)
    f.close()

    print(W)

    # plt.plot(T, W)
    # plt.plot(T, P)
    # plt.plot(T,S)
    # plt.show()


if __name__ == "__main__":
    main()
