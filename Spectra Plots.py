import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

file0 = r'Path.txt'
# file1 =
# file2 =
# file3 =
# file4 =
# file5 =

all_files = [file0]#, file1, file2, file3, file4, file5]


spectra = plt.figure()
for file in all_files:
    df = pd.read_csv(file, sep="\t", header=2)
    plt.plot(df['nm'], df['dBm'])
    plt.xlabel('Wavelength (nm)')
    plt.ylabel('Power (dBm)')
    plt.xticks(np.arange(1536, 1567, 0.5))
    plt.xticks(rotation=45)
    plt.xticks(fontsize=6)
    # plt.xlim(1544, 1550)
    # plt.ylim(-90, 10)
# spectra.savefig(r'Path.jpg') #, dpi=1200)
plt.show()
