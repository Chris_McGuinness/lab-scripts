import numpy as np
import matplotlib.pyplot as plt

input_power = np.array([6, 7.5, 10, 13.5]) # dBm
direct_power = np.array([5.95, 7.46, 9.95, 13.44]) # dBm
### 0V
DiCon1_0V_1550 = np.array([2, 3.48, 5.98, 9.48]) # dBm
DiCon2_0V_1550 = np.array([1.53, 3.02, 5.53, 9.02]) # dBm
DiCon3_0V_1550 = np.array([1.60, 3.09, 5.58, 9.08]) # dBm

DiCon1_0V_1530 = np.array([2.37, 3.76, 6.27, 9.77]) # dBm
DiCon2_0V_1530 = np.array([1.72, 3.24, 5.72, 9.23]) # dBm
DiCon3_0V_1530 = np.array([1.95, 3.44, 5.92, 9.42]) # dBm

DiCon1_0V_1565 = np.array([1.94, 3.42, 5.91, 9.42]) # dBm
DiCon2_0V_1565 = np.array([1.44, 2.95, 5.45, 8.95]) # dBm
DiCon3_0V_1565 = np.array([1.60, 3.11, 5.59, 9.08]) # dBm

### 5V
DiCon1_5V_1550 = np.array([1.87, 3.38, 5.88, 9.38]) # dBm
DiCon2_5V_1550 = np.array([1.61, 3.11, 5.61, 9.10]) # dBm
DiCon3_5V_1550 = np.array([1.44, 2.92, 5.42, 8.92]) # dBm

DiCon1_5V_1530 = np.array([2.17, 3.66, 6.15, 9.65]) # dBm
DiCon2_5V_1530 = np.array([1.77, 3.27, 5.75, 9.27]) # dBm
DiCon3_5V_1530 = np.array([1.83, 3.32, 5.81, 9.30]) # dBm

DiCon1_5V_1565 = np.array([1.80, 3.27, 5.80, 9.31]) # dBm
DiCon2_5V_1565 = np.array([1.56, 3.02, 5.53, 9.01]) # dBm
DiCon3_5V_1565 = np.array([1.40, 2.92, 5.41, 8.89]) # dBm

########################################################################################################################
print(f'0V Loss Averages:'
      f'\nSwitch 1 1550nm = {np.mean(direct_power - DiCon1_0V_1550)}'
      f'\nSwitch 2 1550nm = {np.mean(direct_power - DiCon2_0V_1550)}'
      f'\nSwitch 3 1550nm = {np.mean(direct_power - DiCon3_0V_1550)}'
      f'\nSwitch 1 1530nm = {np.mean(direct_power - DiCon1_0V_1530)}'
      f'\nSwitch 2 1530nm = {np.mean(direct_power - DiCon2_0V_1530)}'
      f'\nSwitch 3 1530nm = {np.mean(direct_power - DiCon3_0V_1530)}'
      f'\nSwitch 1 1565nm = {np.mean(direct_power - DiCon1_0V_1565)}'
      f'\nSwitch 2 1565nm = {np.mean(direct_power - DiCon2_0V_1565)}'
      f'\nSwitch 3 1565nm = {np.mean(direct_power - DiCon3_0V_1565)}'
      )

print(f'5V Loss Averages:'
      f'\nSwitch 1 1550nm = {np.mean(direct_power-DiCon1_5V_1550)}'
      f'\nSwitch 2 1550nm = {np.mean(direct_power-DiCon2_5V_1550)}'
      f'\nSwitch 3 1550nm = {np.mean(direct_power-DiCon3_5V_1550)}'
      f'\nSwitch 1 1530nm = {np.mean(direct_power - DiCon1_5V_1530)}'
      f'\nSwitch 2 1530nm = {np.mean(direct_power - DiCon2_5V_1530)}'
      f'\nSwitch 3 1530nm = {np.mean(direct_power - DiCon3_5V_1530)}'
      f'\nSwitch 1 1565nm = {np.mean(direct_power - DiCon1_5V_1565)}'
      f'\nSwitch 2 1565nm = {np.mean(direct_power - DiCon2_5V_1565)}'
      f'\nSwitch 3 1565nm = {np.mean(direct_power - DiCon3_5V_1565)}'
      )

fig1 = plt.figure()
plt.plot(input_power, direct_power, linestyle='dashdot', label='Direct Power')
plt.plot(input_power, DiCon1_0V_1550, linestyle='dashdot', label='Switch 1 (0V)')
plt.plot(input_power, DiCon1_5V_1550, linestyle='dashdot', label='Switch 1 (5V)')
plt.plot(input_power, DiCon2_0V_1550, linestyle='dashdot', label='Switch 2 (0V)')
plt.plot(input_power, DiCon2_5V_1550, linestyle='dashdot', label='Switch 2 (5V)')
plt.plot(input_power, DiCon3_0V_1550, linestyle='dashdot', label='Switch 3 (0V)')
plt.plot(input_power, DiCon3_5V_1550, linestyle='dashdot', label='Switch 3 (5V)')
plt.grid(True)
plt.xlabel('Input Power (dBm)')
plt.ylabel('Output Power (dBm)')
plt.title('Absolute Output Power for Three Switches')
plt.legend(loc='best')
plt.show()

fig2 = plt.figure()
ax2 = plt.subplot(111)
plt.plot(input_power, direct_power - DiCon1_0V_1530, color='tab:blue', linestyle='dashdot', label='Switch 1 1530')
plt.plot(input_power, direct_power - DiCon1_0V_1550, color='tab:blue', linestyle='solid', label='Switch 1 1550')
plt.plot(input_power, direct_power - DiCon1_0V_1565, color='tab:blue', linestyle='dashed', label='Switch 1 1565')

plt.plot(input_power, direct_power - DiCon2_0V_1530, color='tab:orange', linestyle='dashdot', label='Switch 2 1530')
plt.plot(input_power, direct_power - DiCon2_0V_1550, color='tab:orange', linestyle='solid', label='Switch 2 1550')
plt.plot(input_power, direct_power - DiCon2_0V_1565, color='tab:orange', linestyle='dashed', label='Switch 2 1565')

plt.plot(input_power, direct_power - DiCon3_0V_1530, color='tab:green', linestyle='dashdot', label='Switch 3 1530')
plt.plot(input_power, direct_power - DiCon3_0V_1550, color='tab:green', linestyle='solid', label='Switch 3 1550')
plt.plot(input_power, direct_power - DiCon3_0V_1565, color='tab:green', linestyle='dashed', label='Switch 3 1565')
plt.grid(True)
plt.xlabel('Input Power (dBm)')
plt.ylabel('Drop in Output Power (dBm)')
plt.title('Optical Loss for Three Switches at 0V')

box = ax2.get_position()
ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
# Put a legend to the right of the current axis
ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()

fig3 = plt.figure()
ax3 = plt.subplot(111)
plt.plot(input_power, direct_power - DiCon1_5V_1530, color='tab:blue', linestyle='dashdot', label='Switch 1 1530')
plt.plot(input_power, direct_power - DiCon1_5V_1550, color='tab:blue', linestyle='solid', label='Switch 1 1550')
plt.plot(input_power, direct_power - DiCon1_5V_1565, color='tab:blue', linestyle='dashed', label='Switch 1 1565')

plt.plot(input_power, direct_power - DiCon2_5V_1530, color='tab:orange', linestyle='dashdot', label='Switch 2 1530')
plt.plot(input_power, direct_power - DiCon2_5V_1550, color='tab:orange', linestyle='solid', label='Switch 2 1550')
plt.plot(input_power, direct_power - DiCon2_5V_1565, color='tab:orange', linestyle='dashed', label='Switch 2 1565')

plt.plot(input_power, direct_power - DiCon3_5V_1530, color='tab:green', linestyle='dashdot', label='Switch 3 1530')
plt.plot(input_power, direct_power - DiCon3_5V_1550, color='tab:green', linestyle='solid', label='Switch 3 1550')
plt.plot(input_power, direct_power - DiCon3_5V_1565, color='tab:green', linestyle='dashed', label='Switch 3 1565')
plt.grid(True)
plt.xlabel('Input Power (dBm)')
plt.ylabel('Drop in Output Power (dBm)')
plt.title('Optical Loss for Three Switches at 5V')

box = ax3.get_position()
ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])
# Put a legend to the right of the current axis
ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()