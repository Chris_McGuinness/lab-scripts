import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyvisa as visa
import time
import os

rm = visa.ResourceManager()
# print(rm.list_resources_info())

# --- Functions for the 16 Channel ILX LDC3916
def LDC3916_setup():
    LDC = rm.open_resource("GPIB0::12::INSTR")
    # LDC.clear()
    # print("Connected")
    return LDC

def LDC3916_Laser(LDC, channel, current):

    # --- Select Channel
    # LDC.write(f"CHAN {channel}")

    # --- Laser Setup
    # LDC.write(f"LAS:I {current}")
    LDC.write(f"LAS:OUT ON")
    # LDC.write(f"TEC:OUT 1")

LDC = LDC3916_setup()
LDC3916_Laser(LDC, 0, 0)
