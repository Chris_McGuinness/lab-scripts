import pandas as pd
import numpy as np

def convertion(counts):
    """
    Converts counts to a voltage value between -0.6V and -9.3V.
    Takes a value in Counts. Returns a value in Volts
    """

    return (-8.7/65535)*counts - 0.6

file = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Pilot 1-3 Phase 33142 Counts_5Volts_September5_Normal Regime'
df_in = pd.read_csv(file, sep='\t')

# print(df_in)

# Lists of desired coordinates (x-axis = 'j', y-axis = 'i')
# Coordinates correspond to the centre of a mode on the mode map
# Coordinates = [(31, 50), (72, 103), (68, 146), (92, 183), (66, 195), (94, 222), (14, 150), (18, 185),
#                (23, 203), (31, 222), (34, 242), (172, 181), (190, 212), (193, 229), (208, 247), (244, 226),
#                (234, 12), (210, 22), (235, 65), (206, 82), (199, 117), (200, 144), (239, 202), (161, 11),
#                (169, 46), (148, 65), (128, 85), (142, 116), (143, 136), (77, 41), (77, 8), (35, 8)]

# Removed values that were too close in wavelength to each other
Coordinates = [(31, 50), (72, 103), (68, 146), (92, 183), (66, 195), (94, 222), (14, 150), (18, 185),
               (23, 203), (31, 222), (34, 242), (172, 181), (190, 212), (193, 229), (208, 247),
               (234, 12), (210, 22), (235, 65), (206, 82), (199, 117), (200, 144), (161, 11),
               (169, 46), (148, 65), (128, 85), (142, 116), (143, 136), (77, 41), (35, 8)]

Wavelength = []
BM = []
FM = []
Voltage_1 = []
Voltage_2 = []

# for i in df_in.where(np.abs(df_in['Wavelength (nm)'] - 1550) < 0.1):
# [print(df_in.where(np.abs(df_in['Wavelength (nm)'] - 1550) < 0.1)['Wavelength (nm)']) if df_in.where(np.abs(df_in['Wavelength (nm)'] - 1550) < 0.1)['Wavelength (nm)'] == True:]

# print(df_in.where(np.abs(df_in['Wavelength (nm)'] - 1550) < 0.1))
# for i in range(len(df_in['Wavelength (nm)'])):
    # if np.abs(df_in['Wavelength (nm)'][i] - 1550) < 0.1:
    #     print(df_in[i])

# command that lets me search for the index with specific i and j values
for i in range(len(Coordinates)):
        w = float(df_in.loc[(df_in['i'] == Coordinates[i][1]) & (df_in['j'] == Coordinates[i][0])]['Wavelength (nm)'])
        b = float(df_in.loc[(df_in['i'] == Coordinates[i][1]) & (df_in['j'] == Coordinates[i][0])]['BM (Counts)'])
        f = float(df_in.loc[(df_in['i'] == Coordinates[i][1]) & (df_in['j'] == Coordinates[i][0])]['FM (Counts)'])
        v1 = convertion(b)
        v2 = convertion(f)
        Wavelength.append(w)
        BM.append(b)
        FM.append(f)
        Voltage_1.append(v1)
        Voltage_2.append(v2)

data = {'Wavelength (nm)': Wavelength,
        'BM (Counts)': BM,
        'FM (Counts)': FM,
        'Voltage 1': Voltage_1,
        'Voltage 2': Voltage_2}
df_out = pd.DataFrame(data=data)
# print(df_out.sort_values(by=['Wavelength (nm)']))

# test = df_out.sort_values(by=['Wavelength (nm)'])
# print(test)

W = np.array(df_out.sort_values(by=['Wavelength (nm)'])['Wavelength (nm)'])
B = np.array(df_out.sort_values(by=['Wavelength (nm)'])['BM (Counts)'])
F = np.array(df_out.sort_values(by=['Wavelength (nm)'])['FM (Counts)'])
V1 = np.array(df_out.sort_values(by=['Wavelength (nm)'])['Voltage 1'])
V2 = np.array(df_out.sort_values(by=['Wavelength (nm)'])['Voltage 2'])

df_final = pd.DataFrame({'Wavelength (nm)': W,
                         'BM (Counts)': B,
                         'FM (Counts)': F,
                         'Voltage 1': V1,
                         'Voltage 2': V2})
# print(df_final)

path = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Parker Hannifan\Look-Up Table.csv'
df_final.to_csv(path)

# print(float(df_in.loc[(df_in['BM (Counts)'] == 34952.0) & (df_in['FM (Counts)'] == 36751.0)]['Wavelength (nm)']))
