import pyvisa as visa
import time
import numpy as np

########################################################################################################################
class HP_Laser(object):
    def __init__(self, address="GPIB0::21::INSTR"):
        rm = visa.ResourceManager()
        self.address = address
        self.laser = rm.open_resource(address)
        self.laser.write("*CLS")

    def laser_on(self):
        self.laser.write("1")

    def laser_off(self):
        self.laser.write("0")

    def set_unit_dbm(self):
        self.laser.write(":SOURce:POWer:UNIT: DBM")

    def set_power(self, power):
        self.laser.write(f":SOURce:POWer:LEVel:IMMediate:AMPlitude {power}")

    def set_wavelength(self, wavelength):
        """
        Sets wavelength of the ECL
        MIN wl = 1450 nm
        MAX wl = 1590 nm
        """
        self.laser.write(f"SOURce:WAVElength:CW:FIXED {wavelength}")

    def query_wavelength(self):
        return self.laser.query("SOURce:WAVElength:CW:FIXED?")
