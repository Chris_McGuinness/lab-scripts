import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

# file = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\GE Photodiode Spectral Response.csv'
file = r'C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Si Photodiode Spectral Response.csv'
df = pd.read_csv(file)

# print(df)

x = df['Wavelength'][::10]
y = df['Response'][::10]

print(np.array([x]))
print(np.array([y]))

def si_responsivity(wavelength):
    si_x = np.array([351.3263898, 391.89831317, 432.47023655, 473.04215992, 513.61408329, 554.18600666, 594.75793004,
                     635.32985341, 675.90177678, 716.47370015, 757.04562353, 797.6175469,  838.18947027, 878.76139364,
                     919.33331701, 959.90524039, 1000.47716376, 1025.83461587, 1038.00619288, 1046.12057755,
                     1054.23496223, 1062.3493469,  1069.44943349, 1078.57811625, 1090.74969326])
    si_y = np.array([0.03042965, 0.04440363, 0.09519948, 0.15008666, 0.2057921, 0.26149754, 0.31556646, 0.37454495,
                     0.42861387, 0.48268278, 0.53020558, 0.57691012, 0.61952334, 0.65395392, 0.68920276, 0.71463244,
                     0.71715072, 0.65336581, 0.59610639, 0.53393103, 0.47175566, 0.40630725, 0.34822162, 0.28686611,
                     0.22960668])

    R = np.abs(si_x - wavelength).argmin()
    # print(x[R], y[R])
    text = f'Responsivity at {wavelength}nm = {si_y[R]:.3f}'
    return si_y[R]  # text

# Zn = signal.savgol_filter(y, window_length=11, polyorder=2, deriv=0)
# # Zn = signal.savgol_coeffs(y, window_length=11, polyorder=2, deriv=0)
# print(Zn)

# fit = np.polyfit(x, y, 9)
# print(fit)
# fit_x = np.linspace(x[0], x[len(x)-1], 1000)
# si_polyfit = np.array([1.15792383e-25, -8.14610894e-22, 2.54250548e-18, -4.63461206e-15, 5.46239727e-12,
#                        -4.34837552e-09, 2.36723818e-06, -8.70089946e-04, 2.06625657e-01, -2.86268490e+01,
#                        1.75703089e+03])
# ge_polyfit = np.array([-4.57263854e-24, 5.26435303e-20, -2.66706773e-16, 7.80332608e-13, -1.45293034e-09,
#                        1.78528256e-06, -1.44766641e-03, 7.47060301e-01, -2.22641729e+02, 2.91992996e+04])
# fit_y = np.poly1d(ge_polyfit)
# print(fit_y(1310))

ge_x = [801.98164111, 861.72864972, 902.06896489, 946.28572814, 1009.29350546, 1080.77803259, 1153.49393859,
        1226.20903306, 1297.98706405, 1362.7585961, 1396.32958178, 1444.39017113, 1495.29036101, 1557.72704589,
        1579.18266614, 1588.76006549, 1600.35749866, 1617.44158008, 1638.71932876, 1665.1122537, 1698.283657,
        1743.24407246, 1801.56034869]
ge_y = [0.22914459, 0.29550202, 0.31202832, 0.3703519,  0.4379231, 0.52019398, 0.58198617, 0.6446759, 0.70720108,
        0.72296193, 0.73818766, 0.79308985, 0.8373888,  0.8658751, 0.77859947, 0.74972873, 0.66651847, 0.55149407,
        0.4932661, 0.41164407, 0.34501284, 0.25157028, 0.14223614]


plt.plot(ge_x, ge_y)
# plt.plot(x[R], y[R], color='red', marker='x')
# plt.plot(fit_x, fit_y(fit_x), linestyle='-', color='lime')
plt.xlabel('Wavelength (nm)')
plt.ylabel('Responsivity')
plt.title('Si Photodiode Responsivity')
text = si_responsivity(1310)
plt.text(800, 0.8, text)
plt.show()