import numpy as np
import time
import matplotlib.pyplot as plt
# import pyvisa as visa
# import ILX_Single_Channel as ILX
# import sys
# sys.path.insert(1, r"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\PythonScripts")
# from new_instrs import ILX_PM_8210 as PM

def current_to_wl(I):
    # return I*(4.96273890e-02) + 1.55495401e+03
    # return I*(4.13911892e-02) + 1.55662122e+03  # profile at 27
    return I*(4.27132659e-02) + 1.55623663e+03  # profile at 24

def main():
    """
    Connect to devices
    """
    # rm = visa.ResourceManager()
    # power = PM(1555, "dBm","FAST")
    # current = ILX.ILX_SCh()
    # current.laser_on
    # current.set_current(20)

    """
    Ask for input:
    1. Current start, end and step size
    2. Power plot min and max
    """

    """
    Sweep the 
    """


    """
    If interrupted, ask for input again without disconnecting from devices
    """

    """
    If exit is given, exit and disconnect from all devices
    """

    print("Connected to devices")

    plt.ion()
    figure, ax1 = plt.subplots(figsize=(7, 5))
    ax1.ticklabel_format(useOffset=False)
    ax2 = ax1.twinx()
    # ax1.set_ylim(0, 5)
    # ax2.set_ylim(0, 5)
    plt.grid(True)

    n = input("Options:\n1. Give new parameters\n2. Exit")
    while n != "exit":

        current_start = int(input("Current Start"))
        current_end = int(input("Current End"))
        step = int(input("Step size"))
        I = np.arange(current_start, current_end, step)

        k = 0
        p1 = np.zeros(len(I))
        w1 = np.zeros(len(I))
        p2 = np.zeros(len(I))
        w2 = np.zeros(len(I))

        try:
            while True:

                for i in range(len(I)):
                    _p = I[i]#power.readPower()
                    _w = current_to_wl(I[i])

                    p2[i] = p1[i]
                    w2[i] = w1[i]
                    p1[i] = _p
                    w1[i] = _w

                if k == 0:
                    spectrum1, = ax1.plot(w1, p1, 'r')
                    spectrum2, = ax2.plot(w1, p1, 'b')

                    spectrum1.set_ydata(p1)
                    spectrum2.set_ydata(p1)
                    figure.canvas.draw()
                    figure.canvas.flush_events()
                    print(f"Wavelength: {w1}")
                    print(f"Power: {p1}")

                else:
                    spectrum1.set_ydata(p1)
                    spectrum2.set_ydata(p2)
                    figure.canvas.draw()
                    figure.canvas.flush_events()
                    print(p1)
                    print(w1)

                    print(k)
                    k += 1
                    time.sleep(2)

        except KeyboardInterrupt:
            pass
        n = input("Options:\n1. Give new parameters\n2. Exit")

    # try:
    #     n = input("Give value for n")
    #     while n != "exit":
    #         print(n)
    #         n += 1
    #         if KeyboardInterrupt == True:
    #             n = input("Give value for n")

    # except:
    #     print("Done")



if __name__ == "__main__":
    # start = time.time()
    main()
    # end = time.time()
    # print(f"Done in {(end-start)/60} minutes")
