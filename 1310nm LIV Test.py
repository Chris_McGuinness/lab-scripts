import numpy as np
# import matplotlib.pyplot as plt
import pyvisa as visa
import time

rm = visa.ResourceManager()

# --- Want the script to be able to recognise any current supply and run from the same commands ---
# --- GPIB addresses for our devices are known so these can be hard coded in later instead --------


def detect_current_supply():
    """
    This function assumes the only device connected via GPIB is a current supply.
    Returns device[0], the first and only element of the list.
    """
    device = rm.list_resources()
    return device[0]


def current_suppy_connection():
    """
    Calls the current supply detection function.
    Connects to the current supply via GPIB and returns a current supply object.
    """
    device = detect_current_supply()
    current_supply_object = rm.open_resource(device)
    return device, current_supply_object


def set_current(current):
    """
    Takes a current supply object.
    Checks the type of current supply.
    Passes an if statement to give the correct command for that specific current supply.
    """
    device, current_supply_object = current_suppy_connection()

    if device == "LDC_3900":
        Change_current = current

    elif device == "ldc_3916370":
        Change_current = current


def main():


if __name__ == "__main__":
    main()
