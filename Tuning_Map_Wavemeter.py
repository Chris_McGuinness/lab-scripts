import numpy as np
import time
import matplotlib.pyplot as plt
import HP68120C
import RandS
import pyvisa as visa
import sys
# import random

rm = visa.ResourceManager()


def set_TEC(temperature):
    """
    Function will take a temperature in degrees celsius, probably 25.
    Will call the correct TEC function from instrs and set the temperature.
    """
    # print(rm.list_resources_info())
    TEC = rm.open_resource("GPIB0::1::INSTR")
    # tmout = 5000
    # TEC.timeout(5000)
    time.sleep(0.2)
    TEC.write(f"TEC:T {temperature}")
    TEC.write(f"Output ON")
    return TEC


def TEC_shutdown(TEC):
    time.sleep(0.2)
    TEC.write(f"Output OFF")


def gain_on():
    CC = rm.open_resource("GPIB0::1::INSTR")
    CC.write("LAS:OUT 1")
    return CC


def set_gain(device, current):#, SOA=False):
    # --- Set Current
    device.write(f"LAS:LDI {current}")
    # time.sleep(0.1)


def main():
    # print(rm.list_resources_info())
    # sys.exit()

    start = time.time()

    device = "Not Sure"
    file = rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Ring Laser Maps\Device_{device}_{start}.npz"

    ILX = gain_on()
    set_gain(ILX, 120)

    section_1_start = 0
    section_1_end = 10
    section_1_step = 0.2

    section_2_start = 0
    section_2_end = 10
    section_2_step = 0.2

    section_1_voltages = np.array(np.arange(section_1_start, abs(section_1_end) + section_1_step, section_1_step))
    section_2_voltages = np.array(np.arange(section_2_start, abs(section_2_end) + section_2_step, section_2_step))

    data = np.zeros((3, len(section_1_voltages), len(section_2_voltages)))

    HP = HP68120C.HPWavemeter()
    RS = RandS.RandS()
    HP.continuous_sweep_off()
    RS.initialise_section_voltage(1, 0)
    RS.initialise_section_voltage(3, 0)
    RS.initialise_section_voltage(4, 0)

    print('Test Beginning...')
    for i in range(len(section_1_voltages)):
        set_gain(ILX, 0)
        time.sleep(0.1)
        # --- set first section voltage
        RS.change_section_voltage(1, section_1_voltages[i])

        for j in range(len(section_2_voltages)):
            # --- set second section voltage
            RS.change_section_voltage(4, section_2_voltages[j])
            set_gain(ILX, 120)

            w, p, s = HP.read_power_and_wavelength()
            # w = random.randint(1282, 1322)
            # p = random.randint(1282, 1322)
            # s = random.randint(1282, 1322)
            data[0, j, i] = w
            data[1, j, i] = p
            data[2, j, i] = s

    np.savez(file=file, Data=data)

    end = time.time()
    duration = (end - start) / 60  # 3600
    # print(f"Done in {duration} hours")
    print(f"Done in {duration} minutes")

    # print(data)
    fig = plt.figure(figsize=(17, 5))
    plt.subplot(131)
    # norm1 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
    plot1 = plt.imshow(data[0], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
    cbar = fig.colorbar(plot1)
    plt.title("Wavelength")
    plt.xlabel("Ring 1 (V)")
    plt.ylabel("Ring 2 (V)")
    plt.subplot(132)
    # norm2 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
    plot2 = plt.imshow(data[1], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
    cbar = fig.colorbar(plot2)
    plt.title("Power")
    plt.xlabel("Ring 1 (V)")
    plt.ylabel("Ring 2 (V)")
    plt.subplot(133)
    # norm3 = plt.Normalize(vmin=1535, vmax=1585, clip=True)
    plot3 = plt.imshow(data[2], origin='lower', extent=(0, -10, 0, -10), cmap='YlGnBu')
    cbar = fig.colorbar(plot3)
    plt.title("SMSR")
    plt.xlabel("Ring 1 (V)")
    plt.ylabel("Ring 2 (V)")
    plt.show()
    fig.savefig(
        rf"C:\Users\ChrisMcGuinness\OneDrive - PILOT PHOTONICS\Documents\Ring Laser Maps\Device_{device}_{start}.png",
        dpi=800)


if __name__ == "__main__":

    main()
